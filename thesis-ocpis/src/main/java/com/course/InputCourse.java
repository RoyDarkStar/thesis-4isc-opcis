package com.course;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InputCourse
 */
@WebServlet(urlPatterns = "/InputCourse.com")
public class InputCourse extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/WEB-INF/view/InputCourse.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String courseName = request.getParameter("courseName");
		String courseDesc = request.getParameter("courseDescription");
		int lecUnits = Integer.parseInt(request.getParameter("lecUnits"));
		int labUnits = Integer.parseInt(request.getParameter("labUnits"));

		CourseService courseService = new CourseService();
		courseService.addCourse(courseName, courseDesc, lecUnits, labUnits);

		response.sendRedirect("InputCourse.com");
	}

}
