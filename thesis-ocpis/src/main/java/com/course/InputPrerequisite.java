package com.course;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InputPrerequisite
 */
@WebServlet("/InputPrerequisite.com")
public class InputPrerequisite extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/WEB-INF/view/InputPrerequisite.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		int courseID = Integer.parseInt(request.getParameter("selectCourse"));
		int prerequisite = Integer.parseInt(request.getParameter("selectPrerequisite"));

		CourseService courseService = new CourseService();
		courseService.addPrerequisite(courseID, prerequisite);

		response.sendRedirect("InputPrerequisite.com");

	}

}
