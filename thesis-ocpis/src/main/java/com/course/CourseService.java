package com.course;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class CourseService {

	public void addCourse(String courseName, String courseDescription, int lecUnits, int labUnits) {

		int programID = 3;
		int curriculumID = 3;
		int year = 4;
		int term = 2;

		if (labUnits == 0) {
			Connection con = null;
			PreparedStatement ps = null;
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

				// Lugs Connection
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";

				// Roy Connection
				// String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";

				con = DriverManager.getConnection(connectionUrl);
				ps = con.prepareStatement(
						"INSERT INTO [Course](programID, curriculumID, year, term, courseName, courseDescription, lecUnits) VALUES (?, ?, ?, ?, ?, ?, ?)");
				ps.setInt(1, programID);
				ps.setInt(2, curriculumID);
				ps.setInt(3, year);
				ps.setInt(4, term);
				ps.setString(5, courseName);
				ps.setString(6, courseDescription);
				ps.setInt(7, lecUnits);
				ps.executeUpdate();
			}

			catch (Exception e) {
				System.out.println(e);
			}
		}
		if (lecUnits == 0) {
			Connection con = null;
			PreparedStatement ps = null;
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

				// Lugs Connection
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";

				// Roy Connection
				// String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";

				con = DriverManager.getConnection(connectionUrl);
				ps = con.prepareStatement(
						"INSERT INTO [Course](programID, curriculumID, year, term, courseName, courseDescription, labUnits) VALUES (?, ?, ?, ?, ?, ?, ?)");
				ps.setInt(1, programID);
				ps.setInt(2, curriculumID);
				ps.setInt(3, year);
				ps.setInt(4, term);
				ps.setString(5, courseName);
				ps.setString(6, courseDescription);
				ps.setInt(7, labUnits);
				ps.executeUpdate();
			}

			catch (Exception e) {
				System.out.println(e);
			}
		} else if (lecUnits != 0 && labUnits != 0) {
			Connection con = null;
			PreparedStatement ps = null;
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

				// Lugs Connection
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";

				// Roy Connection
				// String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";

				con = DriverManager.getConnection(connectionUrl);
				ps = con.prepareStatement(
						"INSERT INTO [Course](programID, curriculumID, year, term, courseName, courseDescription, lecUnits, labUnits) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
				ps.setInt(1, programID);
				ps.setInt(2, curriculumID);
				ps.setInt(3, year);
				ps.setInt(4, term);
				ps.setString(5, courseName);
				ps.setString(6, courseDescription);
				ps.setInt(7, lecUnits);
				ps.setInt(8, labUnits);
				ps.executeUpdate();
			}

			catch (Exception e) {
				System.out.println(e);
			}
		}

	}

	public void addPrerequisite(int courseID, int prerequisite) {

		Connection con = null;
		PreparedStatement ps = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			// Roy Connection
			// String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			// "databaseName=OPCIS;user=sa;password=April241997;";

			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("INSERT INTO [Prerequisite](courseID, prerequisite) VALUES (?,?)");
			ps.setInt(1, courseID);
			ps.setInt(2, prerequisite);
			ps.executeUpdate();
		}

		catch (Exception e) {
			System.out.println(e);
		}
	}
}
