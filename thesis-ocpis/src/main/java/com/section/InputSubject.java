package com.section;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InputSubject
 */
@WebServlet("/InputSubject.com")
public class InputSubject extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/WEB-INF/view/InputSubject.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		int courseID = Integer.parseInt(request.getParameter("selectCourse"));
		int sectionID = Integer.parseInt(request.getParameter("selectSection"));

		SectionService sectionService = new SectionService();
		sectionService.addSubject(courseID, sectionID);

		response.sendRedirect("InputSubject.com");
	}

}
