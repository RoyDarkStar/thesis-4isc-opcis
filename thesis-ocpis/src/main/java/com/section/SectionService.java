package com.section;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class SectionService {

	public void addSection(String sectionName) {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			// Roy Connection
			// String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			// "databaseName=OPCIS;user=sa;password=April241997;";

			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("INSERT INTO [Section](sectionName) VALUES (?)");
			ps.setString(1, sectionName);
			ps.executeUpdate();
			con.close();
		}

		catch (Exception e) {
			System.out.println(e);
		}
	}

	public void addSubject(int courseID, int sectionID) {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			// Roy Connection
			// String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			// "databaseName=OPCIS;user=sa;password=April241997;";

			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("INSERT INTO [Enrollment](courseID, sectionID) VALUES (?, ?)");
			ps.setInt(1, courseID);
			ps.setInt(2, sectionID);
			ps.executeUpdate();
			con.close();
		}

		catch (Exception e) {
			System.out.println(e);
		}
	}

	public void addSchedule(int enrollmentID, String day, String timeStart, String timeEnd, String room) {

		Connection con = null;
		PreparedStatement ps = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			// Roy Connection
			// String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			// "databaseName=OPCIS;user=sa;password=April241997;";

			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement(
					"INSERT INTO [EnrollmentSchedule](enrollmentID, day, timeStart, timeEnd, room) VALUES (?, ?, ?, ?, ?)");
			ps.setInt(1, enrollmentID);
			ps.setString(2, day);
			ps.setString(3, timeStart);
			ps.setString(4, timeEnd);
			ps.setString(5, room);
			ps.executeUpdate();
			con.close();
		}

		catch (Exception e) {
			System.out.println(e);
		}
	}
}
