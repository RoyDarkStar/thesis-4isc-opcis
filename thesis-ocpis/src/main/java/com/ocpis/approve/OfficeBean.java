package com.ocpis.approve;

public class OfficeBean {

	private String collegeName;
	private String petitionCourse;
	private String courseID;
	private String programName;
	private String curriculumID;
	private String studentLastName;
	private String studentFirstName;
	private String studentMiddleName;
	private String studentID;
	private String price;

	public OfficeBean(String collegeName, String petitionCourse, String courseID, String programName,
			String curriculumID, String studentLastName, String studentFirstName, String studentMiddleName,
			String studentID, String price) {
		super();
		this.collegeName = collegeName;
		this.petitionCourse = petitionCourse;
		this.courseID = courseID;
		this.programName = programName;
		this.curriculumID = curriculumID;
		this.studentLastName = studentLastName;
		this.studentFirstName = studentFirstName;
		this.studentMiddleName = studentMiddleName;
		this.studentID = studentID;
		this.price = price;
	}

	/**
	 * @return the collegeName
	 */
	public String getCollegeName() {
		return collegeName;
	}

	/**
	 * @param collegeName
	 *            the collegeName to set
	 */
	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}

	/**
	 * @return the petitionCourse
	 */
	public String getPetitionCourse() {
		return petitionCourse;
	}

	/**
	 * @param petitionCourse
	 *            the petitionCourse to set
	 */
	public void setPetitionCourse(String petitionCourse) {
		this.petitionCourse = petitionCourse;
	}

	/**
	 * @return the courseID
	 */
	public String getCourseID() {
		return courseID;
	}

	/**
	 * @param courseID
	 *            the courseID to set
	 */
	public void setCourseID(String courseID) {
		this.courseID = courseID;
	}

	/**
	 * @return the programName
	 */
	public String getProgramName() {
		return programName;
	}

	/**
	 * @param programName
	 *            the programName to set
	 */
	public void setProgramName(String programName) {
		this.programName = programName;
	}

	/**
	 * @return the curriculumID
	 */
	public String getCurriculumID() {
		return curriculumID;
	}

	/**
	 * @param curriculumID
	 *            the curriculumID to set
	 */
	public void setCurriculumID(String curriculumID) {
		this.curriculumID = curriculumID;
	}

	/**
	 * @return the studentLastName
	 */
	public String getStudentLastName() {
		return studentLastName;
	}

	/**
	 * @param studentLastName
	 *            the studentLastName to set
	 */
	public void setStudentLastName(String studentLastName) {
		this.studentLastName = studentLastName;
	}

	/**
	 * @return the studentFirstName
	 */
	public String getStudentFirstName() {
		return studentFirstName;
	}

	/**
	 * @param studentFirstName
	 *            the studentFirstName to set
	 */
	public void setStudentFirstName(String studentFirstName) {
		this.studentFirstName = studentFirstName;
	}

	/**
	 * @return the studentMiddleName
	 */
	public String getStudentMiddleName() {
		return studentMiddleName;
	}

	/**
	 * @param studentMiddleName
	 *            the studentMiddleName to set
	 */
	public void setStudentMiddleName(String studentMiddleName) {
		this.studentMiddleName = studentMiddleName;
	}

	/**
	 * @return the studentID
	 */
	public String getStudentID() {
		return studentID;
	}

	/**
	 * @param studentID
	 *            the studentID to set
	 */
	public void setStudentID(String studentID) {
		this.studentID = studentID;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((collegeName == null) ? 0 : collegeName.hashCode());
		result = prime * result + ((courseID == null) ? 0 : courseID.hashCode());
		result = prime * result + ((curriculumID == null) ? 0 : curriculumID.hashCode());
		result = prime * result + ((petitionCourse == null) ? 0 : petitionCourse.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((programName == null) ? 0 : programName.hashCode());
		result = prime * result + ((studentFirstName == null) ? 0 : studentFirstName.hashCode());
		result = prime * result + ((studentID == null) ? 0 : studentID.hashCode());
		result = prime * result + ((studentLastName == null) ? 0 : studentLastName.hashCode());
		result = prime * result + ((studentMiddleName == null) ? 0 : studentMiddleName.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OfficeBean other = (OfficeBean) obj;
		if (collegeName == null) {
			if (other.collegeName != null)
				return false;
		} else if (!collegeName.equals(other.collegeName))
			return false;
		if (courseID == null) {
			if (other.courseID != null)
				return false;
		} else if (!courseID.equals(other.courseID))
			return false;
		if (curriculumID == null) {
			if (other.curriculumID != null)
				return false;
		} else if (!curriculumID.equals(other.curriculumID))
			return false;
		if (petitionCourse == null) {
			if (other.petitionCourse != null)
				return false;
		} else if (!petitionCourse.equals(other.petitionCourse))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (programName == null) {
			if (other.programName != null)
				return false;
		} else if (!programName.equals(other.programName))
			return false;
		if (studentFirstName == null) {
			if (other.studentFirstName != null)
				return false;
		} else if (!studentFirstName.equals(other.studentFirstName))
			return false;
		if (studentID == null) {
			if (other.studentID != null)
				return false;
		} else if (!studentID.equals(other.studentID))
			return false;
		if (studentLastName == null) {
			if (other.studentLastName != null)
				return false;
		} else if (!studentLastName.equals(other.studentLastName))
			return false;
		if (studentMiddleName == null) {
			if (other.studentMiddleName != null)
				return false;
		} else if (!studentMiddleName.equals(other.studentMiddleName))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"OfficeBean [collegeName=%s, petitionCourse=%s, courseID=%s, programName=%s, curriculumID=%s, studentLastName=%s, studentFirstName=%s, studentMiddleName=%s, studentID=%s, price=%s]",
				collegeName, petitionCourse, courseID, programName, curriculumID, studentLastName, studentFirstName,
				studentMiddleName, studentID, price);
	}

}
