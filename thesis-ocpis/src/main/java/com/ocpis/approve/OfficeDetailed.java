package com.ocpis.approve;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/ListDetailed.com")
public class OfficeDetailed extends HttpServlet {

	// private ApproveService approveService = new ApproveService(); //class name ng
	// approveService
	private OfficeService officeService = new OfficeService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String role = (String) session.getAttribute("role");
		String studentNumber = (String) session.getAttribute("studentNumber");
		String statusID = (String) session.getAttribute("ApproveBean");
		String petitionID = request.getParameter("PetitionIDBean");
		session.setAttribute("petitionID", petitionID);
		System.out.println(petitionID);
		// approveService.invalidateBean();
		officeService.invalidateBean();
		officeService.invalidateSched();

		if (role.equals("office")) {
			// approveService.invalidateBean();
			officeService.invalidateBean();
			request.setAttribute("office", officeService.retrieveOffice(petitionID));
			request.setAttribute("sched", officeService.retrieveSched(petitionID));
			request.getRequestDispatcher("/WEB-INF/view/OfficeDetailedPetition.jsp").forward(request, response);
		} else if (role.equals("faculty")) {
			officeService.invalidateBean();
			request.setAttribute("office", officeService.retrieveOffice(petitionID));
			request.setAttribute("sched", officeService.retrieveSched(petitionID));
			request.getRequestDispatcher("/WEB-INF/view/OfficeDetailedPetition.jsp").forward(request, response);
		} else {

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}