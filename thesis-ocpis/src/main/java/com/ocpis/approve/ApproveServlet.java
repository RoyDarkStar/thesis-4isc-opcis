package com.ocpis.approve;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/ListApprove.com")
public class ApproveServlet extends HttpServlet {

	private ApproveService approveService = new ApproveService(); // class name ng approveService
	private OfficeService officeService = new OfficeService();
	private FacultyService faculty = new FacultyService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String role = (String) session.getAttribute("role");
		String studentNumber = (String) session.getAttribute("studentNumber");
		String statusID = (String) session.getAttribute("ApproveBean");
		// approveService.invalidateBean();
		faculty.invalidateFacultyBean();

		if (role.equals("faculty")) {
			approveService.invalidateBean();
			String employeeNumber = (String) session.getAttribute("employeeNumber");
			String programID = (String) session.getAttribute("programID");
			request.setAttribute("approve", approveService.retrieveApprove(programID, role));
			request.getRequestDispatcher("/WEB-INF/view/FacultyApprovePetition.jsp").forward(request, response);
		} else if (role.equals("professor")) {
			approveService.invalidateBean();
			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/ProfessorApprovePetition.jsp").forward(request, response);

		} else if (role.equals("dean")) {
			approveService.invalidateBean();
			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/DeanApprovePetition.jsp").forward(request, response);
		}

		else if (role.equals("registrar")) {
			approveService.invalidateBean();
			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/RegistrarApprovePetition.jsp").forward(request, response);
		} else if (role.equals("ovraa")) {
			approveService.invalidateBean();
			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/OVRAAApprovePetition.jsp").forward(request, response);
		} else if (role.equals("accounting")) {
			approveService.invalidateBean();
			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/AccountingApprovePetition.jsp").forward(request, response);
		} else if (role.equals("office")) {
			approveService.invalidateBean();
			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/OfficeApprovePetition.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("/WEB-INF/view/error-500-alt.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}