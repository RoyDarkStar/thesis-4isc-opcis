package com.ocpis.approve;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/RegistrarChangeHeader.com")
public class RegistrarChangeHeader extends HttpServlet {

	private ApproveService approveService = new ApproveService(); // class name ng approveService
	private OfficeService officeService = new OfficeService();
	private FacultyService faculty = new FacultyService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String role = (String) session.getAttribute("role");
		String studentNumber = (String) session.getAttribute("studentNumber");
		String statusID = (String) session.getAttribute("ApproveBean");
		String semester = request.getParameter("changeSemester");
		String startDate = request.getParameter("changeStartDate");
		String endDate = request.getParameter("changeEndDate");
		String finalDate = request.getParameter("changeStartDate") + " - " + request.getParameter("changeEndDate");
		if (role.equals("registrar")) {
			approveService.invalidateBean();
			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			if (startDate != null && endDate != null) {
				Connection con = null;
				PreparedStatement ps = null;
				try {
					Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

					// Roy
					/*
					 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
					 * "databaseName=OPCIS;user=sa;password=April241997;";
					 */

					// Lugs
					Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
					String connectionUrl = "jdbc:sqlserver://localhost:1433;"
							+ "databaseName=OPCIS;user=sa;password=April241997;";
					con = DriverManager.getConnection(connectionUrl);
					String sql = "UPDATE Setting set Semester = '" + semester + "', date =  '" + finalDate
							+ "' WHERE settingID = 1";
					ps = con.prepareStatement(sql);
					ps.executeUpdate();
					int rows = ps.executeUpdate();
					System.out.println(rows + " rows updated");
					con.close();
				} catch (Exception e) {
					System.out.println(e);
				}
			}
			request.getRequestDispatcher("/WEB-INF/view/RegistrarApprovePetition.jsp").forward(request, response);

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}