package com.ocpis.approve;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ApproveService {
	private static List<ApproveBean> approve = new ArrayList<ApproveBean>();

	public List<ApproveBean> retrieveApprove(String studentNumber, String role) {
		// Ang explanation neto is, Binabato ko kay Getters etong mga approve.add (Yung
		// nakalagay sa array
		/*
		 * String petitionCourse; // Something to store String facultyID;
		 */

		String petitionID;
		String course;
		String faculty;
		String creator;
		String slot;
		String deptChair;
		String deptChairRemarks;
		String discount;
		String dean;
		String deanRemarks;
		String registrar;
		String registrarRemarks;
		String ovraa;
		String ovraaRemarks;
		String accounting;
		String accountingRemarks;
		String first;
		String last;
		String middle;

		if (role.equals("faculty")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT P.petitionID, P.petitionCourse, P.facultyID, F.firstName, F.middleName,"
								+ " F.lastName, P.studentID, X.deptChair, X.deptChairRemarks, X.facultyDiscount, X.dean, X.deanRemarks, X.registrar,"
								+ " X.registrarRemarks, X.ovraa, X.ovraaRemarks, X.accounting, X.accountingRemarks"
								+ " FROM [Petition]P JOIN [Course]C ON P.petitionCourse=C.courseName JOIN [DepartmentChair]D  ON C.programID=D.programID"
								+ " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID JOIN [Student]S ON P.studentID=S.studentID"
								+ " JOIN [Faculty]F ON P.facultyID=F.facultyID"
								+ " WHERE S.programID=? AND D.programID=? AND X.deptChair IS NULL");
				ps.setString(1, studentNumber);
				ps.setString(2, studentNumber);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					first = rs.getString("firstName");
					middle = rs.getString("middleName");
					last = rs.getString("lastName");
					faculty = first + " " + middle + " " + last;
					creator = rs.getString("studentID");
					slot = countPetitionMembers(petitionID);
					deptChair = rs.getString("deptChair");
					deptChairRemarks = rs.getString("deptChairRemarks");
					discount = rs.getString("facultyDiscount");
					dean = rs.getString("dean");
					deanRemarks = rs.getString("deanRemarks");
					registrar = rs.getString("registrar");
					registrarRemarks = rs.getString("registrarRemarks");
					ovraa = rs.getString("ovraa");
					ovraaRemarks = rs.getString("ovraaRemarks");
					accounting = rs.getString("accounting");
					accountingRemarks = rs.getString("accountingRemarks");

					approve.add(new ApproveBean(petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks,
							discount, dean, deanRemarks, registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
							accountingRemarks));

				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("professor")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT P.petitionID, P.petitionCourse, P.facultyID, F.firstName, F.middleName, F.lastName, P.studentID, X.deptChair,"
								+ " X.deptChairRemarks, X.facultyDiscount, X.dean, X.deanRemarks, X.registrar, X.registrarRemarks, X.ovraa, X.ovraaRemarks, X.accounting, X.accountingRemarks"
								+ " FROM [Petition]P" + " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID"
								+ " JOIN [Faculty]F ON P.facultyID=F.facultyID"
								+ " WHERE F.facultyID = ? AND X.deptChair IS NOT NULL"
								+ " AND X.facultyDiscount IS NULL");
				ps.setString(1, studentNumber);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					first = rs.getString("firstName");
					middle = rs.getString("middleName");
					last = rs.getString("lastName");
					faculty = first + " " + middle + " " + last;
					creator = rs.getString("studentID");
					slot = countPetitionMembers(petitionID);
					deptChair = rs.getString("deptChair");
					deptChairRemarks = rs.getString("deptChairRemarks");
					discount = rs.getString("facultyDiscount");
					dean = rs.getString("dean");
					deanRemarks = rs.getString("deanRemarks");
					registrar = rs.getString("registrar");
					registrarRemarks = rs.getString("registrarRemarks");
					ovraa = rs.getString("ovraa");
					ovraaRemarks = rs.getString("ovraaRemarks");
					accounting = rs.getString("accounting");
					accountingRemarks = rs.getString("accountingRemarks");

					approve.add(new ApproveBean(petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks,
							discount, dean, deanRemarks, registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
							accountingRemarks));

				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("dean")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT P.petitionID, P.petitionCourse, P.facultyID, F.firstName, F.middleName, F.lastName, P.studentID, X.deptChair,"
								+ " X.deptChairRemarks, X.facultyDiscount, X.dean, X.deanRemarks, X.registrar, X.registrarRemarks, X.ovraa, X.ovraaRemarks, X.accounting, X.accountingRemarks"
								+ " FROM [Petition]P" + " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID"
								+ " JOIN [Faculty]F ON P.facultyID=F.facultyID" + " WHERE X.faculty IS NOT NULL"
								+ " AND X.dean IS NULL");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					first = rs.getString("firstName");
					middle = rs.getString("middleName");
					last = rs.getString("lastName");
					faculty = first + " " + middle + " " + last;
					creator = rs.getString("studentID");
					slot = countPetitionMembers(petitionID);
					deptChair = rs.getString("deptChair");
					deptChairRemarks = rs.getString("deptChairRemarks");
					discount = rs.getString("facultyDiscount");
					dean = rs.getString("dean");
					deanRemarks = rs.getString("deanRemarks");
					registrar = rs.getString("registrar");
					registrarRemarks = rs.getString("registrarRemarks");
					ovraa = rs.getString("ovraa");
					ovraaRemarks = rs.getString("ovraaRemarks");
					accounting = rs.getString("accounting");
					accountingRemarks = rs.getString("accountingRemarks");

					approve.add(new ApproveBean(petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks,
							discount, dean, deanRemarks, registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
							accountingRemarks));
				}
				con.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("registrar")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT P.petitionID, P.petitionCourse, P.facultyID, F.firstName, F.middleName, F.lastName, P.studentID, X.deptChair,"
								+ " X.deptChairRemarks, X.facultyDiscount, X.dean, X.deanRemarks, X.registrar, X.registrarRemarks, X.ovraa, X.ovraaRemarks, X.accounting, X.accountingRemarks"
								+ " FROM [Petition]P" + " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID"
								+ " JOIN [Faculty]F ON P.facultyID=F.facultyID" + " WHERE X.dean IS NOT NULL"
								+ " AND X.registrar IS NULL");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					first = rs.getString("firstName");
					middle = rs.getString("middleName");
					last = rs.getString("lastName");
					faculty = first + " " + middle + " " + last;
					creator = rs.getString("studentID");
					slot = countPetitionMembers(petitionID);
					deptChair = rs.getString("deptChair");
					deptChairRemarks = rs.getString("deptChairRemarks");
					discount = rs.getString("facultyDiscount");
					dean = rs.getString("dean");
					deanRemarks = rs.getString("deanRemarks");
					registrar = rs.getString("registrar");
					registrarRemarks = rs.getString("registrarRemarks");
					ovraa = rs.getString("ovraa");
					ovraaRemarks = rs.getString("ovraaRemarks");
					accounting = rs.getString("accounting");
					accountingRemarks = rs.getString("accountingRemarks");

					approve.add(new ApproveBean(petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks,
							discount, dean, deanRemarks, registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
							accountingRemarks));
				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("ovraa")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT P.petitionID, P.petitionCourse, P.facultyID, F.firstName, F.middleName, F.lastName, P.studentID, X.deptChair,"
								+ " X.deptChairRemarks, X.facultyDiscount, X.dean, X.deanRemarks, X.registrar, X.registrarRemarks, X.ovraa, X.ovraaRemarks, X.accounting, X.accountingRemarks"
								+ " FROM [Petition]P" + " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID"
								+ " JOIN [Faculty]F ON P.facultyID=F.facultyID" + " WHERE X.registrar IS NOT NULL"
								+ " AND X.ovraa IS NULL");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					first = rs.getString("firstName");
					middle = rs.getString("middleName");
					last = rs.getString("lastName");
					faculty = first + " " + middle + " " + last;
					creator = rs.getString("studentID");
					slot = countPetitionMembers(petitionID);
					deptChair = rs.getString("deptChair");
					deptChairRemarks = rs.getString("deptChairRemarks");
					discount = rs.getString("facultyDiscount");
					dean = rs.getString("dean");
					deanRemarks = rs.getString("deanRemarks");
					registrar = rs.getString("registrar");
					registrarRemarks = rs.getString("registrarRemarks");
					ovraa = rs.getString("ovraa");
					ovraaRemarks = rs.getString("ovraaRemarks");
					accounting = rs.getString("accounting");
					accountingRemarks = rs.getString("accountingRemarks");

					approve.add(new ApproveBean(petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks,
							discount, dean, deanRemarks, registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
							accountingRemarks));
				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("accounting")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT P.petitionID, P.petitionCourse, P.facultyID, F.firstName, F.middleName, F.lastName, P.studentID, X.deptChair,"
								+ " X.deptChairRemarks, X.facultyDiscount, X.dean, X.deanRemarks, X.registrar, X.registrarRemarks, X.ovraa, X.ovraaRemarks, X.accounting, X.accountingRemarks"
								+ " FROM [Petition]P" + " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID"
								+ " JOIN [Faculty]F ON P.facultyID=F.facultyID" + " WHERE X.ovraa IS NOT NULL"
								+ " AND X.accounting IS NULL");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					first = rs.getString("firstName");
					middle = rs.getString("middleName");
					last = rs.getString("lastName");
					faculty = first + " " + middle + " " + last;
					creator = rs.getString("studentID");
					slot = countPetitionMembers(petitionID);
					deptChair = rs.getString("deptChair");
					deptChairRemarks = rs.getString("deptChairRemarks");
					discount = rs.getString("facultyDiscount");
					dean = rs.getString("dean");
					deanRemarks = rs.getString("deanRemarks");
					registrar = rs.getString("registrar");
					registrarRemarks = rs.getString("registrarRemarks");
					ovraa = rs.getString("ovraa");
					ovraaRemarks = rs.getString("ovraaRemarks");
					accounting = rs.getString("accounting");
					accountingRemarks = rs.getString("accountingRemarks");

					approve.add(new ApproveBean(petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks,
							discount, dean, deanRemarks, registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
							accountingRemarks));
				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("office")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT * FROM [ApprovedPetition]A" + " JOIN [PetitionStatus]S ON A.petitionID = S.petitionID"
								+ " JOIN [Petition]P ON S.petitionID = P.petitionID"
								+ " JOIN [Faculty]F ON P.facultyID=F.facultyID"
								+ " WHERE S.deptChair = 'Approved' AND S.registrar = 'Approved'"
								+ " AND S.ovraa = 'Approved' AND S.accounting ='Approved'");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					first = rs.getString("firstName");
					middle = rs.getString("middleName");
					last = rs.getString("lastName");
					faculty = first + " " + middle + " " + last;
					creator = rs.getString("studentID");
					slot = countPetitionMembers(petitionID);
					deptChair = rs.getString("deptChair");
					deptChairRemarks = rs.getString("deptChairRemarks");
					discount = rs.getString("facultyDiscount");
					dean = rs.getString("dean");
					deanRemarks = rs.getString("deanRemarks");
					registrar = rs.getString("registrar");
					registrarRemarks = rs.getString("registrarRemarks");
					ovraa = rs.getString("ovraa");
					ovraaRemarks = rs.getString("ovraaRemarks");
					accounting = rs.getString("accounting");
					accountingRemarks = rs.getString("accountingRemarks");

					approve.add(new ApproveBean(petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks,
							discount, dean, deanRemarks, registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
							accountingRemarks));
				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		}
		/*
		 * try{ Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver"); String
		 * connectionUrl = "jdbc:sqlserver://localhost:1433;" +
		 * "databaseName=OPCIS;user=sa;password=April241997;"; //
		 * "jdbc:sqlserver://localhost:1433;" + //
		 * "databaseName=OPCIS;user=sa;password=April241997;"; Connection con =
		 * DriverManager.getConnection(connectionUrl); PreparedStatement ps = con.
		 * prepareStatement("SELECT * FROM [PetitionStatus]S JOIN [PETITION]P ON S.petitionID = P.petitionID JOIN [PetitionSchedule]A ON A.petitionID = P.petitionID JOIN [PetitionMembers]B ON B.petitionID = A.petitionID"
		 * ); //ps.setString(1, studentNumber); ResultSet rs = ps.executeQuery();
		 * while(rs.next()){ statusID = rs.getString("statusID"); name =
		 * rs.getString("petitionID"); course = rs.getString("petitionCourse"); schedule
		 * = rs.getString("scheduleID"); timestart = rs.getString("timestart"); timeend
		 * = rs.getString("timeend"); room = rs.getString("room"); faculty =
		 * rs.getString("facultyID"); creator = rs.getString("studentID"); slot =
		 * rs.getString("memberID"); deptChair = rs.getString("deptChair");
		 * deptChairRemarks = rs.getString("deptChairRemarks"); registrar =
		 * rs.getString("registrar"); registrarRemarks =
		 * rs.getString("registrarRemarks"); ovraa = rs.getString("ovraa"); ovraaRemarks
		 * = rs.getString("ovraaRemarks"); accounting = rs.getString("accounting");
		 * accountingRemarks = rs.getString("accountingRemarks");
		 * 
		 * approve.add(new ApproveBean(statusID, name, course, schedule, timestart,
		 * timeend, room, faculty, creator, slot, deptChair, deptChairRemarks,
		 * registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
		 * accountingRemarks));
		 * 
		 * } con.close();
		 * 
		 * } catch(Exception e){ System.out.println(e); }
		 */
		return approve;
	}

	public List<ApproveBean> retrieveApproveHistory(String studentNumber, String role) {
		// Ang explanation neto is, Binabato ko kay Getters etong mga approve.add (Yung
		// nakalagay sa array
		/*
		 * String petitionCourse; // Something to store String facultyID;
		 */
		String petitionID;
		String course;
		String faculty;
		String creator;
		String slot;
		String deptChair;
		String deptChairRemarks;
		String discount;
		String dean;
		String deanRemarks;
		String registrar;
		String registrarRemarks;
		String ovraa;
		String ovraaRemarks;
		String accounting;
		String accountingRemarks;
		String first;
		String last;
		String middle;

		if (role.equals("professor")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				/*
				 * PreparedStatement ps = con.prepareStatement("SELECT * FROM [Petition]P" +
				 * " JOIN [Course]C ON P.petitionCourse=C.courseName" +
				 * " JOIN [DepartmentChair]D  ON C.programID=D.programID" +
				 * " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID" +
				 * " JOIN [Student]S ON P.studentID=S.studentID" +
				 * " WHERE S.programID=? AND D.programID=?" + " AND X.deptChair IS NOT NULL");
				 */
				/*
				 * PreparedStatement ps = con.
				 * prepareStatement("SELECT P.petitionID, P.petitionCourse, P.facultyID, F.firstName,"
				 * +
				 * " F.middleName, F.lastName, P.studentID, X.deptChair, X.deptChairRemarks, X.facultyDiscount, X.dean,"
				 * +
				 * " X.deanRemarks, X.registrar, X.registrarRemarks, X.ovraa, X.ovraaRemarks, X.accounting, X.accountingRemarks"
				 * +
				 * " FROM [Petition]P JOIN [Course]C ON P.petitionCourse=C.courseName JOIN [DepartmentChair]D  ON C.programID=D.programID"
				 * + " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID" +
				 * " JOIN [Faculty]F ON P.facultyID=F.facultyID" +
				 * " JOIN [Student]S ON P.studentID=S.studentID" +
				 * " WHERE S.programID=? AND D.programID=? AND X.deptChair IS NULL;");
				 */
				PreparedStatement ps = con.prepareStatement(
						"SELECT P.petitionID, P.petitionCourse, P.facultyID, F.firstName, F.middleName, F.lastName, P.studentID, X.deptChair,"
								+ " X.deptChairRemarks, X.facultyDiscount, X.dean, X.deanRemarks, X.registrar, X.registrarRemarks, X.ovraa, X.ovraaRemarks, X.accounting, X.accountingRemarks"
								+ " FROM [Petition]P" + " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID"
								+ " JOIN [Faculty]F ON P.facultyID=F.facultyID"
								+ " WHERE F.facultyID = ? AND X.deptChair IS NOT NULL"
								+ " AND X.facultyDiscount IS NOT NULL");
				ps.setString(1, studentNumber);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					first = rs.getString("firstName");
					middle = rs.getString("middleName");
					last = rs.getString("lastName");
					faculty = first + " " + middle + " " + last;
					creator = rs.getString("studentID");
					slot = countPetitionMembers(petitionID);
					deptChair = rs.getString("deptChair");
					deptChairRemarks = rs.getString("deptChairRemarks");
					discount = rs.getString("facultyDiscount");
					dean = rs.getString("dean");
					deanRemarks = rs.getString("deanRemarks");
					registrar = rs.getString("registrar");
					registrarRemarks = rs.getString("registrarRemarks");
					ovraa = rs.getString("ovraa");
					ovraaRemarks = rs.getString("ovraaRemarks");
					accounting = rs.getString("accounting");
					accountingRemarks = rs.getString("accountingRemarks");

					approve.add(new ApproveBean(petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks,
							discount, dean, deanRemarks, registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
							accountingRemarks));
				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("faculty")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				/*
				 * PreparedStatement ps = con.prepareStatement("SELECT * FROM [Petition]P" +
				 * " JOIN [Course]C ON P.petitionCourse=C.courseName" +
				 * " JOIN [DepartmentChair]D  ON C.programID=D.programID" +
				 * " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID" +
				 * " JOIN [Student]S ON P.studentID=S.studentID" +
				 * " WHERE S.programID=? AND D.programID=?" + " AND X.deptChair IS NOT NULL");
				 */
				/*
				 * PreparedStatement ps = con.
				 * prepareStatement("SELECT P.petitionID, P.petitionCourse, P.facultyID, F.firstName,"
				 * +
				 * " F.middleName, F.lastName, P.studentID, X.deptChair, X.deptChairRemarks, X.facultyDiscount, X.dean,"
				 * +
				 * " X.deanRemarks, X.registrar, X.registrarRemarks, X.ovraa, X.ovraaRemarks, X.accounting, X.accountingRemarks"
				 * +
				 * " FROM [Petition]P JOIN [Course]C ON P.petitionCourse=C.courseName JOIN [DepartmentChair]D  ON C.programID=D.programID"
				 * + " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID" +
				 * " JOIN [Faculty]F ON P.facultyID=F.facultyID" +
				 * " JOIN [Student]S ON P.studentID=S.studentID" +
				 * " WHERE S.programID=? AND D.programID=? AND X.deptChair IS NULL;");
				 */
				PreparedStatement ps = con.prepareStatement(
						"SELECT P.petitionID, P.petitionCourse, P.facultyID, F.firstName, F.middleName,"
								+ " F.lastName, P.studentID, X.deptChair, X.deptChairRemarks, X.facultyDiscount, X.dean, X.deanRemarks, X.registrar,"
								+ " X.registrarRemarks, X.ovraa, X.ovraaRemarks, X.accounting, X.accountingRemarks"
								+ " FROM [Petition]P JOIN [Course]C ON P.petitionCourse=C.courseName JOIN [DepartmentChair]D  ON C.programID=D.programID"
								+ " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID JOIN [Student]S ON P.studentID=S.studentID"
								+ " JOIN [Faculty]F ON P.facultyID=F.facultyID"
								+ " WHERE S.programID=? AND D.programID=? AND X.deptChair IS NOT NULL");
				ps.setString(1, studentNumber);
				ps.setString(2, studentNumber);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					first = rs.getString("firstName");
					middle = rs.getString("middleName");
					last = rs.getString("lastName");
					faculty = first + " " + middle + " " + last;
					creator = rs.getString("studentID");
					slot = countPetitionMembers(petitionID);
					deptChair = rs.getString("deptChair");
					deptChairRemarks = rs.getString("deptChairRemarks");
					discount = rs.getString("facultyDiscount");
					dean = rs.getString("dean");
					deanRemarks = rs.getString("deanRemarks");
					registrar = rs.getString("registrar");
					registrarRemarks = rs.getString("registrarRemarks");
					ovraa = rs.getString("ovraa");
					ovraaRemarks = rs.getString("ovraaRemarks");
					accounting = rs.getString("accounting");
					accountingRemarks = rs.getString("accountingRemarks");

					approve.add(new ApproveBean(petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks,
							discount, dean, deanRemarks, registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
							accountingRemarks));
				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("dean")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT P.petitionID, P.petitionCourse, P.facultyID, F.firstName, F.middleName, F.lastName, P.studentID, X.deptChair,"
								+ " X.deptChairRemarks, X.facultyDiscount, X.dean, X.deanRemarks, X.registrar, X.registrarRemarks, X.ovraa, X.ovraaRemarks, X.accounting, X.accountingRemarks"
								+ " FROM [Petition]P" + " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID"
								+ " JOIN [Faculty]F ON P.facultyID=F.facultyID" + " WHERE X.faculty IS NOT NULL"
								+ " AND X.dean IS NOT NULL;");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					first = rs.getString("firstName");
					middle = rs.getString("middleName");
					last = rs.getString("lastName");
					faculty = first + " " + middle + " " + last;
					creator = rs.getString("studentID");
					slot = countPetitionMembers(petitionID);
					deptChair = rs.getString("deptChair");
					deptChairRemarks = rs.getString("deptChairRemarks");
					discount = rs.getString("facultyDiscount");
					dean = rs.getString("dean");
					deanRemarks = rs.getString("deanRemarks");
					registrar = rs.getString("registrar");
					registrarRemarks = rs.getString("registrarRemarks");
					ovraa = rs.getString("ovraa");
					ovraaRemarks = rs.getString("ovraaRemarks");
					accounting = rs.getString("accounting");
					accountingRemarks = rs.getString("accountingRemarks");

					approve.add(new ApproveBean(petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks,
							discount, dean, deanRemarks, registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
							accountingRemarks));
				}
				con.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("registrar")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT P.petitionID, P.petitionCourse, P.facultyID, F.firstName, F.middleName, F.lastName, P.studentID, X.deptChair,"
								+ " X.deptChairRemarks, X.facultyDiscount, X.dean, X.deanRemarks, X.registrar, X.registrarRemarks, X.ovraa, X.ovraaRemarks, X.accounting, X.accountingRemarks"
								+ " FROM [Petition]P" + " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID"
								+ " JOIN [Faculty]F ON P.facultyID=F.facultyID" + " WHERE X.dean IS NOT NULL"
								+ " AND X.registrar IS NOT NULL;");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					first = rs.getString("firstName");
					middle = rs.getString("middleName");
					last = rs.getString("lastName");
					faculty = first + " " + middle + " " + last;
					creator = rs.getString("studentID");
					slot = countPetitionMembers(petitionID);
					deptChair = rs.getString("deptChair");
					deptChairRemarks = rs.getString("deptChairRemarks");
					discount = rs.getString("facultyDiscount");
					dean = rs.getString("dean");
					deanRemarks = rs.getString("deanRemarks");
					registrar = rs.getString("registrar");
					registrarRemarks = rs.getString("registrarRemarks");
					ovraa = rs.getString("ovraa");
					ovraaRemarks = rs.getString("ovraaRemarks");
					accounting = rs.getString("accounting");
					accountingRemarks = rs.getString("accountingRemarks");

					approve.add(new ApproveBean(petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks,
							discount, dean, deanRemarks, registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
							accountingRemarks));
				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("ovraa")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT P.petitionID, P.petitionCourse, P.facultyID, F.firstName, F.middleName, F.lastName, P.studentID, X.deptChair,"
								+ " X.deptChairRemarks, X.facultyDiscount, X.dean, X.deanRemarks, X.registrar, X.registrarRemarks, X.ovraa, X.ovraaRemarks, X.accounting, X.accountingRemarks"
								+ " FROM [Petition]P" + " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID"
								+ " JOIN [Faculty]F ON P.facultyID=F.facultyID" + " WHERE X.registrar IS NOT NULL"
								+ " AND X.ovraa IS NOT NULL;");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					first = rs.getString("firstName");
					middle = rs.getString("middleName");
					last = rs.getString("lastName");
					faculty = first + " " + middle + " " + last;
					creator = rs.getString("studentID");
					slot = countPetitionMembers(petitionID);
					deptChair = rs.getString("deptChair");
					deptChairRemarks = rs.getString("deptChairRemarks");
					discount = rs.getString("facultyDiscount");
					dean = rs.getString("dean");
					deanRemarks = rs.getString("deanRemarks");
					registrar = rs.getString("registrar");
					registrarRemarks = rs.getString("registrarRemarks");
					ovraa = rs.getString("ovraa");
					ovraaRemarks = rs.getString("ovraaRemarks");
					accounting = rs.getString("accounting");
					accountingRemarks = rs.getString("accountingRemarks");

					approve.add(new ApproveBean(petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks,
							discount, dean, deanRemarks, registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
							accountingRemarks));
				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("accounting")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT P.petitionID, P.petitionCourse, P.facultyID, F.firstName, F.middleName, F.lastName, P.studentID, X.deptChair,"
								+ " X.deptChairRemarks, X.facultyDiscount, X.dean, X.deanRemarks, X.registrar, X.registrarRemarks, X.ovraa, X.ovraaRemarks, X.accounting, X.accountingRemarks"
								+ " FROM [Petition]P" + " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID"
								+ " JOIN [Faculty]F ON P.facultyID=F.facultyID" + " WHERE X.ovraa IS NOT NULL"
								+ " AND X.accounting IS NOT NULL;");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					first = rs.getString("firstName");
					middle = rs.getString("middleName");
					last = rs.getString("lastName");
					faculty = first + " " + middle + " " + last;
					creator = rs.getString("studentID");
					slot = countPetitionMembers(petitionID);
					deptChair = rs.getString("deptChair");
					deptChairRemarks = rs.getString("deptChairRemarks");
					discount = rs.getString("facultyDiscount");
					dean = rs.getString("dean");
					deanRemarks = rs.getString("deanRemarks");
					registrar = rs.getString("registrar");
					registrarRemarks = rs.getString("registrarRemarks");
					ovraa = rs.getString("ovraa");
					ovraaRemarks = rs.getString("ovraaRemarks");
					accounting = rs.getString("accounting");
					accountingRemarks = rs.getString("accountingRemarks");

					approve.add(new ApproveBean(petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks,
							discount, dean, deanRemarks, registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
							accountingRemarks));
				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("office")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT * FROM [ApprovedPetition]A" + " JOIN [PetitionStatus]S ON A.petitionID = S.petitionID"
								+ " JOIN [PETITION]P ON S.petitionID = P.petitionID"
								+ " JOIN [Faculty]F ON P.facultyID=F.facultyID");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					first = rs.getString("firstName");
					middle = rs.getString("middleName");
					last = rs.getString("lastName");
					faculty = first + " " + middle + " " + last;
					creator = rs.getString("studentID");
					slot = countPetitionMembers(petitionID);
					deptChair = rs.getString("deptChair");
					deptChairRemarks = rs.getString("deptChairRemarks");
					discount = rs.getString("facultyDiscount");
					dean = rs.getString("dean");
					deanRemarks = rs.getString("deanRemarks");
					registrar = rs.getString("registrar");
					registrarRemarks = rs.getString("registrarRemarks");
					ovraa = rs.getString("ovraa");
					ovraaRemarks = rs.getString("ovraaRemarks");
					accounting = rs.getString("accounting");
					accountingRemarks = rs.getString("accountingRemarks");

					approve.add(new ApproveBean(petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks,
							discount, dean, deanRemarks, registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
							accountingRemarks));
				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		}
		/*
		 * try{ Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver"); String
		 * connectionUrl = "jdbc:sqlserver://localhost:1433;" +
		 * "databaseName=OPCIS;user=sa;password=April241997;"; //
		 * "jdbc:sqlserver://localhost:1433;" + //
		 * "databaseName=OPCIS;user=sa;password=April241997;"; Connection con =
		 * DriverManager.getConnection(connectionUrl); PreparedStatement ps = con.
		 * prepareStatement("SELECT * FROM [PetitionStatus]S JOIN [PETITION]P ON S.petitionID = P.petitionID JOIN [PetitionSchedule]A ON A.petitionID = P.petitionID JOIN [PetitionMembers]B ON B.petitionID = A.petitionID"
		 * ); //ps.setString(1, studentNumber); ResultSet rs = ps.executeQuery();
		 * while(rs.next()){ statusID = rs.getString("statusID"); name =
		 * rs.getString("petitionID"); course = rs.getString("petitionCourse"); schedule
		 * = rs.getString("scheduleID"); timestart = rs.getString("timestart"); timeend
		 * = rs.getString("timeend"); room = rs.getString("room"); faculty =
		 * rs.getString("facultyID"); creator = rs.getString("studentID"); slot =
		 * rs.getString("memberID"); deptChair = rs.getString("deptChair");
		 * deptChairRemarks = rs.getString("deptChairRemarks"); registrar =
		 * rs.getString("registrar"); registrarRemarks =
		 * rs.getString("registrarRemarks"); ovraa = rs.getString("ovraa"); ovraaRemarks
		 * = rs.getString("ovraaRemarks"); accounting = rs.getString("accounting");
		 * accountingRemarks = rs.getString("accountingRemarks");
		 * 
		 * approve.add(new ApproveBean(statusID, name, course, schedule, timestart,
		 * timeend, room, faculty, creator, slot, deptChair, deptChairRemarks,
		 * registrar, registrarRemarks, ovraa, ovraaRemarks, accounting,
		 * accountingRemarks));
		 * 
		 * } con.close();
		 * 
		 * } catch(Exception e){ System.out.println(e); }
		 */
		return approve;
	}

	public void rejectPetitionBeanStudent(String role, String remarks, String petitionID) { // Kinukuha ko yung form ng
																							// addPetition sa
																							// StudentPetitionJSP
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			String sql = "UPDATE PetitionStatus " + "set deptChair = ?, deptChairRemarks ='" + remarks + "', "
					+ "registrar = 0, registrarRemarks = 'Not Applicable', "
					+ "ovraa = 0, ovraaRemarks = 'Not Applicable', "
					+ "accounting = 0, accountingRemarks = 'Not Applicable' " + "WHERE petitionID = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, "Rejected"); // Rejected by Dept Chair
			ps.setString(2, petitionID);
			ps.executeUpdate();
			int rows = ps.executeUpdate();
			System.out.println(rows + " rows updated");
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void rejectRegistrarPetitionBeanStudent(String role, String remarks, String statusID) { // Kinukuha ko yung
																									// form ng
																									// addPetition sa
																									// StudentPetitionJSP
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			String sql = "UPDATE PetitionStatus "
					// + "set deptChair = 0, deptChairRemarks = 'Not Applicable', "
					+ "set registrar = ?, registrarRemarks ='" + remarks + "', "
					+ "ovraa = 0, ovraaRemarks = 'Not Applicable', "
					+ "accounting = 0, accountingRemarks = 'Not Applicable' " + "WHERE statusID = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, "0"); // Rejected by: Registrar
			ps.setString(2, statusID);
			ps.executeUpdate();
			int rows = ps.executeUpdate();
			System.out.println(rows + " rows updated");
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void rejectOVRAAPetitionBeanStudent(String role, String remarks, String statusID) { // Kinukuha ko yung form
																								// ng addPetition sa
																								// StudentPetitionJSP
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			String sql = "UPDATE PetitionStatus "
					// + "set deptChair = 0, deptChairRemarks = 'Not Applicable', "
					// + "registrar = 0, registrarRemarks = 'Not Applicable', "
					+ "set ovraa = ?, ovraaRemarks = '" + remarks + "', "
					+ "accounting = 0, accountingRemarks = 'Not Applicable' " + "WHERE statusID = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, "0"); // Rejected by: OVRAA
			ps.setString(2, statusID);
			ps.executeUpdate();
			int rows = ps.executeUpdate();
			System.out.println(rows + " rows updated");
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void rejectAccountingPetitionBeanStudent(String role, String remarks, String statusID) { // Kinukuha ko yung
																									// form ng
																									// addPetition sa
																									// StudentPetitionJSP
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			String sql = "UPDATE PetitionStatus "
					// + "set deptChair = 0, deptChairRemarks = 'Not Applicable', "
					// + "registrar = 0, registrarRemarks = 'Not Applicable', "
					// + "ovraa = 0, ovraaRemarks = 'NotApplicable', "
					+ "set accounting = ?, accountingRemarks = '" + remarks + "' " + "WHERE statusID = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, "0"); // Rejected by: Accounting
			ps.setString(2, statusID);
			ps.executeUpdate();
			int rows = ps.executeUpdate();
			System.out.println(rows + " rows updated");
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void updateApproveBean(String role, String remarks, String petitionID) {
		if (role.equals("faculty")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
				String sql = "UPDATE PetitionStatus " + " set deptChair = ?, deptChairRemarks = '" + remarks
						+ "', chairTimelog = ?" + " WHERE petitionID = ?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, "Approved"); // Approved by Faculty
				ps.setString(2, timestamp);
				ps.setString(3, petitionID);
				ps.executeUpdate();
				int rows = ps.executeUpdate();
				System.out.println(rows + " rows updated");
				con.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("registrar")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
				String sql = "UPDATE PetitionStatus "
						+ " set registrar = ?, registrarRemarks = 'Approved by Registrar', registrarTimelog = ? "
						+ " WHERE petitionID = ?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, "Approved"); // Approved by Faculty
				ps.setString(2, timestamp);
				ps.setString(3, petitionID);
				ps.executeUpdate();
				int rows = ps.executeUpdate();
				System.out.println(rows + " rows updated");
				con.close();
			} catch (Exception e) {
				System.out.println(e);
			}

		} else if (role.equals("dean")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
				String sql = "UPDATE PetitionStatus "
						+ " set dean = ?, deanRemarks = 'Approved by Dean', deanTimelog = ?" + " WHERE petitionID = ?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, "Approved"); // Approved by Faculty
				ps.setString(2, timestamp);
				ps.setString(3, petitionID);
				ps.executeUpdate();
				int rows = ps.executeUpdate();
				System.out.println(rows + " rows updated");
				con.close();
			} catch (Exception e) {
				System.out.println(e);
			}

		} else if (role.equals("ovraa")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
				String sql = "UPDATE PetitionStatus "
						+ " set ovraa = ?, ovraaRemarks = 'Approved by OVRAA', ovraaTimelog = ? "
						+ " WHERE petitionID = ?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, "Approved"); // Approved by Faculty
				ps.setString(2, timestamp);
				ps.setString(3, petitionID);
				ps.executeUpdate();
				int rows = ps.executeUpdate();
				System.out.println(rows + " rows updated");
				con.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("accounting")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
				String sql = "UPDATE PetitionStatus "
						+ " set accounting = ?, accountingRemarks = 'Approved by Accounting', accountingTimelog = ? "
						+ " WHERE petitionID = ? ";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, "Approved");
				ps.setString(2, timestamp);
				ps.setString(3, petitionID);
				ps.executeUpdate();
				int rows = ps.executeUpdate();
				System.out.println(rows + " rows updated");
				con.close();
			} catch (Exception e) {
				System.out.println(e);
			}
			/* approve.update(updateApproveBean); */
		}
	}

	public void addDiscount(String role, String petitionID, int discount) {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
			String sql = "UPDATE PetitionStatus " + " set faculty = ? , facultyDiscount= ?" + " WHERE petitionID = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, "Approved"); // Approved by Faculty
			ps.setInt(2, discount);
			ps.setString(3, petitionID);
			ps.executeUpdate();
			int rows = ps.executeUpdate();
			System.out.println(rows + " rows updated");
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void addPrice(String price, String petitionID) {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			String sql = "UPDATE Petition " + " set price = ?" + " WHERE petitionID = ? ";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, price);
			ps.setString(2, petitionID);
			ps.executeUpdate();
			int rows = ps.executeUpdate();
			System.out.println(rows + " rows updated");
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void addApproveBean(ApproveBean addApprove) { // Kinukuha ko yung form ng addPetition sa StudentPetitionJSP
		approve.add(addApprove);
	}

	public void deleteApproveBean(ApproveBean deleteApprove) {
		approve.remove(deleteApprove);
	}

	public void invalidateBean() {
		approve.clear();
	}

	public void addToApprovedPetition(String petitionID) {

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
			String sql = "INSERT INTO [ApprovedPetition](petitionID, timelog) VALUES (?, ?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, petitionID);
			ps.setString(2, timestamp);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void addNotification(String petitionID, String message) {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
			String sql = "INSERT INTO [Notifications](petitionID, message, timelog) VALUES (?, ?, ?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, petitionID);
			ps.setString(2, message);
			ps.setString(3, timestamp);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public String getPetitionCourse(String petitionID) {
		String petitionCourse = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
			String sql = "SELECT * FROM [Petition] WHERE petitionID=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, petitionID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				petitionCourse = rs.getString("petitionCourse");
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return petitionCourse;
	}

	public String countPetitionMembers(String petitionID) {
		String count = null;

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			String sql = "SELECT COUNT(*) AS Slot FROM [PetitionMembers] WHERE petitionID=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, petitionID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getString("slot");
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return count;
	}

	public void editFaculty(String petitionID, String facultyID) {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			String sql = "UPDATE [Petition] SET facultyID=? WHERE petitionID=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, facultyID);
			ps.setString(2, petitionID);
			ps.executeUpdate();
			int rows = ps.executeUpdate();
			System.out.println(rows + " rows updated");
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}