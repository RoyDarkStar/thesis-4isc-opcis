package com.ocpis.approve;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/RejectApprove.com")
public class ApproveRejectServlet extends HttpServlet {

	private ApproveService approveService = new ApproveService(); // class name ng approveService
	// if

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String role = (String) session.getAttribute("role");
		String studentNumber = (String) session.getAttribute("studentNumber");
		String remarks = request.getParameter("addingRemarks");
		String statusID = request.getParameter("ApproveBean");
		approveService.invalidateBean();
		System.out.println("role: " + role);
		System.out.println("statusID: " + statusID);
		System.out.println("remarks inputted: " + remarks);
		// approveService.addPetitionBeanStudent (role, remarks, statusID);

		if (role.equals("faculty")) {
			approveService.rejectPetitionBeanStudent(role, remarks, statusID);

			String petitionCourse = approveService.getPetitionCourse(statusID);
			String message = "Your Petition class: " + petitionCourse + " has been approved by the Department Chair.";
			approveService.addNotification(statusID, message);

			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/FacultyApprovePetition.jsp").forward(request, response);
		} else if (role.equals("dean")) {
			// approveService.invalidateBean();
			approveService.rejectRegistrarPetitionBeanStudent(role, remarks, statusID);

			String petitionCourse = approveService.getPetitionCourse(statusID);
			String message = "Your Petition class: " + petitionCourse + " has been approved by the Department Chair.";
			approveService.addNotification(statusID, message);

			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			response.sendRedirect("/ListApprove.com");
		}

		else if (role.equals("registrar")) {
			// approveService.invalidateBean();
			approveService.rejectRegistrarPetitionBeanStudent(role, remarks, statusID);

			String petitionCourse = approveService.getPetitionCourse(statusID);
			String message = "Your Petition class: " + petitionCourse + " has been approved by the Department Chair.";
			approveService.addNotification(statusID, message);

			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			response.sendRedirect("/ListApprove.com");
		} else if (role.equals("ovraa")) {
			approveService.rejectOVRAAPetitionBeanStudent(role, remarks, statusID);

			String petitionCourse = approveService.getPetitionCourse(statusID);
			String message = "Your Petition class: " + petitionCourse + " has been approved by the Department Chair.";
			approveService.addNotification(statusID, message);

			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			response.sendRedirect("/ListApprove.com");
		} else if (role.equals("accounting")) {
			approveService.rejectAccountingPetitionBeanStudent(role, remarks, statusID);

			String petitionCourse = approveService.getPetitionCourse(statusID);
			String message = "Your Petition class: " + petitionCourse + " has been approved by the Department Chair.";
			approveService.addNotification(statusID, message);

			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			response.sendRedirect("/ListApprove.com");
		}
	}
}