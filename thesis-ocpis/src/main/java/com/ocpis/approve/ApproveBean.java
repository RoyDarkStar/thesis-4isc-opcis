package com.ocpis.approve;

public class ApproveBean {
	private String petitionID;
	private String course;
	private String faculty;
	private String creator;
	private String slot;
	private String deptChair;
	private String deptChairRemarks;
	private String discount;
	private String dean;
	private String deanRemarks;
	private String registrar;
	private String registrarRemarks;
	private String ovraa;
	private String ovraaRemarks;
	private String accounting;
	private String accountingRemarks;

	public ApproveBean(String petitionID, String course, String faculty, String creator, String slot, String deptChair,
			String deptChairRemarks, String discount, String dean, String deanRemarks, String registrar,
			String registrarRemarks, String ovraa, String ovraaRemarks, String accounting, String accountingRemarks) {
		super();
		this.petitionID = petitionID;
		this.course = course;
		this.faculty = faculty;
		this.creator = creator;
		this.slot = slot;
		this.deptChair = deptChair;
		this.deptChairRemarks = deptChairRemarks;
		this.discount = discount;
		this.dean = dean;
		this.deanRemarks = deanRemarks;
		this.registrar = registrar;
		this.registrarRemarks = registrarRemarks;
		this.ovraa = ovraa;
		this.ovraaRemarks = ovraaRemarks;
		this.accounting = accounting;
		this.accountingRemarks = accountingRemarks;
	}

	/**
	 * @return the petitionID
	 */
	public String getPetitionID() {
		return petitionID;
	}

	/**
	 * @param petitionID
	 *            the petitionID to set
	 */
	public void setPetitionID(String petitionID) {
		this.petitionID = petitionID;
	}

	/**
	 * @return the course
	 */
	public String getCourse() {
		return course;
	}

	/**
	 * @param course
	 *            the course to set
	 */
	public void setCourse(String course) {
		this.course = course;
	}

	/**
	 * @return the faculty
	 */
	public String getFaculty() {
		return faculty;
	}

	/**
	 * @param faculty
	 *            the faculty to set
	 */
	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	/**
	 * @return the creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * @param creator
	 *            the creator to set
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * @return the slot
	 */
	public String getSlot() {
		return slot;
	}

	/**
	 * @param slot
	 *            the slot to set
	 */
	public void setSlot(String slot) {
		this.slot = slot;
	}

	/**
	 * @return the deptChair
	 */
	public String getDeptChair() {
		return deptChair;
	}

	/**
	 * @param deptChair
	 *            the deptChair to set
	 */
	public void setDeptChair(String deptChair) {
		this.deptChair = deptChair;
	}

	/**
	 * @return the deptChairRemarks
	 */
	public String getDeptChairRemarks() {
		return deptChairRemarks;
	}

	/**
	 * @param deptChairRemarks
	 *            the deptChairRemarks to set
	 */
	public void setDeptChairRemarks(String deptChairRemarks) {
		this.deptChairRemarks = deptChairRemarks;
	}

	/**
	 * @return the discount
	 */
	public String getDiscount() {
		return discount;
	}

	/**
	 * @param discount
	 *            the discount to set
	 */
	public void setDiscount(String discount) {
		this.discount = discount;
	}

	/**
	 * @return the dean
	 */
	public String getDean() {
		return dean;
	}

	/**
	 * @param dean
	 *            the dean to set
	 */
	public void setDean(String dean) {
		this.dean = dean;
	}

	/**
	 * @return the deanRemarks
	 */
	public String getDeanRemarks() {
		return deanRemarks;
	}

	/**
	 * @param deanRemarks
	 *            the deanRemarks to set
	 */
	public void setDeanRemarks(String deanRemarks) {
		this.deanRemarks = deanRemarks;
	}

	/**
	 * @return the registrar
	 */
	public String getRegistrar() {
		return registrar;
	}

	/**
	 * @param registrar
	 *            the registrar to set
	 */
	public void setRegistrar(String registrar) {
		this.registrar = registrar;
	}

	/**
	 * @return the registrarRemarks
	 */
	public String getRegistrarRemarks() {
		return registrarRemarks;
	}

	/**
	 * @param registrarRemarks
	 *            the registrarRemarks to set
	 */
	public void setRegistrarRemarks(String registrarRemarks) {
		this.registrarRemarks = registrarRemarks;
	}

	/**
	 * @return the ovraa
	 */
	public String getOvraa() {
		return ovraa;
	}

	/**
	 * @param ovraa
	 *            the ovraa to set
	 */
	public void setOvraa(String ovraa) {
		this.ovraa = ovraa;
	}

	/**
	 * @return the ovraaRemarks
	 */
	public String getOvraaRemarks() {
		return ovraaRemarks;
	}

	/**
	 * @param ovraaRemarks
	 *            the ovraaRemarks to set
	 */
	public void setOvraaRemarks(String ovraaRemarks) {
		this.ovraaRemarks = ovraaRemarks;
	}

	/**
	 * @return the accounting
	 */
	public String getAccounting() {
		return accounting;
	}

	/**
	 * @param accounting
	 *            the accounting to set
	 */
	public void setAccounting(String accounting) {
		this.accounting = accounting;
	}

	/**
	 * @return the accountingRemarks
	 */
	public String getAccountingRemarks() {
		return accountingRemarks;
	}

	/**
	 * @param accountingRemarks
	 *            the accountingRemarks to set
	 */
	public void setAccountingRemarks(String accountingRemarks) {
		this.accountingRemarks = accountingRemarks;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accounting == null) ? 0 : accounting.hashCode());
		result = prime * result + ((accountingRemarks == null) ? 0 : accountingRemarks.hashCode());
		result = prime * result + ((course == null) ? 0 : course.hashCode());
		result = prime * result + ((creator == null) ? 0 : creator.hashCode());
		result = prime * result + ((dean == null) ? 0 : dean.hashCode());
		result = prime * result + ((deanRemarks == null) ? 0 : deanRemarks.hashCode());
		result = prime * result + ((deptChair == null) ? 0 : deptChair.hashCode());
		result = prime * result + ((deptChairRemarks == null) ? 0 : deptChairRemarks.hashCode());
		result = prime * result + ((discount == null) ? 0 : discount.hashCode());
		result = prime * result + ((faculty == null) ? 0 : faculty.hashCode());
		result = prime * result + ((ovraa == null) ? 0 : ovraa.hashCode());
		result = prime * result + ((ovraaRemarks == null) ? 0 : ovraaRemarks.hashCode());
		result = prime * result + ((petitionID == null) ? 0 : petitionID.hashCode());
		result = prime * result + ((registrar == null) ? 0 : registrar.hashCode());
		result = prime * result + ((registrarRemarks == null) ? 0 : registrarRemarks.hashCode());
		result = prime * result + ((slot == null) ? 0 : slot.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApproveBean other = (ApproveBean) obj;
		if (accounting == null) {
			if (other.accounting != null)
				return false;
		} else if (!accounting.equals(other.accounting))
			return false;
		if (accountingRemarks == null) {
			if (other.accountingRemarks != null)
				return false;
		} else if (!accountingRemarks.equals(other.accountingRemarks))
			return false;
		if (course == null) {
			if (other.course != null)
				return false;
		} else if (!course.equals(other.course))
			return false;
		if (creator == null) {
			if (other.creator != null)
				return false;
		} else if (!creator.equals(other.creator))
			return false;
		if (dean == null) {
			if (other.dean != null)
				return false;
		} else if (!dean.equals(other.dean))
			return false;
		if (deanRemarks == null) {
			if (other.deanRemarks != null)
				return false;
		} else if (!deanRemarks.equals(other.deanRemarks))
			return false;
		if (deptChair == null) {
			if (other.deptChair != null)
				return false;
		} else if (!deptChair.equals(other.deptChair))
			return false;
		if (deptChairRemarks == null) {
			if (other.deptChairRemarks != null)
				return false;
		} else if (!deptChairRemarks.equals(other.deptChairRemarks))
			return false;
		if (discount == null) {
			if (other.discount != null)
				return false;
		} else if (!discount.equals(other.discount))
			return false;
		if (faculty == null) {
			if (other.faculty != null)
				return false;
		} else if (!faculty.equals(other.faculty))
			return false;
		if (ovraa == null) {
			if (other.ovraa != null)
				return false;
		} else if (!ovraa.equals(other.ovraa))
			return false;
		if (ovraaRemarks == null) {
			if (other.ovraaRemarks != null)
				return false;
		} else if (!ovraaRemarks.equals(other.ovraaRemarks))
			return false;
		if (petitionID == null) {
			if (other.petitionID != null)
				return false;
		} else if (!petitionID.equals(other.petitionID))
			return false;
		if (registrar == null) {
			if (other.registrar != null)
				return false;
		} else if (!registrar.equals(other.registrar))
			return false;
		if (registrarRemarks == null) {
			if (other.registrarRemarks != null)
				return false;
		} else if (!registrarRemarks.equals(other.registrarRemarks))
			return false;
		if (slot == null) {
			if (other.slot != null)
				return false;
		} else if (!slot.equals(other.slot))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"ApproveBean [petitionID=%s, course=%s, faculty=%s, creator=%s, slot=%s, deptChair=%s, deptChairRemarks=%s, discount=%s, dean=%s, deanRemarks=%s, registrar=%s, registrarRemarks=%s, ovraa=%s, ovraaRemarks=%s, accounting=%s, accountingRemarks=%s]",
				petitionID, course, faculty, creator, slot, deptChair, deptChairRemarks, discount, dean, deanRemarks,
				registrar, registrarRemarks, ovraa, ovraaRemarks, accounting, accountingRemarks);
	}

}