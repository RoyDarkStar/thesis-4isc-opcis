package com.ocpis.approve;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class FacultyService {
	private static List<FacultyBean> facultyList = new ArrayList<FacultyBean>();

	public List<FacultyBean> retieveApprove(String programID, String role) {
		String statusID;
		String petitionID;
		String course;
		String faculty;
		String creator;
		String slot;

		if (role.equals("faculty")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT * FROM [Petition]P" + " JOIN [Course]C ON P.petitionCourse=C.courseName"
								+ " JOIN [DepartmentChair]D  ON C.programID=D.programID"
								+ " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID"
								+ " JOIN [Student]S ON P.studentID=S.studentID"
								+ " WHERE S.programID=? AND D.programID=?" + " AND X.deptChair IS NULL");
				ps.setString(1, programID);
				ps.setString(2, programID);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					ApproveService approve = new ApproveService();
					statusID = rs.getString("statusID");
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					faculty = rs.getString("facultyID");
					creator = rs.getString("studentID");
					slot = approve.countPetitionMembers(petitionID);

					facultyList.add(new FacultyBean(statusID, petitionID, course, faculty, creator, slot));

				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return facultyList;
	}

	public List<FacultyBean> retieveApproveHistory(String programID, String role) {
		String statusID;
		String petitionID;
		String course;
		String faculty;
		String creator;
		String slot;
		if (role.equals("faculty")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT * FROM [Petition]P" + " JOIN [Course]C ON P.petitionCourse=C.courseName"
								+ " JOIN [DepartmentChair]D  ON C.programID=D.programID"
								+ " JOIN [PetitionStatus]X ON P.petitionID=X.petitionID"
								+ " JOIN [Student]S ON P.studentID=S.studentID"
								+ " WHERE S.programID=? AND D.programID=?" + " AND X.deptChair IS NOT NULL");
				ps.setString(1, programID);
				ps.setString(2, programID);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					ApproveService approve = new ApproveService();
					statusID = rs.getString("statusID");
					petitionID = rs.getString("petitionID");
					course = rs.getString("petitionCourse");
					faculty = rs.getString("facultyID");
					creator = rs.getString("studentID");
					slot = approve.countPetitionMembers(petitionID);

					facultyList.add(new FacultyBean(statusID, petitionID, course, faculty, creator, slot));

				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return facultyList;
	}

	public void invalidateFacultyBean() {
		facultyList.clear();
	}
}
