package com.ocpis.approve;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ApproveAddSchedule
 */
@WebServlet("/ApproveAddSchedule.com")
public class ApproveAddSchedule extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/WEB-INF/view/FacultyAddSchedule.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		AddScheduleService sched = new AddScheduleService();
		String petitionID = (String) session.getAttribute("petitionID");
		String day = request.getParameter("addingDay");
		String timeStart = request.getParameter("addingTimeStart");
		String timeEnd = request.getParameter("addingTimeEnd");
		String room = request.getParameter("addingRoom");

		sched.addSchedule(petitionID, day, timeStart, timeEnd, room);

		response.sendRedirect("/ApproveViewSchedule.com?StatusIDBean=" + petitionID);
	}
}
