package com.ocpis.approve;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ApproveViewSchedule
 */
@WebServlet("/ApproveViewSchedule.com")
public class ApproveViewSchedule extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		ApproveScheduleService schedule = new ApproveScheduleService();
		schedule.invalidateSchedule();
		String petitionID = request.getParameter("StatusIDBean");

		session.setAttribute("petitionID", petitionID);
		request.setAttribute("sched", schedule.retrieveSchedule(petitionID));
		request.getRequestDispatcher("/WEB-INF/view/FacultyViewSchedule.jsp").forward(request, response);
	}

}
