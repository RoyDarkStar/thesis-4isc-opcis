package com.ocpis.approve;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class OfficeService {
	private static List<OfficeBean> office = new ArrayList<OfficeBean>();
	private static List<ApproveScheduleBean> sched = new ArrayList<ApproveScheduleBean>();

	public List<OfficeBean> retrieveOffice(String petitionID) {
		String collegeName;
		String petitionCourse;
		String courseID;
		String programName;
		String curriculumID;
		String studentLastName;
		String studentFirstName;
		String studentMiddleName;
		String studentID;
		String deptChairID;
		String deptChairRemarks;
		String deanID;
		String deanRemarks;
		String registrarID;
		String registrarRemarks;
		String ovraaID;
		String ovraaRemarks;
		String accountingID;
		String accountingRemarks;
		String price;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement(
					"SELECT Z.collegeName, b.petitionCourse, B.petitionID, Q.programName, E.curriculumID, \r\n"
							+ "C.lastName, C.firstName, C.middleName, A.studentID, B.price  FROM PetitionMembers A JOIN Petition B ON a.petitionID = b.petitionID JOIN PetitionStatus D ON D.petitionID = A.petitionID\r\n"
							+ "JOIN STUDENT C on C.studentID = A.studentID\r\n"
							+ " JOIN [Faculty] G ON B.facultyID = G.facultyID\r\n"
							+ "            JOIN [COLLEGE]Z ON Z.collegeID = C.collegeID\r\n"
							+ "               JOIN [PROGRAM] Q ON C.programID = Q.programID\r\n"
							+ "				JOIN [Curriculum] E ON C.programID =  E.programID\r\n"
							+ "					WHERE a.petitionID = ?");
			/*
			 * +
			 * "SELECT * FROM PetitionMembers A JOIN Petition B ON a.petitionID = b.petitionID\r\n"
			 * +
			 * "JOIN STUDENT C on C.studentID = A.studentID JOIN PetitionStatus D ON D.petitionID = A.petitionID\r\n"
			 * + " JOIN [Faculty] G ON B.facultyID = G.facultyID\r\n" +
			 * "            JOIN [COLLEGE]Z ON Z.collegeID = C.collegeID\r\n" +
			 * "               JOIN [PROGRAM] Q ON C.programID = Q.programID\r\n" +
			 * "				JOIN [Curriculum] E ON C.programID =  E.programID\r\n" +
			 * "					JOIN [PetitionSchedule] F ON a.petitionID = F.petitionID\r\n"
			 * + "						JOIN [Dean] L ON D.deanID = L.deanID\r\n" +
			 * "						 JOIN [DepartmentChair] Y ON D.chairID = Y.chairID\r\n"
			 * +
			 * "                                    JOIN [Registrar] W ON D.registrarID = W.registrarID\r\n"
			 * +
			 * "                                        JOIN [OVRAA] V ON D.ovraaID = V.ovraaID\r\n"
			 * +
			 * "                                            JOIN [Accounting] U ON D.accountingID = U.accountingID\r\n"
			 * + "\r\n" +
			 * "											WHERE a.petitionID = ?");
			 */
			ps.setString(1, petitionID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				collegeName = rs.getString("collegeName");
				petitionCourse = rs.getString("petitionCourse");
				courseID = rs.getString("petitionID");
				programName = rs.getString("programName");
				curriculumID = rs.getString("curriculumID");
				studentLastName = rs.getString("lastName");
				studentFirstName = rs.getString("firstName");
				studentMiddleName = rs.getString("middleName");
				studentID = rs.getString("studentID");
				/*
				 * deptChairID = rs.getString("chairID"); deptChairRemarks =
				 * rs.getString("deptChairRemarks"); deanID = rs.getString("deanID");
				 * deanRemarks = rs.getString("deanRemarks"); registrarID =
				 * rs.getString("registrarID"); registrarRemarks =
				 * rs.getString("registrarRemarks"); ovraaID = rs.getString("ovraaID");
				 * ovraaRemarks = rs.getString("ovraaRemarks"); accountingID =
				 * rs.getString("accountingID"); accountingRemarks =
				 * rs.getString("accountingRemarks");
				 */
				price = rs.getString("price");

				office.add(new OfficeBean(collegeName, petitionCourse, courseID, programName, curriculumID,
						studentLastName, studentFirstName, studentMiddleName, studentID, price));

			}
			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return office;

	}

	public List<ApproveScheduleBean> retrieveSched(String petitionNumber) {
		String petitionID;
		String scheduleID;
		String day;
		String timeStart;
		String timeEnd;
		String room;

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			/*
			 * "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement("SELECT * FROM [PetitionSchedule] WHERE petitionID=?");
			ps.setString(1, petitionNumber);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				petitionID = rs.getString("petitionID");
				scheduleID = rs.getString("scheduleID");
				day = rs.getString("day");
				timeStart = rs.getString("timeStart");
				timeEnd = rs.getString("timeEnd");
				room = rs.getString("room");

				sched.add(new ApproveScheduleBean(petitionID, scheduleID, day, timeStart, timeEnd, room));
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return sched;
	}

	public void invalidateBean() {
		office.clear();
	}

	public void invalidateSched() {
		sched.clear();
	}
}