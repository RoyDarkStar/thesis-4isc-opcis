package com.ocpis.approve;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/UpdateApprove.com")
public class ApproveUpdate extends HttpServlet {

	private ApproveService approveService = new ApproveService(); // class name ng approveService
	// if

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String role = (String) session.getAttribute("role");
		int discount = 0;
		// String studentNumber = (String) session.getAttribute("studentNumber");
		String remarks = request.getParameter("addingRemarks");
		String price = request.getParameter("addingPrice");
		String statusID = request.getParameter("ApproveBean");
		String approveRemarks = request.getParameter("addingApproveRemarks");
		approveService.invalidateBean();
		System.out.println("role: " + role);
		System.out.println("statusID: " + statusID);
		// System.out.println(remarks);
		// approveService.addPetitionBeanStudent (role, remarks, statusID);
		if (role.equals("faculty")) {
			if (approveRemarks.isEmpty()) {
				remarks = "Approved by Faculty";
				approveService.updateApproveBean(role, remarks, statusID);
				String petitionCourse = approveService.getPetitionCourse(statusID);
				String message = "Your Petition class: " + petitionCourse
						+ " has been approved by the Department Chair.";
				approveService.addNotification(statusID, message);

				String employeeNumber = (String) session.getAttribute("employeeNumber");
				request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
				response.sendRedirect("ListApprove.com");
			} else {
				remarks = approveRemarks;
				approveService.updateApproveBean(role, remarks, statusID);

				String petitionCourse = approveService.getPetitionCourse(statusID);
				String message = "Your Petition class: " + petitionCourse
						+ " has been approved by the Department Chair.";
				approveService.addNotification(statusID, message);

				String employeeNumber = (String) session.getAttribute("employeeNumber");
				request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
				response.sendRedirect("ListApprove.com");

			}
		} else if (role.equals("professor")) {
			// approveService.invalidateBean();
			discount = Integer.parseInt(price);
			approveService.addDiscount(role, statusID, discount);

			String petitionCourse = approveService.getPetitionCourse(statusID);
			String message = "Your Petition class: " + petitionCourse + " has been approved by the Faculty.";
			approveService.addNotification(statusID, message);

			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			response.sendRedirect("ListApprove.com");
		} else if (role.equals("dean")) {
			// approveService.invalidateBean();
			approveService.updateApproveBean(role, remarks, statusID);

			String petitionCourse = approveService.getPetitionCourse(statusID);
			String message = "Your Petition class: " + petitionCourse + " has been approved by the Dean.";
			approveService.addNotification(statusID, message);

			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			response.sendRedirect("/ListApprove.com");
		} else if (role.equals("registrar")) {
			// approveService.invalidateBean();
			approveService.updateApproveBean(role, remarks, statusID);

			String petitionCourse = approveService.getPetitionCourse(statusID);
			String message = "Your Petition class: " + petitionCourse + " has been approved by the Registrar.";
			approveService.addNotification(statusID, message);

			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			response.sendRedirect("/ListApprove.com");
		} else if (role.equals("ovraa")) {
			approveService.updateApproveBean(role, remarks, statusID);

			String petitionCourse = approveService.getPetitionCourse(statusID);
			String message = "Your Petition class: " + petitionCourse + " has been approved by the OVRAA.";
			approveService.addNotification(statusID, message);

			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			response.sendRedirect("/ListApprove.com");
		} else if (role.equals("accounting")) {
			approveService.addPrice(price, statusID);
			approveService.updateApproveBean(role, remarks, statusID);
			approveService.addToApprovedPetition(statusID);

			String petitionCourse = approveService.getPetitionCourse(statusID);
			String message = "Your Petition class: " + petitionCourse + " has been approved by the Accounting.";
			approveService.addNotification(statusID, message);

			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			response.sendRedirect("/ListApprove.com");
		}
	}
}