package com.ocpis.approve;

public class ApproveScheduleBean {

	String petitionID;
	String scheduleID;
	String day;
	String timeStart;
	String timeEnd;
	String room;

	public ApproveScheduleBean(String petitionID, String scheduleID, String day, String timeStart, String timeEnd,
			String room) {
		super();
		this.petitionID = petitionID;
		this.scheduleID = scheduleID;
		this.day = day;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
		this.room = room;
	}

	/**
	 * @return the petitionID
	 */
	public String getPetitionID() {
		return petitionID;
	}

	/**
	 * @param petitionID
	 *            the petitionID to set
	 */
	public void setPetitionID(String petitionID) {
		this.petitionID = petitionID;
	}

	/**
	 * @return the scheduleID
	 */
	public String getScheduleID() {
		return scheduleID;
	}

	/**
	 * @param scheduleID
	 *            the scheduleID to set
	 */
	public void setScheduleID(String scheduleID) {
		this.scheduleID = scheduleID;
	}

	/**
	 * @return the day
	 */
	public String getDay() {
		return day;
	}

	/**
	 * @param day
	 *            the day to set
	 */
	public void setDay(String day) {
		this.day = day;
	}

	/**
	 * @return the timeStart
	 */
	public String getTimeStart() {
		return timeStart;
	}

	/**
	 * @param timeStart
	 *            the timeStart to set
	 */
	public void setTimeStart(String timeStart) {
		this.timeStart = timeStart;
	}

	/**
	 * @return the timeEnd
	 */
	public String getTimeEnd() {
		return timeEnd;
	}

	/**
	 * @param timeEnd
	 *            the timeEnd to set
	 */
	public void setTimeEnd(String timeEnd) {
		this.timeEnd = timeEnd;
	}

	/**
	 * @return the room
	 */
	public String getRoom() {
		return room;
	}

	/**
	 * @param room
	 *            the room to set
	 */
	public void setRoom(String room) {
		this.room = room;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result + ((petitionID == null) ? 0 : petitionID.hashCode());
		result = prime * result + ((room == null) ? 0 : room.hashCode());
		result = prime * result + ((scheduleID == null) ? 0 : scheduleID.hashCode());
		result = prime * result + ((timeEnd == null) ? 0 : timeEnd.hashCode());
		result = prime * result + ((timeStart == null) ? 0 : timeStart.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApproveScheduleBean other = (ApproveScheduleBean) obj;
		if (day == null) {
			if (other.day != null)
				return false;
		} else if (!day.equals(other.day))
			return false;
		if (petitionID == null) {
			if (other.petitionID != null)
				return false;
		} else if (!petitionID.equals(other.petitionID))
			return false;
		if (room == null) {
			if (other.room != null)
				return false;
		} else if (!room.equals(other.room))
			return false;
		if (scheduleID == null) {
			if (other.scheduleID != null)
				return false;
		} else if (!scheduleID.equals(other.scheduleID))
			return false;
		if (timeEnd == null) {
			if (other.timeEnd != null)
				return false;
		} else if (!timeEnd.equals(other.timeEnd))
			return false;
		if (timeStart == null) {
			if (other.timeStart != null)
				return false;
		} else if (!timeStart.equals(other.timeStart))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"ApproveScheduleBean [petitionID=%s, scheduleID=%s, day=%s, timeStart=%s, timeEnd=%s, room=%s]",
				petitionID, scheduleID, day, timeStart, timeEnd, room);
	}

}
