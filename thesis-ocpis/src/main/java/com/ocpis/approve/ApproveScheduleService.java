package com.ocpis.approve;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ApproveScheduleService {

	private static List<ApproveScheduleBean> approveSchedule = new ArrayList<ApproveScheduleBean>();

	public List<ApproveScheduleBean> retrieveSchedule(String petitionNumber) {

		String petitionID;
		String scheduleID;
		String day;
		String timeStart;
		String timeEnd;
		String room;

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			/*
			 * "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement("SELECT * FROM [PetitionSchedule] WHERE petitionID=?");
			ps.setString(1, petitionNumber);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				petitionID = rs.getString("petitionID");
				scheduleID = rs.getString("scheduleID");
				day = rs.getString("day");
				timeStart = rs.getString("timeStart");
				timeEnd = rs.getString("timeEnd");
				room = rs.getString("room");

				approveSchedule.add(new ApproveScheduleBean(petitionID, scheduleID, day, timeStart, timeEnd, room));
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return approveSchedule;
	}

	public void invalidateSchedule() {
		approveSchedule.clear();
	}
}
