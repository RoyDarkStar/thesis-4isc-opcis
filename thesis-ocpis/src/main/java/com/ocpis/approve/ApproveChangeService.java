package com.ocpis.approve;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ApproveChangeService {
	private static List<ChangeBean> change = new ArrayList<ChangeBean>();

	public List<ChangeBean> retrieveChange(String statusID, String role) {
		// Ang explanation neto is, Binabato ko kay Getters etong mga approve.add (Yung
		// nakalagay sa array
		String facultyID;
		String timestart;
		String timeend;
		String room;
		String day;

		if (role.equals("faculty")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				// "jdbc:sqlserver://localhost:1433;" +
				// "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT * FROM [PetitionStatus]S JOIN [PETITION]P ON S.petitionID = P.petitionID JOIN [PetitionSchedule]A ON A.petitionID = P.petitionID JOIN [Course]C ON C.courseName = P.petitionCourse WHERE C.programID = (SELECT D.programID FROM [DepartmentChair]D WHERE D.chairID=? AND S.deptChair = 'Approved')");
				ps.setString(1, statusID);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					facultyID = rs.getString("facultyID");
					timestart = rs.getString("timestart");
					timeend = rs.getString("timeend");
					room = rs.getString("room");
					day = rs.getString("day");

					change.add(new ChangeBean(statusID, facultyID, timestart, timeend, room, day));

				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return change;
	}

	public void editPetition(String statusID, String facultyID, String day, String timestart, String timeend,
			String room) { // Kinukuha ko yung form ng addPetition sa StudentPetitionJSP
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			String sql = "UPDATE PetitionStatus " + "SET facultyID = ?, day = ?, timeStart = ?, timeEnd = ?, room = ? "
					+ "WHERE statusID = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, facultyID);
			ps.setString(2, day);
			ps.setString(3, timestart);
			ps.setString(4, timeend);
			ps.setString(5, room);
			ps.setString(6, statusID);
			ps.executeUpdate();
			int rows = ps.executeUpdate();
			System.out.println(rows + " rows updated");
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void invalidateBean() {
		change.clear();
	}

}