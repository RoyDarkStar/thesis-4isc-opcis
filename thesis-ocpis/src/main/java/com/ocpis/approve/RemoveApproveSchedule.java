package com.ocpis.approve;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RemoveApproveSchedule
 */
@WebServlet("/RemoveApproveSchedule.com")
public class RemoveApproveSchedule extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		RemoveApproveServices remove = new RemoveApproveServices();
		String scheduleID = request.getParameter("ScheduleIDBean");
		String petitionID = request.getParameter("PetitionIDBean");

		remove.removeSchedule(scheduleID);
		response.sendRedirect("ApproveViewSchedule.com?StatusIDBean=" + petitionID);
	}

}
