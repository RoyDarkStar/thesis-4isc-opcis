package com.ocpis.approve;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/ListHistory.com")
public class ApproveHistoryServlet extends HttpServlet {

	private ApproveService approveService = new ApproveService(); // class name ng approveService
	private FacultyService faculty = new FacultyService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String role = (String) session.getAttribute("role");
		// String studentNumber = (String) session.getAttribute("studentNumber");
		// String statusID = (String)session.getAttribute("ApproveBean");
		approveService.invalidateBean();

		if (role.equals("faculty")) {
			faculty.invalidateFacultyBean();
			String programID = (String) session.getAttribute("programID");
			request.setAttribute("approve", faculty.retieveApproveHistory(programID, role));
			request.getRequestDispatcher("/WEB-INF/view/FacultyHistory.jsp").forward(request, response);
		} else if (role.equals("dean")) {
			approveService.invalidateBean();
			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApproveHistory(employeeNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/DeanHistory.jsp").forward(request, response);
		}

		else if (role.equals("registrar")) {
			approveService.invalidateBean();
			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApproveHistory(employeeNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/RegistrarHistory.jsp").forward(request, response);
		} else if (role.equals("ovraa")) {
			approveService.invalidateBean();
			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApproveHistory(employeeNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/OVRAAHistory.jsp").forward(request, response);
		} else if (role.equals("accounting")) {
			approveService.invalidateBean();
			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApproveHistory(employeeNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/AccountingHistory.jsp").forward(request, response);
		} else if (role.equals("office")) {
			approveService.invalidateBean();
			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApproveHistory(employeeNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/OfficeApprovePetition.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}