package com.ocpis.approve;

public class ChangeBean {
	private String statusID;
	private String facultyID;
	private String day;
	private String timeStart;
	private String timeEnd;
	private String end;

	public ChangeBean(String statusID, String facultyID, String day, String timeStart, String timeEnd, String end) {
		super();
		this.statusID = statusID;
		this.facultyID = facultyID;
		this.day = day;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
		this.end = end;
	}

	public String getStatusID() {
		return statusID;
	}

	public void setStatusID(String statusID) {
		this.statusID = statusID;
	}

	public String getFacultyID() {
		return facultyID;
	}

	public void setFacultyID(String facultyID) {
		this.facultyID = facultyID;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(String timeStart) {
		this.timeStart = timeStart;
	}

	public String getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(String timeEnd) {
		this.timeEnd = timeEnd;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result + ((end == null) ? 0 : end.hashCode());
		result = prime * result + ((facultyID == null) ? 0 : facultyID.hashCode());
		result = prime * result + ((statusID == null) ? 0 : statusID.hashCode());
		result = prime * result + ((timeEnd == null) ? 0 : timeEnd.hashCode());
		result = prime * result + ((timeStart == null) ? 0 : timeStart.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChangeBean other = (ChangeBean) obj;
		if (day == null) {
			if (other.day != null)
				return false;
		} else if (!day.equals(other.day))
			return false;
		if (end == null) {
			if (other.end != null)
				return false;
		} else if (!end.equals(other.end))
			return false;
		if (facultyID == null) {
			if (other.facultyID != null)
				return false;
		} else if (!facultyID.equals(other.facultyID))
			return false;
		if (statusID == null) {
			if (other.statusID != null)
				return false;
		} else if (!statusID.equals(other.statusID))
			return false;
		if (timeEnd == null) {
			if (other.timeEnd != null)
				return false;
		} else if (!timeEnd.equals(other.timeEnd))
			return false;
		if (timeStart == null) {
			if (other.timeStart != null)
				return false;
		} else if (!timeStart.equals(other.timeStart))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("ChangeBean [statusID=%s, facultyID=%s, day=%s, timeStart=%s, timeEnd=%s, end=%s]",
				statusID, facultyID, day, timeStart, timeEnd, end);
	}

}