package com.ocpis.approve;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class AddScheduleService {

	public void addSchedule(String petitionID, String day, String timeStart, String timeEnd, String room) {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement(
					"INSERT INTO [PetitionSchedule](petitionID, day, timeStart, timeEnd, room) VALUES (?, ?, ?, ?, ?)");
			ps.setString(1, petitionID);
			ps.setString(2, day);
			ps.setString(3, timeStart);
			ps.setString(4, timeEnd);
			ps.setString(5, room);
			ps.executeUpdate();
			System.out.println("DB updated");

			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
