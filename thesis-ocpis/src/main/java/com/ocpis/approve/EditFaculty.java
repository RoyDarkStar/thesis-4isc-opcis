package com.ocpis.approve;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EditFaculty
 */
@WebServlet("/EditFaculty.com")
public class EditFaculty extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ApproveService approve = new ApproveService();

		String petitionID = request.getParameter("ApproveBean");
		String facultyID = request.getParameter("addingProfessor");

		approve.editFaculty(petitionID, facultyID);
		response.sendRedirect("ListApprove.com");
	}

}
