package com.ocpis.approve;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/ApproveEdit.com")
public class ApproveEditServlet extends HttpServlet {

	private ApproveChangeService changeService = new ApproveChangeService(); // class name ng approveService
	private ApproveService approveService = new ApproveService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String role = (String) session.getAttribute("role");
		String statusID = request.getParameter("changeStatusID");
		String facultyID = request.getParameter("changeFacultyID");
		String day = request.getParameter("changeDay");
		String Timestart = request.getParameter("changeTimestart");
		String Timeend = request.getParameter("changeTimeend");
		String room = request.getParameter("changeRoom");

		changeService.invalidateBean();
		System.out.println("role: " + role);
		System.out.println("statusID: " + statusID);
		// approveService.addPetitionBeanStudent (role, remarks, statusID);

		if (role.equals("faculty")) {
			changeService.editPetition(statusID, facultyID, day, Timestart, Timeend, room);
			String employeeNumber = (String) session.getAttribute("employeeNumber");
			request.setAttribute("approve", approveService.retrieveApprove(employeeNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/FacultyApprovePetition.jsp").forward(request, response);
		} else {
			System.out.println("DI GUMAGANA TONG IF STATEMENT");
		}
	}
}