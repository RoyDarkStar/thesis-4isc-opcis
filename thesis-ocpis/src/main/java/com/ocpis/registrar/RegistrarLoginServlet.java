package com.ocpis.registrar;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.ocpis.petition.PetitionService;

@WebServlet(urlPatterns = "/RegistrarLogin.com")
public class RegistrarLoginServlet extends HttpServlet {

	private RegistrarUserValidationService registrarValidationService = new RegistrarUserValidationService(); // class
																												// name
																												// ng
																												// userValidService
																												// for
																												// Students
	// private ApproveService petitionService = new ApproveService(); //class name
	// ng petitionService

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/view/RegistrarLogin.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String user = request.getParameter("registrarNumber");
		String password = request.getParameter("registrarPassword"); // Getting parameters from View Student Login Form

		boolean isFacultyUserValid = registrarValidationService.isRegistrarValid(user, password); // user and password
																									// (refer to the
																									// Validation
																									// Service)
		// boolean isFacultyUserValid =
		// facultyValidationService.isFacultyValid(employeeNumber, employeePassword);

		if (isFacultyUserValid) {
			request.getSession().setAttribute("employeeNumber", user);// var and argument
			request.getSession().setAttribute("role", "registrar");
			response.sendRedirect("/ListApprove.com"); // Dapat Approve Petition Servlet to. To be changed
		} else {
			request.setAttribute("errorMessage", "Invalid Credentials");
			request.getRequestDispatcher("/WEB-INF/view/RegistrarLogin.jsp").forward(request, response);
		}

	}
}
