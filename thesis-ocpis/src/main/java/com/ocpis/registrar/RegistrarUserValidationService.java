package com.ocpis.registrar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class RegistrarUserValidationService {
	public boolean isRegistrarValid(String user, String password) {
		boolean status = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null; // Dapat ang checking sa faculty
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("SELECT * FROM Registrar WHERE registrarID=? AND password=?"); // Dapat sa
																										// registrar
																										// folder
			ps.setString(1, user);
			ps.setString(2, password);
			rs = ps.executeQuery();
			status = rs.next();
		} catch (Exception e) {
			System.out.println(e);
		}

		return status;
	}

}
