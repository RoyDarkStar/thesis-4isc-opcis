package com.ocpis.joined;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ocpis.petition.PetitionService;

/**
 * Servlet implementation class RemoveJoined
 */
@WebServlet("/RemoveJoined.com")
public class RemoveJoined extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String studentNumber = (String) session.getAttribute("studentNumber");
		String role = (String) session.getAttribute("role");

		String petitionID = (String) request.getParameter("PetitionIDBean");

		PetitionService petitionService = new PetitionService();
		RemoveServices remove = new RemoveServices();

		if (role.equals("student")) {
			// petitionService.deletePetitionStatus(petitionID);
			remove.deletePetitionMember(petitionID, studentNumber);
			// petitionService.deletePetitionSchedule(petitionID);
			// petitionService.deletePetition(petitionID);

			petitionService.invalidateBean();

			// request.setAttribute("petition",
			// petitionService.retrievePetition(studentNumber, role));
			// request.getRequestDispatcher("/WEB-INF/view/StudentCreatedPetition.jsp").forward(request,
			// response);
			response.sendRedirect("JoinedServlet.com");
		} else {

		}
	}

}
