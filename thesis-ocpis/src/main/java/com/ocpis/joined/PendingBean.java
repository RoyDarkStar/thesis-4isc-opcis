package com.ocpis.joined;

public class PendingBean {

	private String petitionID;
	private String course;
	private String professorName;
	private String slot;

	public PendingBean(String petitionID, String course, String professorName, String slot) {
		super();
		this.petitionID = petitionID;
		this.course = course;
		this.professorName = professorName;
		this.slot = slot;
	}

	/**
	 * @return the petitionID
	 */
	public String getPetitionID() {
		return petitionID;
	}

	/**
	 * @param petitionID
	 *            the petitionID to set
	 */
	public void setPetitionID(String petitionID) {
		this.petitionID = petitionID;
	}

	/**
	 * @return the course
	 */
	public String getCourse() {
		return course;
	}

	/**
	 * @param course
	 *            the course to set
	 */
	public void setCourse(String course) {
		this.course = course;
	}

	/**
	 * @return the professorName
	 */
	public String getProfessorName() {
		return professorName;
	}

	/**
	 * @param professorName
	 *            the professorName to set
	 */
	public void setProfessorName(String professorName) {
		this.professorName = professorName;
	}

	/**
	 * @return the slot
	 */
	public String getSlot() {
		return slot;
	}

	/**
	 * @param slot
	 *            the slot to set
	 */
	public void setSlot(String slot) {
		this.slot = slot;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((course == null) ? 0 : course.hashCode());
		result = prime * result + ((petitionID == null) ? 0 : petitionID.hashCode());
		result = prime * result + ((professorName == null) ? 0 : professorName.hashCode());
		result = prime * result + ((slot == null) ? 0 : slot.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PendingBean other = (PendingBean) obj;
		if (course == null) {
			if (other.course != null)
				return false;
		} else if (!course.equals(other.course))
			return false;
		if (petitionID == null) {
			if (other.petitionID != null)
				return false;
		} else if (!petitionID.equals(other.petitionID))
			return false;
		if (professorName == null) {
			if (other.professorName != null)
				return false;
		} else if (!professorName.equals(other.professorName))
			return false;
		if (slot == null) {
			if (other.slot != null)
				return false;
		} else if (!slot.equals(other.slot))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("PendingBean [petitionID=%s, course=%s, professorName=%s, slot=%s]", petitionID, course,
				professorName, slot);
	}

}
