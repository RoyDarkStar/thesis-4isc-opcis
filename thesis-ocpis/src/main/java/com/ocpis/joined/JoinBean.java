package com.ocpis.joined;

public class JoinBean {
	private String petitionID;
	private String course;
	private String professorName;
	private String deptChairRemarks;
	private String deptChairTimelog;
	private String deanRemarks;
	private String deanTimelog;
	private String registrarRemarks;
	private String registrarTimelog;
	private String OVRAATimelog;
	private String ovraaRemarks;
	private String accountingTimelog;
	private String accountingRemarks;
	private String slot;

	public JoinBean(String petitionID, String course, String professorName, String deptChairRemarks,
			String deptChairTimelog, String deanRemarks, String deanTimelog, String registrarRemarks,
			String registrarTimelog, String oVRAATimelog, String ovraaRemarks, String accountingTimelog,
			String accountingRemarks, String slot) {
		super();
		this.petitionID = petitionID;
		this.course = course;
		this.professorName = professorName;
		this.deptChairRemarks = deptChairRemarks;
		this.deptChairTimelog = deptChairTimelog;
		this.deanRemarks = deanRemarks;
		this.deanTimelog = deanTimelog;
		this.registrarRemarks = registrarRemarks;
		this.registrarTimelog = registrarTimelog;
		OVRAATimelog = oVRAATimelog;
		this.ovraaRemarks = ovraaRemarks;
		this.accountingTimelog = accountingTimelog;
		this.accountingRemarks = accountingRemarks;
		this.slot = slot;
	}

	/**
	 * @return the petitionID
	 */
	public String getPetitionID() {
		return petitionID;
	}

	/**
	 * @param petitionID
	 *            the petitionID to set
	 */
	public void setPetitionID(String petitionID) {
		this.petitionID = petitionID;
	}

	/**
	 * @return the course
	 */
	public String getCourse() {
		return course;
	}

	/**
	 * @param course
	 *            the course to set
	 */
	public void setCourse(String course) {
		this.course = course;
	}

	/**
	 * @return the professorName
	 */
	public String getProfessorName() {
		return professorName;
	}

	/**
	 * @param professorName
	 *            the professorName to set
	 */
	public void setProfessorName(String professorName) {
		this.professorName = professorName;
	}

	/**
	 * @return the deptChairRemarks
	 */
	public String getDeptChairRemarks() {
		return deptChairRemarks;
	}

	/**
	 * @param deptChairRemarks
	 *            the deptChairRemarks to set
	 */
	public void setDeptChairRemarks(String deptChairRemarks) {
		this.deptChairRemarks = deptChairRemarks;
	}

	/**
	 * @return the deptChairTimelog
	 */
	public String getDeptChairTimelog() {
		return deptChairTimelog;
	}

	/**
	 * @param deptChairTimelog
	 *            the deptChairTimelog to set
	 */
	public void setDeptChairTimelog(String deptChairTimelog) {
		this.deptChairTimelog = deptChairTimelog;
	}

	/**
	 * @return the deanRemarks
	 */
	public String getDeanRemarks() {
		return deanRemarks;
	}

	/**
	 * @param deanRemarks
	 *            the deanRemarks to set
	 */
	public void setDeanRemarks(String deanRemarks) {
		this.deanRemarks = deanRemarks;
	}

	/**
	 * @return the deanTimelog
	 */
	public String getDeanTimelog() {
		return deanTimelog;
	}

	/**
	 * @param deanTimelog
	 *            the deanTimelog to set
	 */
	public void setDeanTimelog(String deanTimelog) {
		this.deanTimelog = deanTimelog;
	}

	/**
	 * @return the registrarRemarks
	 */
	public String getRegistrarRemarks() {
		return registrarRemarks;
	}

	/**
	 * @param registrarRemarks
	 *            the registrarRemarks to set
	 */
	public void setRegistrarRemarks(String registrarRemarks) {
		this.registrarRemarks = registrarRemarks;
	}

	/**
	 * @return the registrarTimelog
	 */
	public String getRegistrarTimelog() {
		return registrarTimelog;
	}

	/**
	 * @param registrarTimelog
	 *            the registrarTimelog to set
	 */
	public void setRegistrarTimelog(String registrarTimelog) {
		this.registrarTimelog = registrarTimelog;
	}

	/**
	 * @return the oVRAATimelog
	 */
	public String getOVRAATimelog() {
		return OVRAATimelog;
	}

	/**
	 * @param oVRAATimelog
	 *            the oVRAATimelog to set
	 */
	public void setOVRAATimelog(String oVRAATimelog) {
		OVRAATimelog = oVRAATimelog;
	}

	/**
	 * @return the ovraaRemarks
	 */
	public String getOvraaRemarks() {
		return ovraaRemarks;
	}

	/**
	 * @param ovraaRemarks
	 *            the ovraaRemarks to set
	 */
	public void setOvraaRemarks(String ovraaRemarks) {
		this.ovraaRemarks = ovraaRemarks;
	}

	/**
	 * @return the accountingTimelog
	 */
	public String getAccountingTimelog() {
		return accountingTimelog;
	}

	/**
	 * @param accountingTimelog
	 *            the accountingTimelog to set
	 */
	public void setAccountingTimelog(String accountingTimelog) {
		this.accountingTimelog = accountingTimelog;
	}

	/**
	 * @return the accountingRemarks
	 */
	public String getAccountingRemarks() {
		return accountingRemarks;
	}

	/**
	 * @param accountingRemarks
	 *            the accountingRemarks to set
	 */
	public void setAccountingRemarks(String accountingRemarks) {
		this.accountingRemarks = accountingRemarks;
	}

	/**
	 * @return the slot
	 */
	public String getSlot() {
		return slot;
	}

	/**
	 * @param slot
	 *            the slot to set
	 */
	public void setSlot(String slot) {
		this.slot = slot;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((OVRAATimelog == null) ? 0 : OVRAATimelog.hashCode());
		result = prime * result + ((accountingRemarks == null) ? 0 : accountingRemarks.hashCode());
		result = prime * result + ((accountingTimelog == null) ? 0 : accountingTimelog.hashCode());
		result = prime * result + ((course == null) ? 0 : course.hashCode());
		result = prime * result + ((deanRemarks == null) ? 0 : deanRemarks.hashCode());
		result = prime * result + ((deanTimelog == null) ? 0 : deanTimelog.hashCode());
		result = prime * result + ((deptChairRemarks == null) ? 0 : deptChairRemarks.hashCode());
		result = prime * result + ((deptChairTimelog == null) ? 0 : deptChairTimelog.hashCode());
		result = prime * result + ((ovraaRemarks == null) ? 0 : ovraaRemarks.hashCode());
		result = prime * result + ((petitionID == null) ? 0 : petitionID.hashCode());
		result = prime * result + ((professorName == null) ? 0 : professorName.hashCode());
		result = prime * result + ((registrarRemarks == null) ? 0 : registrarRemarks.hashCode());
		result = prime * result + ((registrarTimelog == null) ? 0 : registrarTimelog.hashCode());
		result = prime * result + ((slot == null) ? 0 : slot.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JoinBean other = (JoinBean) obj;
		if (OVRAATimelog == null) {
			if (other.OVRAATimelog != null)
				return false;
		} else if (!OVRAATimelog.equals(other.OVRAATimelog))
			return false;
		if (accountingRemarks == null) {
			if (other.accountingRemarks != null)
				return false;
		} else if (!accountingRemarks.equals(other.accountingRemarks))
			return false;
		if (accountingTimelog == null) {
			if (other.accountingTimelog != null)
				return false;
		} else if (!accountingTimelog.equals(other.accountingTimelog))
			return false;
		if (course == null) {
			if (other.course != null)
				return false;
		} else if (!course.equals(other.course))
			return false;
		if (deanRemarks == null) {
			if (other.deanRemarks != null)
				return false;
		} else if (!deanRemarks.equals(other.deanRemarks))
			return false;
		if (deanTimelog == null) {
			if (other.deanTimelog != null)
				return false;
		} else if (!deanTimelog.equals(other.deanTimelog))
			return false;
		if (deptChairRemarks == null) {
			if (other.deptChairRemarks != null)
				return false;
		} else if (!deptChairRemarks.equals(other.deptChairRemarks))
			return false;
		if (deptChairTimelog == null) {
			if (other.deptChairTimelog != null)
				return false;
		} else if (!deptChairTimelog.equals(other.deptChairTimelog))
			return false;
		if (ovraaRemarks == null) {
			if (other.ovraaRemarks != null)
				return false;
		} else if (!ovraaRemarks.equals(other.ovraaRemarks))
			return false;
		if (petitionID == null) {
			if (other.petitionID != null)
				return false;
		} else if (!petitionID.equals(other.petitionID))
			return false;
		if (professorName == null) {
			if (other.professorName != null)
				return false;
		} else if (!professorName.equals(other.professorName))
			return false;
		if (registrarRemarks == null) {
			if (other.registrarRemarks != null)
				return false;
		} else if (!registrarRemarks.equals(other.registrarRemarks))
			return false;
		if (registrarTimelog == null) {
			if (other.registrarTimelog != null)
				return false;
		} else if (!registrarTimelog.equals(other.registrarTimelog))
			return false;
		if (slot == null) {
			if (other.slot != null)
				return false;
		} else if (!slot.equals(other.slot))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"JoinBean [petitionID=%s, course=%s, professorName=%s, deptChairRemarks=%s, deptChairTimelog=%s, deanRemarks=%s, deanTimelog=%s, registrarRemarks=%s, registrarTimelog=%s, OVRAATimelog=%s, ovraaRemarks=%s, accountingTimelog=%s, accountingRemarks=%s, slot=%s]",
				petitionID, course, professorName, deptChairRemarks, deptChairTimelog, deanRemarks, deanTimelog,
				registrarRemarks, registrarTimelog, OVRAATimelog, ovraaRemarks, accountingTimelog, accountingRemarks,
				slot);
	}

}
