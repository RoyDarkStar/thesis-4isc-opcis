package com.ocpis.joined;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.ocpis.approve.ApproveService;

public class JoinedService {
	private static List<JoinBean> join = new ArrayList<JoinBean>();
	private ApproveService service = new ApproveService();

	public List<JoinBean> retrieveJoin(String studentNumber) {
		// Ang explanation neto is, Binabato ko kay Getters etong mga petition.add (Yung
		// nakalagay sa array
		// Tas after kunin ni Getters, Ireretrieve ko using setters then return the
		// array list

		String petitionID;
		String petitionCourse;
		String facultyName;
		String first;
		String middle;
		String last;
		String deptChairRemarks;
		String deptChairTimelog;
		String deanTimelog;
		String deanRemarks;
		String registrarTimelog;
		String registrarRemarks;
		String OVRAATimelog;
		String ovraaRemarks;
		String accountingRemarks;
		String accountingTimelog;
		String slot;

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			/*
			 * "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement(
					"SELECT * FROM [PetitionMembers]M" + " JOIN [Petition]P ON M.petitionID = P.petitionID"
							+ " JOIN [Faculty]F ON P.facultyID = F.facultyID"
							+ " JOIN [PetitionStatus]A ON P.petitionID = A.petitionID" + " WHERE M.studentID=?");
			ps.setString(1, studentNumber);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {

				petitionID = rs.getString("petitionID");
				petitionCourse = rs.getString("petitionCourse");
				first = rs.getString("firstName");
				middle = rs.getString("middleName");
				last = rs.getString("lastName");
				facultyName = first + " " + middle + " " + last;
				deptChairTimelog = rs.getString("chairTimelog");
				deptChairRemarks = rs.getString("deptChairRemarks");
				deanTimelog = rs.getString("deanTimelog");
				deanRemarks = rs.getString("deanRemarks");
				registrarTimelog = rs.getString("registrarTimelog");
				registrarRemarks = rs.getString("registrarRemarks");
				ovraaRemarks = rs.getString("ovraaRemarks");
				OVRAATimelog = rs.getString("ovraaTimelog");
				accountingRemarks = rs.getString("accountingRemarks");
				accountingTimelog = rs.getString("accountingTimelog");
				slot = service.countPetitionMembers(petitionID);

				join.add(new JoinBean(petitionID, petitionCourse, facultyName, deptChairRemarks, deptChairTimelog,
						deanRemarks, deanTimelog, registrarRemarks, registrarTimelog, ovraaRemarks, OVRAATimelog,
						accountingRemarks, accountingTimelog, slot));

			}
			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return join;
	}

	public void addJoinBean(JoinBean addJoin) { // Kinukuha ko yung form ng addPetition sa StudentPetitionJSP
		join.add(addJoin);
	}

	public void deleteJoinBean(JoinBean deleteJoin) {
		join.remove(deleteJoin);
	}

	public void invalidateJoinBean() {
		join.clear();
	}
}
