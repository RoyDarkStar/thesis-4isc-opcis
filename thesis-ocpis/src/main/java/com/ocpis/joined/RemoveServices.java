package com.ocpis.joined;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class RemoveServices {

	public void deletePetitionMember(String petitionID, String studentID) {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con
					.prepareStatement("DELETE FROM [PetitionMembers] WHERE petitionID=? AND studentID=?");
			ps.setString(1, petitionID);
			ps.setString(2, studentID);
			ps.executeUpdate();

			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
