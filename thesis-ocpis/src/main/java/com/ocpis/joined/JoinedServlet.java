package com.ocpis.joined;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ocpis.petition.PetitionService;

@WebServlet(urlPatterns = "/JoinedServlet.com")
public class JoinedServlet extends HttpServlet {
	private JoinedService createService = new JoinedService(); // class name ng petitionService
	private PetitionService petitionService = new PetitionService(); // class name ng petitionService
	private PendingService pending = new PendingService();

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String studentNumber = (String) session.getAttribute("studentNumber");
		String role = (String) session.getAttribute("role");

		createService.invalidateJoinBean();
		petitionService.invalidateBean();
		pending.invalidatePending();

		request.setAttribute("pending", pending.retrieveJoin(studentNumber));
		request.setAttribute("join", createService.retrieveJoin(studentNumber));
		request.setAttribute("notif", petitionService.retrieveNotification(studentNumber, role));
		request.getRequestDispatcher("/WEB-INF/view/StudentJoined.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
