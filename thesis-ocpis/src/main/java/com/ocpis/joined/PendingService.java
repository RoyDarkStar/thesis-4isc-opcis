package com.ocpis.joined;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.ocpis.approve.ApproveService;

public class PendingService {
	private static List<PendingBean> pending = new ArrayList<PendingBean>();
	private ApproveService service = new ApproveService();

	public List<PendingBean> retrieveJoin(String studentNumber) {
		// Ang explanation neto is, Binabato ko kay Getters etong mga petition.add (Yung
		// nakalagay sa array
		// Tas after kunin ni Getters, Ireretrieve ko using setters then return the
		// array list

		String petitionID;
		String petitionCourse;
		String facultyName;
		String slot;

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			/*
			 * "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement(
					"SELECT * FROM [PetitionMembers]M" + " JOIN [Petition]P ON M.petitionID = P.petitionID"
							+ " JOIN [Faculty]F ON P.facultyID = F.facultyID" + " WHERE M.studentID=?"
							+ " AND P.petitionID NOT IN (SELECT S.petitionID FROM [PetitionStatus]S);");
			ps.setString(1, studentNumber);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				petitionID = rs.getString("petitionID");
				petitionCourse = rs.getString("petitionCourse");
				String first = rs.getString("firstName");
				String middle = rs.getString("middleName");
				String last = rs.getString("lastName");
				facultyName = first + " " + middle + " " + last;
				slot = service.countPetitionMembers(petitionID);

				pending.add(new PendingBean(petitionID, petitionCourse, facultyName, slot));

			}
			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return pending;
	}

	public void invalidatePending() {
		pending.clear();
	}

}
