package com.ocpis.office;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.ocpis.petition.PetitionService;

@WebServlet(urlPatterns = "/OfficeLogin.com")
public class OfficeLoginServlet extends HttpServlet {

	private OfficeUserValidationService facultyValidationService = new OfficeUserValidationService(); // class name ng
																										// userValidService
																										// for Students
	// private ApproveService petitionService = new ApproveService(); //class name
	// ng petitionService

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/view/OfficeLogin.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String employeeNumber = request.getParameter("employeeNumber");
		String employeePassword = request.getParameter("employeePassword"); // Getting parameters from View Student
																			// Login Form

		boolean isFacultyUserValid = facultyValidationService.isFacultyValid(employeeNumber, employeePassword); // user
																												// and
																												// password
																												// (refer
																												// to
																												// the
																												// Validation
																												// Service)
		// boolean isFacultyUserValid =
		// facultyValidationService.isFacultyValid(employeeNumber, employeePassword);

		if (isFacultyUserValid) {
			request.getSession().setAttribute("employeeNumber", employeeNumber);// var and argument
			request.getSession().setAttribute("role", "office");
			response.sendRedirect("/ListApprove.com");
		} else {
			request.setAttribute("errorMessage", "Invalid Credentials");
			request.getRequestDispatcher("/WEB-INF/view/OVRAALogin.jsp").forward(request, response);
		}

	}
}
