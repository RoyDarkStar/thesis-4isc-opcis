package com.ocpis.petition;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/AddPetition.com")
public class PetitionAddServlet extends HttpServlet {

	private PetitionService petitionService = new PetitionService(); // class name ng petitionService
	// if

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String studentNumber = (String) session.getAttribute("studentNumber");
		String role = (String) session.getAttribute("role");
		PetitionService petitionService = new PetitionService();
		// request.setAttribute("notif",
		// petitionService.retrieveNotification(studentNumber, role));
		// System.out.println(studentNumber);
		if (role.equals("student")) {
			petitionService.invalidateBean();
			request.setAttribute("notif", petitionService.retrieveNotification(studentNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/StudentAddPetition.jsp").forward(request, response);

		} else if (role.equals("faculty")) {
			request.getRequestDispatcher("/WEB-INF/view/FacultyAddPetition.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String newCourse = request.getParameter("addingCourse");
		String validity = request.getParameter("validity");
		// System.out.println(newCourse);
		// String newProfessor = request.getParameter("addingProfessor");
		String day = request.getParameter("addingDay");
		// String timeStart = request.getParameter("addingTimeStart");
		// String timeEnd = request.getParameter("addingTimeEnd");
		String room = request.getParameter("addingRoom");
		HttpSession session = request.getSession();
		String role = (String) session.getAttribute("role");
		String studentNumber = (String) session.getAttribute("studentNumber");
		String programID = (String) session.getAttribute("studentProgram");
		String facultyID = request.getParameter("addingProfessor");
		petitionService.invalidateBean();

		/*
		 * petitionService.addPetitionBean(new PetitionBean(newCourse, newProfessor));
		 * request.setAttribute("petition",
		 * petitionService.retrievePetition(studentNumber));
		 */

		if (role.equals("student")) {
			if (petitionService.verifyAddPetition(studentNumber, newCourse)) {
				request.setAttribute("errorMessage",
						"User is already joined to a petition class with the same course description");
				// request.setAttribute("petition",
				// petitionService.retrievePetition(studentNumber, role));
				request.getRequestDispatcher("/WEB-INF/view/StudentAddPetition.jsp").forward(request, response);
			} else {
				if (petitionService.verifyAdvised(studentNumber, newCourse)) {
					request.setAttribute("errorMessage", "Student is already advised to this course");
					request.getRequestDispatcher("/WEB-INF/view/StudentAddPetition.jsp").forward(request, response);
				} else {
					if (petitionService.verifyPassed(studentNumber, newCourse)) {
						request.setAttribute("errorMessage", "Student already completed that course");
						request.getRequestDispatcher("/WEB-INF/view/StudentAddPetition.jsp").forward(request, response);
					} else {
						if (petitionService.verifyPrerequisite(studentNumber, newCourse, programID)) {
							session.setAttribute("createdPetition", newCourse);
							petitionService.addPetitionBeanStudent(newCourse, facultyID, studentNumber, validity);
							petitionService.addPetitionMember(studentNumber, newCourse);
							// petitionService.addPetitionStatus(studentNumber, newCourse);
							response.sendRedirect("/AddPetitionSchedule.com");
						} else {
							request.setAttribute("errorMessage",
									"Student has not completed this course's prerequisite/s");
							request.getRequestDispatcher("/WEB-INF/view/StudentAddPetition.jsp").forward(request,
									response);
						}
					}

					/*
					 * //Checking ng time start and time end kapag parehas mageerror
					 * if(petitionService.verifyTimeframe(timeStart, timeEnd)){ //Checking kung
					 * conflict sa iba pa nyang ginawa na petition
					 * if(petitionService.verifySchedule(timeStart, timeEnd, studentNumber)){
					 * System.out.
					 * println("Time selected is conflict with other created petition class");
					 * request.getRequestDispatcher("/WEB-INF/view/StudentAddPetition.jsp").forward(
					 * request, response); }else{ petitionService.addPetitionBeanStudent(newCourse,
					 * facultyID, studentNumber);
					 * //petitionService.addPetitionSchedule(studentNumber, newCourse, day,
					 * timeStart, timeEnd, room); //petitionService.addPetitionMember(studentNumber,
					 * newCourse); //petitionService.addPetitionStatus(studentNumber, newCourse);
					 * request.setAttribute("petition",
					 * petitionService.retrievePetition(studentNumber, role));
					 * request.getRequestDispatcher("/WEB-INF/view/StudentListPetition.jsp").forward
					 * (request, response); }
					 * 
					 * } else{ request.setAttribute("errorMessage", "Invalid Timeframe");
					 * request.getRequestDispatcher("/WEB-INF/view/StudentAddPetition.jsp").forward(
					 * request, response); }
					 */
				}
			}
		} else if (role.equals("faculty")) {
			request.getRequestDispatcher("/WEB-INF/view/FacultyListPetition.jsp").forward(request, response);
		}
		// request.getRequestDispatcher("/WEB-INF/view/StudentListPetition.jsp").forward(request,
		// response);
	}
}