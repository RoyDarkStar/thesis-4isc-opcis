package com.ocpis.petition;

public class NotificationBean {
	private String timelog;
	private String message;

	public NotificationBean(String timelog, String message) {
		super();
		this.timelog = timelog;
		this.message = message;
	}

	public String getTimelog() {
		return timelog;
	}

	public void setTimelog(String timelog) {
		this.timelog = timelog;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return String.format("NotificationBean [timelog=%s, message=%s]", timelog, message);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((timelog == null) ? 0 : timelog.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotificationBean other = (NotificationBean) obj;
		if (timelog == null) {
			if (other.timelog != null)
				return false;
		} else if (!timelog.equals(other.timelog))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		return true;
	}
}
