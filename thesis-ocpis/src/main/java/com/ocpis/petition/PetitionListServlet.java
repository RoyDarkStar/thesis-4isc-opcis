package com.ocpis.petition;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ocpis.student.MemberService;

@WebServlet(urlPatterns = "/ListPetition.com")
public class PetitionListServlet extends HttpServlet {

	// Petition Service from bean
	private PetitionService petitionService = new PetitionService(); // class name ng petitionService
	private MemberService memberService = new MemberService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String studentNumber = (String) session.getAttribute("studentNumber");
		String studentProgram = (String) session.getAttribute("studentProgram");
		String studentYear = (String) session.getAttribute("studentYear");
		String employeeNumber = (String) session.getAttribute("employeeNumber");
		String petitionID = (String) session.getAttribute("petitionIDBean");

		PetitionService petitionService = new PetitionService();
		MemberService memberService = new MemberService();
		String role = (String) session.getAttribute("role");
		// request.getRequestDispatcher("/WEB-INF/view/StudentListPetition.jsp").forward(request,
		// response);
		if (role.equals("student")) {
			petitionService.invalidateBean();
			memberService.invalidateMember();
			request.setAttribute("petition",
					petitionService.retrievePetition(studentNumber, studentProgram, studentYear, role));
			request.setAttribute("notif", petitionService.retrieveNotification(studentNumber, role));
			request.setAttribute("member", memberService.retrieveMember(petitionID));
			request.getRequestDispatcher("/WEB-INF/view/StudentListPetition.jsp").forward(request, response);
		}

		else if (role.equals("faculty")) {
			petitionService.invalidateBean();
			// request.setAttribute("petition",
			// petitionService.retrievePetition(employeeNumber, role));
			request.getRequestDispatcher("/WEB-INF/view/FacultyListPetition.jsp").forward(request, response);
		}
	}
}