package com.ocpis.petition;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = "/PetitionDelete.com")
public class PetitionDelete extends HttpServlet {

	private PetitionService petitionService = new PetitionService(); // class name ng petitionService

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*
		 * petitionService.deletePetitionBean(new
		 * PetitionBean(request.getParameter("PetitionBean") ,
		 * request.getParameter("CourseBean") ));
		 */
		HttpSession session = request.getSession();
		String studentNumber = (String) session.getAttribute("studentNumber");
		String role = (String) session.getAttribute("role");
		String petitionID = (String) request.getParameter("PetitionIDBean");

		PetitionService petitionService = new PetitionService();

		if (role.equals("student")) {
			// petitionService.deletePetitionStatus(petitionID);
			petitionService.deletePetitionMembers(petitionID);
			petitionService.deletePetitionSchedule(petitionID);
			petitionService.deletePetition(petitionID);

			petitionService.invalidateBean();

			// request.setAttribute("petition",
			// petitionService.retrievePetition(studentNumber, role));
			// request.getRequestDispatcher("/WEB-INF/view/StudentCreatedPetition.jsp").forward(request,
			// response);
			response.sendRedirect("CreatedServlet.com");
		} else {

		}

		// response.sendRedirect("/ListPetition.com");
	}
}