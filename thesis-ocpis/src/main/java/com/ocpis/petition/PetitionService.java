
package com.ocpis.petition;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ocpis.approve.ApproveService;

public class PetitionService {
	private static List<PetitionBean> petition = new ArrayList<PetitionBean>();
	private static List<NotificationBean> notif = new ArrayList<NotificationBean>();
	private static List<SearchBean> search = new ArrayList<SearchBean>();
	private ApproveService service = new ApproveService();
	// Stinatic ko lang siya kasi wala akong db
	/*
	 * static { petition.add(new PetitionBean("ICS100", "IICS")); petition.add(new
	 * PetitionBean("ICS110", "IICS")); petition.add(new PetitionBean("ICS111",
	 * "IICS")); petition.add(new PetitionBean("ICS200", "IICS")); petition.add(new
	 * PetitionBean("ICS210", "IICS")); petition.add(new PetitionBean("ICS211",
	 * "IICS")); }
	 */

	public List<PetitionBean> retrievePetition(String id, String program, String year, String role) {
		// Ang explanation neto is, Binabato ko kay Getters etong mga petition.add (Yung
		// nakalagay sa array
		// Tas after kunin ni Getters, Ireretrieve ko using setters then return the
		// array list
		String petitionCourse;
		String professorFirstName;
		String professorMiddleName;
		String professorLastName;
		String professorName;
		String petitionID;
		String slot;
		String validity;

		if (role.equals("student")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

				// Roy
				/*
				 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
				 * "databaseName=OPCIS;user=sa;password=April241997;";
				 */

				// Lugs
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement("SELECT * FROM [Petition]P"
						+ " JOIN [Course]C ON P.petitionCourse=C.courseName"
						+ " JOIN [Faculty]F ON P.facultyID=F.facultyID" + " WHERE C.programID=? AND C.year<=? AND"
						+ " P.studentID!=?" + " AND P.petitionID NOT IN (SELECT S.petitionID FROM [PetitionStatus]S)");
				ps.setString(1, program);
				ps.setString(2, year);
				ps.setString(3, id);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionCourse = rs.getString("petitionCourse");
					professorFirstName = rs.getString("firstName");
					professorMiddleName = rs.getString("middleName");
					professorLastName = rs.getString("lastName");
					professorName = professorFirstName + " " + professorMiddleName + " " + professorLastName;
					petitionID = rs.getString("petitionID");
					slot = service.countPetitionMembers(petitionID);
					validity = rs.getString("validity");

					petition.add(new PetitionBean(petitionCourse, professorName, petitionID, slot, validity));

				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (role.equals("faculty")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();

				// Roy
				/*
				 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
				 * "databaseName=OPCIS;user=sa;password=April241997;";
				 */

				// Lugs
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement("SELECT * FROM [Petition]p  WHERE d.chairID=?");
				ps.setString(1, id);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					petitionCourse = rs.getString("petitionCourse");
					petitionID = rs.getString("petitionID");
					String facultyID = rs.getString("facultyID");
					professorName = retrieveFacultyName(facultyID);
					slot = service.countPetitionMembers(petitionID);
					validity = rs.getString("validity");

					petition.add(new PetitionBean(petitionCourse, professorName, petitionID, slot, validity));

				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return petition;
	}

	// For static values
	/*
	 * public void addPetitionBean (PetitionBean addPetition) { //Kinukuha ko yung
	 * form ng addPetition sa StudentPetitionJSP petition.add(addPetition); }
	 */

	public void addPetitionBeanStudent(String petitionCourse, String facultyID, String studentID, String validity) { // Kinukuha
																														// ko
																														// yung
																														// form
																														// ng
																														// addPetition
																														// sa
																														// StudentPetitionJSP
		String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement(
					"INSERT INTO [Petition](petitionCourse, facultyID, studentID, timelog, validity) VALUES (?, ?, ?, ?, ?)");
			ps.setString(1, petitionCourse);
			ps.setString(2, facultyID);
			ps.setString(3, studentID);
			ps.setString(4, timestamp);
			ps.setString(5, validity);
			ps.executeUpdate();
			System.out.println("DB updated");

			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public String retrievePetitionID(String studentID, String petitionCourse) {
		String petitionID = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con
					.prepareStatement("SELECT * FROM [Petition] WHERE studentID=? AND petitionCourse=?");
			ps.setString(1, studentID);
			ps.setString(2, petitionCourse);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				petitionID = rs.getString("petitionID");

			}
			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return petitionID;
	}

	public void addPetitionSchedule(String studentID, String petitionID, String day, String timeStart, String timeEnd,
			String room) {
		// String petitionCode = retrievePetitionID(studentID, petitionCourse);
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement(
					"INSERT INTO [PetitionSchedule](petitionID, day, timeStart, timeEnd, room) VALUES (?, ?, ?, ?, ?)");
			ps.setString(1, petitionID);
			ps.setString(2, day);
			ps.setString(3, timeStart);
			ps.setString(4, timeEnd);
			ps.setString(5, room);
			ps.executeUpdate();
			System.out.println("DB updated");

			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void addPetitionMember(String studentID, String petitionCourse) {
		String petitionCode = retrievePetitionID(studentID, petitionCourse);
		String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con
					.prepareStatement("INSERT INTO [PetitionMembers](petitionID, studentID, timelog) VALUES (?, ?, ?)");
			ps.setString(1, petitionCode);
			ps.setString(2, studentID);
			ps.setString(3, timestamp);
			ps.executeUpdate();
			System.out.println("DB updated");

			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void addPetitionStatus(String petitionID) {
		// String petitionCode = retrievePetitionID(studentID, petitionCourse);
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=qwertyasi;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement("INSERT INTO [PetitionStatus](petitionID) VALUES (?)");
			ps.setString(1, petitionID);
			ps.executeUpdate();
			System.out.println("DB updated");

			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	// For deleting static values
	/*
	 * public void deletePetitionBean(PetitionBean deletePetition) {
	 * petition.remove(deletePetition); }
	 */

	public void deletePetition(String petitionID) {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement("DELETE FROM [Petition] WHERE petitionID=?");
			ps.setString(1, petitionID);
			ps.executeUpdate();

			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void deletePetitionMembers(String petitionID) {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement("DELETE FROM [PetitionMembers] WHERE petitionID=?");
			ps.setString(1, petitionID);
			ps.executeUpdate();

			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void deletePetitionSchedule(String petitionID) {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement("DELETE FROM [PetitionSchedule] WHERE petitionID=?");
			ps.setString(1, petitionID);
			ps.executeUpdate();

			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void deletePetitionStatus(String petitionID) {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=qwertyasi;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement("DELETE FROM [PetitionStatus] WHERE petitionID=?");
			ps.setString(1, petitionID);
			ps.executeQuery();

			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public boolean verifyAddPetition(String studentNumber, String courseName) {
		boolean status = false;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=qwertyasi;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement(
					"SELECT * FROM [PetitionMembers]m JOIN [Petition]p ON m.petitionID = p.petitionID where m.studentID=? AND p.petitionCourse=?");
			ps.setString(1, studentNumber);
			ps.setString(2, courseName);
			ResultSet rs = ps.executeQuery();
			status = rs.next();

			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public boolean verifyTimeframe(String timeStart, String timeEnd) {
		boolean status = false;
		String[] start = timeStart.split(":");
		String[] end = timeEnd.split(":");

		int timeStartInt = Integer.parseInt(start[0]);
		int timeEndInt = Integer.parseInt(end[0]);
		if (!timeStart.equals(timeEnd)) {
			if (timeStartInt > timeEndInt) {
				status = false;
			} else {
				status = true;
			}
		} else {
			status = false;
		}
		return status;
	}

	public void invalidateBean() {
		petition.clear();
		notif.clear();
	}

	public String retrieveFacultyName(String facultyID) {
		String professorName = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=qwertyasi;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement("SELECT * FROM [Faculty] where facultyID = ?");
			ps.setString(1, facultyID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				String firstName = rs.getString("firstName");
				String middleName = rs.getString("middleName");
				String lastName = rs.getString("lastName");
				professorName = firstName + " " + middleName + " " + lastName;
			}

			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return professorName;
	}

	public List<NotificationBean> retrieveNotification(String id, String role) {
		// Ang explanation neto is, Binabato ko kay Getters etong mga petition.add (Yung
		// nakalagay sa array
		// Tas after kunin ni Getters, Ireretrieve ko using setters then return the
		// array list

		String timelog;
		String message;

		if (role.equals("student")) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

				// Roy
				/*
				 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
				 * "databaseName=OPCIS;user=sa;password=April241997;";
				 */

				// Lugs
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";
				Connection con = DriverManager.getConnection(connectionUrl);
				PreparedStatement ps = con.prepareStatement(
						"SELECT * FROM [Notifications]n JOIN [Petition]p ON n.petitionID = p.petitionID JOIN [PetitionMembers]m ON p.petitionID = m.petitionID WHERE m.studentID = ?");
				ps.setString(1, id);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					timelog = rs.getString("timelog");
					message = rs.getString("message");
					notif.add(new NotificationBean(timelog, message));

				}
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return notif;
	}

	public List<SearchBean> searchPetition(String searchItem) {
		String courseCode;
		String courseName;

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement("SELECT * FROM [Petition] WHERE petitionCourse LIKE ?");
			ps.setString(1, searchItem + '%');
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				courseCode = rs.getString("petitionCourse");
				courseName = rs.getString("petitionID");
				search.add(new SearchBean(courseCode, courseName));

			}
			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return search;

	}

	public void invalidateSearc() {
		search.clear();
	}

	/*
	 * public boolean verifySchedule(String timeBegin, String timeFinish, String
	 * id){ String[] obtainedTimeStart; String[] obtainedTimeEnd; String[]
	 * timeStart; String[] timeEnd; int timeStartInt; int timeEndInt; int
	 * obtainedTimeStartInt; int obtainedTimeEndInt; boolean verification = false;
	 * 
	 * timeStart = timeBegin.split(":"); timeEnd = timeFinish.split(":");
	 * 
	 * timeStartInt = Integer.parseInt(timeStart[0]); timeEndInt =
	 * Integer.parseInt(timeEnd[0]); try{
	 * Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	 * 
	 * //Roy String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
	 * "databaseName=OPCIS;user=sa;password=April241997;";
	 * 
	 * //Lugs String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
	 * "databaseName=OPCIS;user=sa;password=April241997;"; Connection con =
	 * DriverManager.getConnection(connectionUrl); PreparedStatement ps = con.
	 * prepareStatement("SELECT * FROM [PetitionSchedule]S JOIN [Petition]P ON S.petitionID = P.petitionID JOIN [PetitionMembers]M ON P.petitionID = M.petitionID WHERE M.studentID=?"
	 * ); ps.setString(1, id); ResultSet rs = ps.executeQuery(); while(rs.next()){
	 * obtainedTimeStart = rs.getString("timeStart").split(":");
	 * obtainedTimeStartInt = Integer.parseInt(obtainedTimeStart[0]);
	 * obtainedTimeEnd = rs.getString("timeEnd").split(":"); obtainedTimeEndInt =
	 * Integer.parseInt(obtainedTimeEnd[0]); if(obtainedTimeStartInt == timeStartInt
	 * || timeEndInt <= obtainedTimeEndInt && timeEndInt >=obtainedTimeStartInt &&
	 * timeStartInt <= obtainedTimeEndInt && timeStartInt >=obtainedTimeStartInt){
	 * verification = true; } else{ verification = false; }
	 * 
	 * } con.close();
	 * 
	 * } catch(Exception e){ System.out.println(e); } return verification; }
	 */
	public boolean verifySchedule(String timeStart, String timeEnd, String day) {
		boolean status = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("SELECT * FROM [PetitionSchedule]S"
					+ " JOIN [Petition]P ON S.petitionID = P.petitionID" + " WHERE S.day=?"
					+ " AND ((S.timeEnd<=? AND S.timeEnd>?) OR (S.timeStart<=? AND S.timeStart>=?))");
			ps.setString(1, day);
			ps.setString(2, timeEnd);
			ps.setString(3, timeStart);
			ps.setString(4, timeEnd);
			ps.setString(5, timeStart);
			rs = ps.executeQuery();
			status = rs.next();
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public boolean verifyAdvised(String studentNumber, String courseName) {
		boolean status = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement(
					"SELECT * FROM [AdvisedStudent]A JOIN [Enrollment]E ON A.enrollmentID=E.enrollmentID "
							+ "JOIN [Course]C ON E.courseID=C.courseID WHERE A.studentID=? AND C.courseName=?");
			ps.setString(1, studentNumber);
			ps.setString(2, courseName);
			rs = ps.executeQuery();
			status = rs.next();
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public boolean verifyPassed(String studentNumber, String courseName) {
		boolean status = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("SELECT * FROM [PassedSubjects]F" + " JOIN [Course]C ON F.courseID=C.courseID"
					+ " WHERE C.courseName=? AND studentID=?");
			ps.setString(1, courseName);
			ps.setString(2, studentNumber);
			rs = ps.executeQuery();
			status = rs.next();
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public Integer countPrerequisite(String courseName, String programID) {
		int count = 0;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("SELECT COUNT(*)AS count FROM [Course]C"
					+ " JOIN [Prerequisite]P ON C.courseID=P.courseID" + " JOIN [Course]D ON D.courseID=P.prerequisite"
					+ " WHERE C.courseName=? AND C.programID=?");
			ps.setString(1, courseName);
			ps.setString(2, programID);
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt("count");
				System.out.println(count);
			}
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return count;
	}

	public Integer countPassedPrerequisite(String courseName, String programID, String studentNumber) {
		int count = 0;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("SELECT COUNT(*) AS count FROM [Course]C"
					+ " JOIN [Prerequisite]P ON C.courseID=P.courseID"
					+ " JOIN [Course]D ON D.courseID=P.prerequisite WHERE C.courseName=? AND C.programID=?"
					+ " AND D.courseName IN(SELECT DISTINCT X.courseName FROM [PassedSubjects]Z JOIN [Course]X ON Z.courseID=X.courseID WHERE Z.studentID=?)");
			ps.setString(1, courseName);
			ps.setString(2, programID);
			ps.setString(3, studentNumber);
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt("count");
				System.out.println(count);
			}
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return count;
	}

	public boolean verifyPrerequisite(String studentNumber, String courseName, String programID) {
		boolean status = false;
		int countStandard = countPrerequisite(courseName, programID);
		int countPassed = countPassedPrerequisite(courseName, programID, studentNumber);

		if (countStandard == countPassed) {
			status = true;
		} else {
			status = false;
		}

		return status;
	}
}
