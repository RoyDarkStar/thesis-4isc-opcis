package com.ocpis.petition;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/SearchServlet.com")
public class SearchServlet extends HttpServlet {

	private PetitionService petitionService = new PetitionService(); // class name ng petitionService
	// if

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		petitionService.invalidateSearc();
		String searchItem = (String) request.getParameter("searchInput");
		System.out.print(searchItem);
		// request.getRequestDispatcher("/WEB-INF/view/StudentSearchPetition.jsp").forward(request,
		// response);

		request.setAttribute("search", petitionService.searchPetition(searchItem));
		request.getRequestDispatcher("/WEB-INF/view/SearchResults.jsp").forward(request, response);
		/*
		 * petitionService.addPetitionBean(new PetitionBean(newCourse, newProfessor));
		 * request.setAttribute("petition",
		 * petitionService.retrievePetition(studentNumber));
		 */

		/*
		 * if (role.equals("student")) {
		 * if(petitionService.verifyAddPetition(studentNumber, newCourse)){ System.out.
		 * println("User is already joined to a petition class with the same course description"
		 * ); request.setAttribute("petition",
		 * petitionService.retrievePetition(studentNumber, role));
		 * request.getRequestDispatcher("/WEB-INF/view/StudentListPetition.jsp").forward
		 * (request, response); } else{ //Checking ng time start and time end kapag
		 * parehas mageerror if(petitionService.verifyTimeframe(timeStart, timeEnd)){
		 * petitionService.addPetitionBeanStudent(newCourse, facultyID, studentNumber);
		 * petitionService.addPetitionSchedule(studentNumber, newCourse, day, timeStart,
		 * timeEnd, room); petitionService.addPetitionMember(studentNumber, newCourse);
		 * petitionService.addPetitionStatus(studentNumber, newCourse);
		 * request.setAttribute("petition",
		 * petitionService.retrievePetition(studentNumber, role));
		 * request.getRequestDispatcher("/WEB-INF/view/StudentListPetition.jsp").forward
		 * (request, response); } else{ response.sendRedirect("/AddPetition.com"); } } }
		 * else if (role.equals("faculty")){
		 * request.getRequestDispatcher("/WEB-INF/view/FacultyListPetition.jsp").forward
		 * (request, response); }
		 * //request.getRequestDispatcher("/WEB-INF/view/StudentListPetition.jsp").
		 * forward(request, response); }
		 */
	}
}