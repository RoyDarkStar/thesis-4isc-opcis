package com.ocpis.petition;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AddPetitionSchedule
 */
@WebServlet("/AddPetitionSchedule.com")
public class AddPetitionSchedule extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/WEB-INF/view/StudentAddSchedule.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		PetitionService petitionService = new PetitionService();
		String studentNumber = (String) session.getAttribute("studentNumber");
		String petitionCourse = (String) session.getAttribute("createdPetition");
		String day = request.getParameter("addingDay");
		String timeStart = request.getParameter("addingTimeStart");
		String timeEnd = request.getParameter("addingTimeEnd");
		String room = request.getParameter("addingRoom");

		String petitionID = petitionService.retrievePetitionID(studentNumber, petitionCourse);

		if (petitionService.verifyTimeframe(timeStart, timeEnd)) {
			if (petitionService.verifySchedule(timeStart, timeEnd, day)) {
				request.setAttribute("errorMessage", "Time selected is conflict with other created petition class");
				request.getRequestDispatcher("/WEB-INF/view/StudentAddSchedule.jsp").forward(request, response);
			} else {
				petitionService.addPetitionSchedule(studentNumber, petitionID, day, timeStart, timeEnd, room);
				request.getRequestDispatcher("/WEB-INF/view/StudentAddSchedule.jsp").forward(request, response);
			}
		} else {
			request.setAttribute("errorMessage", "Invalid Timeframe");
			request.getRequestDispatcher("/WEB-INF/view/StudentAddSchedule.jsp").forward(request, response);
		}

	}

}
