package com.ocpis.faculty;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.ocpis.petition.PetitionService;

@WebServlet(urlPatterns = "/FacultyLogin.com")
public class FacultyLoginServlet extends HttpServlet {

	private FacultyUserValidationService facultyValidationService = new FacultyUserValidationService(); // class name ng
																										// userValidService
																										// for Students
	// private ApproveService petitionService = new ApproveService(); //class name
	// ng petitionService

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/view/FacultyLogin.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String employeeNumber = request.getParameter("employeeNumber");
		String employeePassword = request.getParameter("employeePassword"); // Getting parameters from View Student
																			// Login Form

		boolean isFacultyUserValid = facultyValidationService.isFacultyValid(employeeNumber, employeePassword); // user
																												// and
																												// password
																												// (refer
																												// to
																												// the
																												// Validation
																												// Service)
		// boolean isFacultyUserValid =
		// facultyValidationService.isFacultyValid(employeeNumber, employeePassword);

		if (isFacultyUserValid) {
			String programID = Integer
					.toString(facultyValidationService.setProgramID(employeeNumber, employeePassword));
			request.getSession().setAttribute("programID", programID);
			request.getSession().setAttribute("employeeNumber", employeeNumber);// var and argument
			request.getSession().setAttribute("role", "faculty");
			response.sendRedirect("/ListApprove.com");

			// request.setAttribute("studentNumber", studentNumber);//var and argument
			// request.setAttribute("studentPassword", studentPassword);
			// request.setAttribute("petition", petitionService.retrievePetition());
			// //Kukunin ko yung return sa ApproveService.java, PS: nilipat ko sa petition
			// Servlet
			// request.setAttribute("status", "Online");Naka comment out to kasi binabato ko
			// sa Petition Servlet hindi dito sa login
			// request.getRequestDispatcher("/WEB-INF/view/studentDashboard.jsp").forward(request,
			// response); Parang, if user is valid, babato ko data to petitionservlet
		} else {
			request.setAttribute("errorMessage", "Invalid Credentials");
			request.getRequestDispatcher("/WEB-INF/view/FacultyLogin.jsp").forward(request, response);
		}

	}
}
