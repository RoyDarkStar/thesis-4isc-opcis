package com.ocpis.faculty;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class FacultyUserValidationService {
	public boolean isFacultyValid(String user, String password) {
		boolean status = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null; // Dapat ang checking sa faculty
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			// Roy connection
			// String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			// "databaseName=OPCIS;user=sa;password=April241997;";
			// Lugs connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("SELECT * FROM DepartmentChair WHERE chairID=? AND password=?");
			ps.setString(1, user);
			ps.setString(2, password);
			rs = ps.executeQuery();
			status = rs.next();
		}

		catch (Exception e) {
			System.out.println(e);
		}

		return status;
	}

	public Integer setProgramID(String user, String password) {
		int programID = 0;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null; // Dapat ang checking sa faculty
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			// Roy connection
			// String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			// "databaseName=OPCIS;user=sa;password=April241997;";
			// Lugs connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("SELECT * FROM DepartmentChair WHERE chairID=? AND password=?");
			ps.setString(1, user);
			ps.setString(2, password);
			rs = ps.executeQuery();
			while (rs.next()) {
				programID = rs.getInt("programID");
			}
		}

		catch (Exception e) {
			System.out.println(e);
		}

		return programID;
	}

}
