package com.ocpis.faculty;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ocpis.petition.PetitionService;

//import com.ocpis.petition.PetitionService;

@WebServlet(urlPatterns = "/FacultyLogout.com")
public class FacultyLogoutServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		PetitionService petitionService = new PetitionService();

		// Clear Session
		session.invalidate();

		// Clear Bean values
		petitionService.invalidateBean();
		request.getRequestDispatcher("/WEB-INF/view/FacultyLogin.jsp").forward(request, response);
	}

}
