package com.ocpis.created;

public class InitialBean {
	String courseID;
	String courseName;
	String professorName;
	String slot;
	String validity;
	String buttonClass;
	String buttonOnclick;

	public InitialBean(String courseID, String courseName, String professorName, String slot, String validity,
			String buttonClass, String buttonOnclick) {
		super();
		this.courseID = courseID;
		this.courseName = courseName;
		this.professorName = professorName;
		this.slot = slot;
		this.validity = validity;
		this.buttonClass = buttonClass;
		this.buttonOnclick = buttonOnclick;
	}

	/**
	 * @return the courseID
	 */
	public String getCourseID() {
		return courseID;
	}

	/**
	 * @param courseID
	 *            the courseID to set
	 */
	public void setCourseID(String courseID) {
		this.courseID = courseID;
	}

	/**
	 * @return the courseName
	 */
	public String getCourseName() {
		return courseName;
	}

	/**
	 * @param courseName
	 *            the courseName to set
	 */
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	/**
	 * @return the professorName
	 */
	public String getProfessorName() {
		return professorName;
	}

	/**
	 * @param professorName
	 *            the professorName to set
	 */
	public void setProfessorName(String professorName) {
		this.professorName = professorName;
	}

	/**
	 * @return the slot
	 */
	public String getSlot() {
		return slot;
	}

	/**
	 * @param slot
	 *            the slot to set
	 */
	public void setSlot(String slot) {
		this.slot = slot;
	}

	/**
	 * @return the validity
	 */
	public String getValidity() {
		return validity;
	}

	/**
	 * @param validity
	 *            the validity to set
	 */
	public void setValidity(String validity) {
		this.validity = validity;
	}

	/**
	 * @return the buttonClass
	 */
	public String getButtonClass() {
		return buttonClass;
	}

	/**
	 * @param buttonClass
	 *            the buttonClass to set
	 */
	public void setButtonClass(String buttonClass) {
		this.buttonClass = buttonClass;
	}

	/**
	 * @return the buttonOnclick
	 */
	public String getButtonOnclick() {
		return buttonOnclick;
	}

	/**
	 * @param buttonOnclick
	 *            the buttonOnclick to set
	 */
	public void setButtonOnclick(String buttonOnclick) {
		this.buttonOnclick = buttonOnclick;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buttonClass == null) ? 0 : buttonClass.hashCode());
		result = prime * result + ((buttonOnclick == null) ? 0 : buttonOnclick.hashCode());
		result = prime * result + ((courseID == null) ? 0 : courseID.hashCode());
		result = prime * result + ((courseName == null) ? 0 : courseName.hashCode());
		result = prime * result + ((professorName == null) ? 0 : professorName.hashCode());
		result = prime * result + ((slot == null) ? 0 : slot.hashCode());
		result = prime * result + ((validity == null) ? 0 : validity.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InitialBean other = (InitialBean) obj;
		if (buttonClass == null) {
			if (other.buttonClass != null)
				return false;
		} else if (!buttonClass.equals(other.buttonClass))
			return false;
		if (buttonOnclick == null) {
			if (other.buttonOnclick != null)
				return false;
		} else if (!buttonOnclick.equals(other.buttonOnclick))
			return false;
		if (courseID == null) {
			if (other.courseID != null)
				return false;
		} else if (!courseID.equals(other.courseID))
			return false;
		if (courseName == null) {
			if (other.courseName != null)
				return false;
		} else if (!courseName.equals(other.courseName))
			return false;
		if (professorName == null) {
			if (other.professorName != null)
				return false;
		} else if (!professorName.equals(other.professorName))
			return false;
		if (slot == null) {
			if (other.slot != null)
				return false;
		} else if (!slot.equals(other.slot))
			return false;
		if (validity == null) {
			if (other.validity != null)
				return false;
		} else if (!validity.equals(other.validity))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"InitialBean [courseID=%s, courseName=%s, professorName=%s, slot=%s, validity=%s, buttonClass=%s, buttonOnclick=%s]",
				courseID, courseName, professorName, slot, validity, buttonClass, buttonOnclick);
	}

}
