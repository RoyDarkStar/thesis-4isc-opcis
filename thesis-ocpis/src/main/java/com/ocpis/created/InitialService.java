package com.ocpis.created;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.ocpis.approve.ApproveService;

public class InitialService {
	private static List<InitialBean> initial = new ArrayList<InitialBean>();
	private ApproveService service = new ApproveService();

	public List<InitialBean> retrieveInitial(String studentNumber, String programID) {
		// Ang explanation neto is, Binabato ko kay Getters etong mga petition.add (Yung
		// nakalagay sa array
		// Tas after kunin ni Getters, Ireretrieve ko using setters then return the
		// array list

		String petitionID;
		String petitionCourse;
		String facultyName;
		String slot;
		String validity;
		int count;
		String buttonOnclick = "";
		String buttonClass = "";
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			/*
			 * "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con
					.prepareStatement("SELECT * FROM [Petition]P" + " JOIN [Course]C ON P.petitionCourse=C.courseName"
							+ " JOIN [Faculty]F ON P.facultyID=F.facultyID" + " WHERE C.programID=? AND P.studentID=?"
							+ " AND P.petitionID NOT IN (SELECT S.petitionID FROM [PetitionStatus]S)");
			ps.setString(1, programID);
			ps.setString(2, studentNumber);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				petitionID = rs.getString("petitionID");
				petitionCourse = rs.getString("petitionCourse");
				String first = rs.getString("firstName");
				String middle = rs.getString("middleName");
				String last = rs.getString("lastName");
				facultyName = first + " " + middle + " " + last;
				slot = service.countPetitionMembers(petitionID);
				count = Integer.parseInt(slot);
				validity = rs.getString("validity");
				if (count == 1) {
					buttonOnclick = "return true";
					buttonClass = "btn btn-danger";
				} else {
					buttonOnclick = "return false";
					buttonClass = "btn btn-disabled";
				}

				initial.add(new InitialBean(petitionID, petitionCourse, facultyName, slot, validity, buttonClass,
						buttonOnclick));

			}
			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return initial;
	}

	public void invalidateBean() {
		initial.clear();
	}
}
