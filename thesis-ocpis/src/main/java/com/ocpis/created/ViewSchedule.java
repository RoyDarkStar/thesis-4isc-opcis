package com.ocpis.created;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ViewSchedule
 */
@WebServlet("/ViewSchedule.com")
public class ViewSchedule extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ScheduleService schedule = new ScheduleService();
		schedule.invalidateSchedule();
		String petitionID = request.getParameter("PetitionIDBean");
		request.setAttribute("sched", schedule.retrieveSchedule(petitionID));
		request.getRequestDispatcher("/WEB-INF/view/StudentViewSchedule.jsp").forward(request, response);
	}

}
