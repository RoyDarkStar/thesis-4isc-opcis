package com.ocpis.created;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ScheduleService {

	private static List<ScheduleBean> schedule = new ArrayList<ScheduleBean>();

	public List<ScheduleBean> retrieveSchedule(String petitionID) {
		String day;
		String timeStart;
		String timeEnd;
		String room;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			/*
			 * "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement("SELECT * FROM [PetitionSchedule] WHERE petitionID=?");
			ps.setString(1, petitionID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				day = rs.getString("day");
				timeStart = rs.getString("timeStart");
				timeEnd = rs.getString("timeEnd");
				room = rs.getString("room");

				schedule.add(new ScheduleBean(day, timeStart, timeEnd, room));
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return schedule;
	}

	public void invalidateSchedule() {
		schedule.clear();
	}
}
