package com.ocpis.created;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ocpis.petition.PetitionService;

/**
 * Servlet implementation class SendApproval
 */
@WebServlet("/SendApproval.com")
public class SendApproval extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		PetitionService petition = new PetitionService();

		String studentID = (String) session.getAttribute("studentNumber");
		String petitionID = (String) request.getParameter("PetitionIDBean");

		petition.addPetitionStatus(petitionID);

		response.sendRedirect("CreatedServlet.com");

	}

}
