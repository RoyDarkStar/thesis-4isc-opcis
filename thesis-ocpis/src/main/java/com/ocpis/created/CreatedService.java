package com.ocpis.created;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.ocpis.approve.ApproveService;

public class CreatedService {
	private static List<CreatedBean> create = new ArrayList<CreatedBean>();
	private ApproveService service = new ApproveService();

	public List<CreatedBean> retrievecreate(String studentNumber, String programID) {
		// Ang explanation neto is, Binabato ko kay Getters etong mga petition.add (Yung
		// nakalagay sa array
		// Tas after kunin ni Getters, Ireretrieve ko using setters then return the
		// array list
		String petitionID;
		String petitionCourse;
		String facultyName;
		String first;
		String middle;
		String last;
		String deptChairRemarks;
		String deptChairTimelog;
		String discount;
		String deanTimelog;
		String deanRemarks;
		String registrarTimelog;
		String registrarRemarks;
		String OVRAATimelog;
		String ovraaRemarks;
		String accountingRemarks;
		String accountingTimelog;
		String slot;
		String price;

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			/*
			 * "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement("SELECT * FROM [Petition]P"
					+ " JOIN [Course]C ON P.petitionCourse=C.courseName" + " JOIN [Faculty]F ON P.facultyID=F.facultyID"
					+ " JOIN [PetitionStatus]S ON P.petitionID=S.petitionID"
					+ " WHERE C.programID=? AND P.studentID=?");
			ps.setString(1, programID);
			ps.setString(2, studentNumber);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				petitionID = rs.getString("petitionID");
				petitionCourse = rs.getString("petitionCourse");
				first = rs.getString("firstName");
				middle = rs.getString("middleName");
				last = rs.getString("lastName");
				facultyName = first + " " + middle + " " + last;
				deptChairTimelog = rs.getString("chairTimelog");
				deptChairRemarks = rs.getString("deptChairRemarks");
				discount = rs.getString("facultyDiscount");
				deanTimelog = rs.getString("deanTimelog");
				deanRemarks = rs.getString("deanRemarks");
				registrarTimelog = rs.getString("registrarTimelog");
				registrarRemarks = rs.getString("registrarRemarks");
				ovraaRemarks = rs.getString("ovraaRemarks");
				OVRAATimelog = rs.getString("ovraaTimelog");
				accountingRemarks = rs.getString("accountingRemarks");
				accountingTimelog = rs.getString("accountingTimelog");
				slot = service.countPetitionMembers(petitionID);
				price = rs.getString("price");

				create.add(new CreatedBean(petitionID, petitionCourse, facultyName, deptChairRemarks, deptChairTimelog,
						discount, deanRemarks, deanTimelog, registrarRemarks, registrarTimelog, ovraaRemarks,
						OVRAATimelog, accountingRemarks, accountingTimelog, slot, price));

			}
			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return create;
	}

	public void addcreateBean(CreatedBean addcreate) { // Kinukuha ko yung form ng addPetition sa StudentPetitionJSP
		create.add(addcreate);
	}

	public void deletecreateBean(CreatedBean deletecreate) {
		create.remove(deletecreate);
	}

	public void invalidateBean() {
		create.clear();
	}
}
