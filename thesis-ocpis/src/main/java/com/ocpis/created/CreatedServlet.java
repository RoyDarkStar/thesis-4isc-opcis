package com.ocpis.created;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ocpis.petition.PetitionService;

@WebServlet(urlPatterns = "/CreatedServlet.com")
public class CreatedServlet extends HttpServlet {
	private CreatedService createService = new CreatedService(); // class name ng petitionService
	private PetitionService petitionService = new PetitionService(); // class name ng petitionService
	private InitialService initialService = new InitialService();

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String programID = (String) session.getAttribute("studentProgram");
		System.out.println(programID);
		String studentNumber = (String) session.getAttribute("studentNumber");
		String role = (String) session.getAttribute("role");
		createService.invalidateBean();
		petitionService.invalidateBean();
		initialService.invalidateBean();

		request.setAttribute("initial", initialService.retrieveInitial(studentNumber, programID));
		request.setAttribute("create", createService.retrievecreate(studentNumber, programID));
		request.setAttribute("notif", petitionService.retrieveNotification(studentNumber, role));
		request.getRequestDispatcher("/WEB-INF/view/StudentCreatedPetition.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
