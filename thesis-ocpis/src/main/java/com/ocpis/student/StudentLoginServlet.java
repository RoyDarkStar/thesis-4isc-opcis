package com.ocpis.student;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.ocpis.petition.PetitionService;

@WebServlet(urlPatterns = "/StudentLogin.com")
public class StudentLoginServlet extends HttpServlet {

	private StudentUserValidationService studentValidationService = new StudentUserValidationService(); // class name ng
																										// userValidService
																										// for Students
	// private ApproveService petitionService = new ApproveService(); //class name
	// ng petitionService

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/view/StudentLogin.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String studentNumber = request.getParameter("studentNumber");
		String studentPassword = request.getParameter("studentPassword"); // Getting parameters from View Student Login
																			// Form

		boolean isStudentUserValid = studentValidationService.isUserValid(studentNumber, studentPassword); // user and
																											// password
																											// (refer to
																											// the
																											// Validation
																											// Service)
		// boolean isFacultyUserValid =
		// studentValidationService.isUserValid(studentNumber, studentPassword);

		if (isStudentUserValid) {
			String programID = Integer
					.toString(studentValidationService.setStudentProgram(studentNumber, studentPassword));
			String studentYear = Integer
					.toString(studentValidationService.setStudentYear(studentNumber, studentPassword));
			request.getSession().setAttribute("studentProgram", programID);
			request.getSession().setAttribute("studentYear", studentYear);
			request.getSession().setAttribute("studentNumber", studentNumber);// var and argument
			request.getSession().setAttribute("role", "student");
			response.sendRedirect("/ListPetition.com");
			// request.getRequestDispatcher("/ListPetition.com").forward(request, response);

			// request.setAttribute("studentNumber", studentNumber);//var and argument
			// request.setAttribute("studentPassword", studentPassword);
			// request.setAttribute("petition", petitionService.retrievePetition());
			// //Kukunin ko yung return sa ApproveService.java, PS: nilipat ko sa petition
			// Servlet
			// request.setAttribute("status", "Online");Naka comment out to kasi binabato ko
			// sa Petition Servlet hindi dito sa login
			// request.getRequestDispatcher("/WEB-INF/view/studentDashboard.jsp").forward(request,
			// response); Parang, if user is valid, babato ko data to petitionservlet
		} else {
			request.setAttribute("errorMessage", "Invalid Credentials");
			request.getRequestDispatcher("/WEB-INF/view/StudentLogin.jsp").forward(request, response);
		}

	}
}
