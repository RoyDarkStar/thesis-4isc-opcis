package com.ocpis.student;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import com.ocpis.petition.PetitionService;
import com.ocpis.petition.PetitionService;

@WebServlet(urlPatterns = "/StudentJoinController.com")
public class StudentJoinServletController extends HttpServlet {
	private JoinService JoinService = new JoinService(); // class name ng petitionService

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		JoinService joinService = new JoinService();
		PetitionService petitionService = new PetitionService();
		String role = (String) session.getAttribute("role");
		String studentID = (String) session.getAttribute("studentNumber");
		String petitionID = (String) session.getAttribute("petitionID");
		String programID = (String) session.getAttribute("studentProgram");

		String newCourse = joinService.getPetitionCourse(petitionID);

		if (role.equals("student")) {
			if (joinService.validateJoin(petitionID, studentID)) {
				// System.out.print("User is already a member of the selected petition class");
				request.setAttribute("errorMessage", "User is already a member of the selected petition class");
				petitionService.invalidateBean();
				// request.setAttribute("petition", petitionService.retrievePetition(studentID,
				// role));
				request.getRequestDispatcher("/WEB-INF/view/StudentJoinPetition.jsp").forward(request, response);
			} else {
				if (petitionService.verifyAddPetition(studentID, newCourse)) {
					request.setAttribute("errorMessage",
							"User is already joined to a petition class with the same course description");
					// request.setAttribute("petition",
					// petitionService.retrievePetition(studentNumber, role));
					request.getRequestDispatcher("/WEB-INF/view/StudentJoinPetition.jsp").forward(request, response);
				} else {
					if (petitionService.verifyAdvised(studentID, newCourse)) {
						request.setAttribute("errorMessage", "Student is already advised to this course");
						request.getRequestDispatcher("/WEB-INF/view/StudentJoinPetition.jsp").forward(request,
								response);
					} else {
						if (petitionService.verifyPassed(studentID, newCourse)) {
							request.setAttribute("errorMessage", "Student already completed that course");
							request.getRequestDispatcher("/WEB-INF/view/StudentJoinPetition.jsp").forward(request,
									response);
						} else {
							if (petitionService.verifyPrerequisite(studentID, newCourse, programID)) {
								joinService.joinPetition(petitionID, studentID);
								RequestDispatcher dispatcher = request
										.getRequestDispatcher("/WEB-INF/view/StudentJoinPetitionSucess.jsp");
								dispatcher.forward(request, response);
							} else {
								request.setAttribute("errorMessage",
										"Student has not completed this course's prerequisite/s");
								request.getRequestDispatcher("/WEB-INF/view/StudentJoinPetition.jsp").forward(request,
										response);
							}
						}
					}
				}
				/*
				 * joinService.joinPetition(petitionID, studentID); RequestDispatcher
				 * dispatcher=request.getRequestDispatcher(
				 * "/WEB-INF/view/StudentJoinPetitionSucess.jsp"); dispatcher.forward(request,
				 * response);
				 */
			}

		}

	}
}
