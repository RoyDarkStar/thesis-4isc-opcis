package com.ocpis.student;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JoinService {

	public void joinPetition(String petitionNumber, String studentNumber) {
		String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con
					.prepareStatement("INSERT INTO [PetitionMembers](petitionID, studentID, timelog) VALUES (?, ?, ?)");
			ps.setString(1, petitionNumber);
			ps.setString(2, studentNumber);
			ps.setString(3, timestamp);
			ps.executeQuery();

			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public boolean validateJoin(String petitionNumber, String studentNumber) {
		boolean status = false;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con
					.prepareStatement("SELECT * FROM [PetitionMembers] WHERE petitionID = ? AND studentID = ?");
			ps.setString(1, petitionNumber);
			ps.setString(2, studentNumber);
			ResultSet rs = ps.executeQuery();
			status = rs.next();

			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public String getPetitionCourse(String petitionID) {
		String petitionCourse = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("SELECT * FROM [Petition] WHERE petitionID=?");
			ps.setString(1, petitionID);
			rs = ps.executeQuery();
			while (rs.next()) {
				petitionCourse = rs.getString("petitionCourse");
			}
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return petitionCourse;
	}

}
