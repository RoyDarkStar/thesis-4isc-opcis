package com.ocpis.student;

public class MemberBean {
	private String memberID;
	private String JoinDate;
	private String petitionCourse;
	private String facultyID;
	private String lastName;
	private String firstName;
	private String middleName;
	private String chairID;
	private String studentID;

	public MemberBean(String memberID, String joinDate, String petitionCourse, String facultyID, String lastName,
			String firstName, String middleName, String chairID, String studentID) {
		super();
		this.memberID = memberID;
		this.JoinDate = joinDate;
		this.petitionCourse = petitionCourse;
		this.facultyID = facultyID;
		this.lastName = lastName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.chairID = chairID;
		this.studentID = studentID;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getJoinDate() {
		return JoinDate;
	}

	public void setJoinDate(String joinDate) {
		JoinDate = joinDate;
	}

	public String getPetitionCourse() {
		return petitionCourse;
	}

	public void setPetitionCourse(String petitionCourse) {
		this.petitionCourse = petitionCourse;
	}

	public String getFacultyID() {
		return facultyID;
	}

	public void setFacultyID(String facultyID) {
		this.facultyID = facultyID;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getChairID() {
		return chairID;
	}

	public void setChairID(String chairID) {
		this.chairID = chairID;
	}

	public String getStudentID() {
		return studentID;
	}

	public void setStudentID(String studentID) {
		this.studentID = studentID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((JoinDate == null) ? 0 : JoinDate.hashCode());
		result = prime * result + ((chairID == null) ? 0 : chairID.hashCode());
		result = prime * result + ((facultyID == null) ? 0 : facultyID.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((memberID == null) ? 0 : memberID.hashCode());
		result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((petitionCourse == null) ? 0 : petitionCourse.hashCode());
		result = prime * result + ((studentID == null) ? 0 : studentID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MemberBean other = (MemberBean) obj;
		if (JoinDate == null) {
			if (other.JoinDate != null)
				return false;
		} else if (!JoinDate.equals(other.JoinDate))
			return false;
		if (chairID == null) {
			if (other.chairID != null)
				return false;
		} else if (!chairID.equals(other.chairID))
			return false;
		if (facultyID == null) {
			if (other.facultyID != null)
				return false;
		} else if (!facultyID.equals(other.facultyID))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (memberID == null) {
			if (other.memberID != null)
				return false;
		} else if (!memberID.equals(other.memberID))
			return false;
		if (middleName == null) {
			if (other.middleName != null)
				return false;
		} else if (!middleName.equals(other.middleName))
			return false;
		if (petitionCourse == null) {
			if (other.petitionCourse != null)
				return false;
		} else if (!petitionCourse.equals(other.petitionCourse))
			return false;
		if (studentID == null) {
			if (other.studentID != null)
				return false;
		} else if (!studentID.equals(other.studentID))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MemberBean [memberID=" + memberID + ", JoinDate=" + JoinDate + ", petitionCourse=" + petitionCourse
				+ ", facultyID=" + facultyID + ", lastName=" + lastName + ", firstName=" + firstName + ", middleName="
				+ middleName + ", chairID=" + chairID + ", studentID=" + studentID + "]";
	}

}