package com.ocpis.student;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class StudentUserValidationService {
	public boolean isUserValid(String user, String password) {
		boolean status = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			// Roy Connection
			// String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			// "databaseName=OPCIS;user=sa;password=April241997;";
			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("SELECT * FROM Student WHERE studentID=? AND password=?");
			ps.setString(1, user);
			ps.setString(2, password);
			rs = ps.executeQuery();
			status = rs.next();
		}

		catch (Exception e) {
			System.out.println(e);
		}

		return status;
	}

	public Integer setStudentYear(String user, String pass) {

		int year = 0;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			// Roy Connection
			// String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			// "databaseName=OPCIS;user=sa;password=April241997;";

			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("SELECT * FROM Student WHERE studentID=? AND password=?");
			ps.setString(1, user);
			ps.setString(2, pass);
			rs = ps.executeQuery();
			while (rs.next()) {
				year = rs.getInt("year");
			}
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return year;
	}

	public Integer setStudentProgram(String user, String pass) {

		int programID = 0;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			// Roy Connection
			// String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			// "databaseName=OPCIS;user=sa;password=April241997;";

			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("SELECT * FROM Student WHERE studentID=? AND password=?");
			ps.setString(1, user);
			ps.setString(2, pass);
			rs = ps.executeQuery();
			while (rs.next()) {
				programID = rs.getInt("programID");
			}
		}

		catch (Exception e) {
			System.out.println(e);
		}
		return programID;
	}

}
