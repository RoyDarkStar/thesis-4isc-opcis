package com.ocpis.student;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class MemberService {

	private static List<MemberBean> member = new ArrayList<MemberBean>();

	public List<MemberBean> retrieveMember(String PetitionID) {
		String memberID;
		String joinDate;
		String petitionCourse;
		String facultyID;
		String lastName;
		String firstName;
		String middleName;
		String chairID;
		String studentID;

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Roy
			/*
			 * String connectionUrl = "jdbc:sqlserver://localhost:1433;" +
			 * "databaseName=OPCIS;user=sa;password=April241997;";
			 */

			// Lugs
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";
			Connection con = DriverManager.getConnection(connectionUrl);
			PreparedStatement ps = con.prepareStatement(
					"SELECT A.studentID, A.timelog, B.petitionCourse, B.facultyID, B.chairID, B.studentID as creator, lastName, firstName, middleName from PetitionMembers A JOIN Petition B ON A.petitionID =  b.petitionID JOIN Faculty C ON b.facultyID = c.facultyID WHERE A.petitionID = ?");
			ps.setString(1, PetitionID);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				memberID = rs.getString("studentID");
				joinDate = rs.getString("timelog");
				petitionCourse = rs.getString("petitionCourse");
				facultyID = rs.getString("facultyID");
				lastName = rs.getString("lastName");
				firstName = rs.getString("firstName");
				middleName = rs.getString("middleName");
				chairID = rs.getString("chairID");
				studentID = rs.getString("creator");
				member.add(new MemberBean(memberID, joinDate, petitionCourse, facultyID, lastName, firstName,
						middleName, chairID, studentID));

			}
			con.close();

		} catch (Exception e) {
			System.out.println(e);
		}
		return member;
	}

	public void invalidateMember() {
		member.clear();
	}

}
