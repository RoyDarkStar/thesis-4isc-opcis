package com.ocpis.student;

public class JoinBean {
	private int slotID;
	private int petitionID;
	private int studentID;

	public JoinBean(int slotID, int petitionID, int studentID) {
		this.slotID = slotID;
		this.petitionID = petitionID;
		this.studentID = studentID;
	}

	public int getSlotID() {
		return slotID;
	}

	public void setSlotID(int slotID) {
		this.slotID = slotID;
	}

	public int getPetitionID() {
		return petitionID;
	}

	public void setPetitionID(int petitionID) {
		this.petitionID = petitionID;
	}

	public int getStudentID() {
		return studentID;
	}

	public void setStudentID(int studentID) {
		this.studentID = studentID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("JoinBean [slotID=");
		builder.append(slotID);
		builder.append(", petitionID=");
		builder.append(petitionID);
		builder.append(", studentID=");
		builder.append(studentID);
		builder.append("]");
		return builder.toString();
	}

}
