package com.ocpis.student;

import java.util.List;

public interface JoinPersistence {
	List<JoinBean> findall();

	void insert(JoinBean joinbean);
}
