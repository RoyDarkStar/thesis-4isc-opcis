package com.ocpis.student;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ocpis.petition.PetitionService;

//import com.ocpis.petition.PetitionService;

@WebServlet(urlPatterns = "/StudentLogout.com")
public class StudentLogoutServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		PetitionService petitionService = new PetitionService();

		// Clear Session
		session.invalidate();

		// Clear Bean values
		petitionService.invalidateBean();
		// request.getRequestDispatcher("/StudentLogin.com").forward(request, response);
		response.sendRedirect("/StudentLogin.com");
	}

}
