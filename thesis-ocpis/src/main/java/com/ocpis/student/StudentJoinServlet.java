package com.ocpis.student;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import com.ocpis.petition.PetitionService;

@WebServlet(urlPatterns = "/StudentJoinPetition.com")
public class StudentJoinServlet extends HttpServlet {
	private MemberService memberService = new MemberService();
	private StudentUserValidationService studentValidationService = new StudentUserValidationService(); // class name ng
																										// userValidService
																										// for Students
	// private ApproveService petitionService = new ApproveService(); //class name
	// ng petitionService

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String petitionID = (String) request.getParameter("PetitionIDBean");
		memberService.invalidateMember();
		session.setAttribute("petitionID", petitionID);
		request.setAttribute("member", memberService.retrieveMember(petitionID));
		request.getRequestDispatcher("/WEB-INF/view/StudentJoinPetition.jsp").forward(request, response);
	}
}
