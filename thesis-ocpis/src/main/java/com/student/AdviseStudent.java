package com.student;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdviseStudent
 */
@WebServlet("/AdviseStudent.com")
public class AdviseStudent extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/WEB-INF/view/AdviseStudent.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		int studentNumber = Integer.parseInt(request.getParameter("selectStudent"));
		int enrollmentID = Integer.parseInt(request.getParameter("selectSubject"));

		StudentService studentService = new StudentService();
		studentService.adviseStudent(studentNumber, enrollmentID);

		response.sendRedirect("AdviseStudent.com");
	}

}
