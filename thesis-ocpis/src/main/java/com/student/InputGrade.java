package com.student;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InputGrade
 */
@WebServlet("/InputGrade.com")
public class InputGrade extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/WEB-INF/view/InputGrade.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		int studentNumber = Integer.parseInt(request.getParameter("selectStudent"));
		int courseID = Integer.parseInt(request.getParameter("selectCourse"));
		float grade = Float.parseFloat(request.getParameter("grade"));

		StudentService studentService = new StudentService();
		studentService.addGrade(studentNumber, courseID, grade);

		response.sendRedirect("InputGrade.com");
	}

}
