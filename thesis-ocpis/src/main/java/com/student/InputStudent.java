package com.student;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InputStudent
 */
@WebServlet("/InputStudent.com")
public class InputStudent extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/WEB-INF/view/InputStudent.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		int studentNumber = Integer.parseInt(request.getParameter("studentNumber"));
		int collegeID = Integer.parseInt(request.getParameter("selectCollege"));
		int programID = Integer.parseInt(request.getParameter("selectProgram"));
		int curriculumID = Integer.parseInt(request.getParameter("curriculumCode"));
		int retentionStatus = Integer.parseInt(request.getParameter("retentionStatus"));
		int status = Integer.parseInt(request.getParameter("status"));
		String firstName = request.getParameter("firstName");
		String middleName = request.getParameter("middleName");
		String lastName = request.getParameter("lastName");
		String password = request.getParameter("password");
		String email = request.getParameter("email");
		String birthdate = request.getParameter("birthdate");
		int year = Integer.parseInt(request.getParameter("year"));
		int term = Integer.parseInt(request.getParameter("term"));

		StudentService studentService = new StudentService();
		studentService.addStudent(studentNumber, collegeID, programID, curriculumID, retentionStatus, status, firstName,
				middleName, lastName, password, email, birthdate, year, term);

		response.sendRedirect("InputStudent.com");
	}

}
