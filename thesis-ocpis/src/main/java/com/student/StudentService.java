package com.student;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class StudentService {
	public void addStudent(int studentNumber, int collegeID, int programID, int curriculumID, int retentionStatus,
			int status, String firstName, String middleName, String lastName, String password, String email,
			String birthdate, int year, int term) {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement(
					"INSERT INTO [Student](studentID, collegeID, programID, curriculumID, retentionStatus, status, firstName, middleName, lastName, "
							+ "password, email, birthdate, year, term) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setInt(1, studentNumber);
			ps.setInt(2, collegeID);
			ps.setInt(3, programID);
			ps.setInt(4, curriculumID);
			ps.setInt(5, retentionStatus);
			ps.setInt(6, status);
			ps.setString(7, firstName);
			ps.setString(8, middleName);
			ps.setString(9, lastName);
			ps.setString(10, password);
			ps.setString(11, email);
			ps.setString(12, birthdate);
			ps.setInt(13, year);
			ps.setInt(14, term);
			ps.executeUpdate();
			con.close();
		}

		catch (Exception e) {
			System.out.println(e);
		}
	}

	public void addGrade(int studentID, int courseID, float grade) {
		if (grade == 5) {
			Connection con = null;
			PreparedStatement ps = null;
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

				// Lugs Connection
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";

				con = DriverManager.getConnection(connectionUrl);
				ps = con.prepareStatement("INSERT INTO [FailedSubjects](studentID, courseID) VALUES (?, ?)");
				ps.setInt(1, studentID);
				ps.setInt(2, courseID);
				ps.executeUpdate();
				con.close();
			}

			catch (Exception e) {
				System.out.println(e);
			}
		} else {
			Connection con = null;
			PreparedStatement ps = null;
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

				// Lugs Connection
				String connectionUrl = "jdbc:sqlserver://localhost:1433;"
						+ "databaseName=OPCIS;user=sa;password=April241997;";

				con = DriverManager.getConnection(connectionUrl);
				ps = con.prepareStatement("INSERT INTO [PassedSubjects](studentID, courseID, grade) VALUES (?, ?, ?)");
				ps.setInt(1, studentID);
				ps.setInt(2, courseID);
				ps.setFloat(3, grade);
				ps.executeUpdate();
				con.close();
			}

			catch (Exception e) {
				System.out.println(e);
			}
		}

	}

	public void adviseStudent(int studentID, int enrollmentID) {

		Connection con = null;
		PreparedStatement ps = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			// Lugs Connection
			String connectionUrl = "jdbc:sqlserver://localhost:1433;"
					+ "databaseName=OPCIS;user=sa;password=April241997;";

			con = DriverManager.getConnection(connectionUrl);
			ps = con.prepareStatement("INSERT INTO [AdvisedStudent](studentID, enrollmentID) VALUES (?, ?)");
			ps.setInt(1, studentID);
			ps.setInt(2, enrollmentID);
			ps.executeUpdate();
			con.close();
		}

		catch (Exception e) {
			System.out.println(e);
		}
	}
}
