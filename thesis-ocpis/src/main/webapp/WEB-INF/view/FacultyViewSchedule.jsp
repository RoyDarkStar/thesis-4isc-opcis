<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import ="java.sql.*" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="images/icons/OfficialSealUST.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Student Created Petition - UST OPCIS</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <!--  Data Tables CSS -->
	<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel = "stylesheet">
	<style>
 table 
{
    table-layout:fixed;
    width:100%;
}
</style>
    
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="orange" data-image="${pageContext.servletContext.contextPath}/assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo">
                <a href="#" class="simple-text">
                    UST OPCIS (${employeeNumber})
                </a>
            </div>
            <!-- Start of SideBar wrapper-->
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class ="active">
                        <a href="/ViewSchedule.com">
                            <i class="material-icons">assignment</i>
                            <p>View Schedule</p>
                        </a>
                    </li>
                    <!-- <li class="active">
                        <a href="./table.html">
                            <i class="material-icons">content_paste</i>
                            <p>Table List</p>
                        </a>
                    </li>
                    <li>
                        <a href="./typography.html">
                            <i class="material-icons">library_books</i>
                            <p>Typography</p>
                        </a>
                    </li>
                    <li>
                        <a href="./icons.html">
                            <i class="material-icons">bubble_chart</i>
                            <p>Icons</p>
                        </a>
                    </li>
                    <li>
                        <a href="./maps.html">
                            <i class="material-icons">location_on</i>
                            <p>Maps</p>
                        </a>
                    </li> -->
                   <!--  <li>
                        <a href="./notifications.html">
                            <i class="material-icons text-gray">notifications</i>
                            <p>Notifications</p>
                        </a>
                    </li> -->
                </ul>
            </div>
            <!-- End of SideBar wrapper-->
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Available Petition</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                           
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification"></span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                </a>
                                <!-- Dito ko ilalagay yung for each -->
                                <ul class="dropdown-menu">
                                    <c:forEach items="${notif}" var="NotificationBean">
                                        <li>
                                            <a href = "#">Alert: ${NotificationBean.message} &nbsp;Time: ${NotificationBean.timelog}</a>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </li>
                            
                            <li>
                                <a href="/StudentLogout.com">
                                    <i class="material-icons">power_settings_new</i>
                                    <p class="hidden-lg hidden-md">Logout</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="orange">
                                    <h4 class="title">Pending Petitions</h4>
                                    <% 
	String studentNumber = (String)request.getSession().getAttribute("studentNumber");
	String studentYear = (String)request.getSession().getAttribute("studentYear");
	//int year = Integer.parseInt(studentYear);
	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
	String connectionUrl = "jdbc:sqlserver://localhost:1433;" +  "databaseName=OPCIS;user=sa;password=April241997;";
	Connection con=DriverManager.getConnection(connectionUrl); 
	PreparedStatement ps= con.prepareStatement("SELECT * FROM SETTING Where settingID = 1");
	ResultSet rs=ps.executeQuery();  
%>
<%
	while(rs.next()) {
	String settingID = rs.getString("settingID");
	String dateString = rs.getString("date");
	String semesterString=rs.getString("semester");
%>
	<h6><strong>Semester:</strong> <%=semesterString%> Semester <strong>Enrollment Dates:</strong> <%=dateString%></h6>                                         
<%
	}
%> 
                                    <p class="category"></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table id = "example" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                        <thead class="text-primary">
                                        <tr>
                                            <th>Schedule</th>
                                            <th>Time</th>
                                            <th>Room</th>
                                            <th>Action</th>
                                         </tr>
                                        </thead>
                                        <c:forEach items="${sched}" var ="ApproveScheduleBean">
                                            <tr>
                                                <td>${ApproveScheduleBean.day}</td>
                                                <td>${ApproveScheduleBean.timeStart} - ${ApproveScheduleBean.timeEnd}</td>
                                                <td>${ApproveScheduleBean.room}</td>
                                                <td><a class = "btn btn-danger" href="/RemoveApproveSchedule.com?ScheduleIDBean=${ApproveScheduleBean.scheduleID}&PetitionIDBean=${ApproveScheduleBean.petitionID}">
                                                Remove</a></td>
                                            </tr> 
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                </div> 
                            </div>
                            <a class = "btn btn-info" href="/ApproveAddSchedule.com">Add Schedule</a>
                            <a class = "btn btn-danger" href="/ListApprove.com">Go Back</a>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <footer class="footer">
                <div class="container-fluid">
                    <div class="copyright pull-right">
                        �
                        <script>
                            document.write(new Date().getFullYear())
                        </script>, made with love by
                        <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.   
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="${pageContext.servletContext.contextPath}/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/assets/js/material.min.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="${pageContext.servletContext.contextPath}/assets/js/chartist.min.js"></script>
<!--  Dynamic Elements plugin -->
<script src="${pageContext.servletContext.contextPath}/assets/js/arrive.min.js"></script>
<!--  PerfectScrollbar Library -->
<script src="${pageContext.servletContext.contextPath}/assets/js/perfect-scrollbar.jquery.min.js"></script>
<!--  Notifications Plugin    -->
<script src="${pageContext.servletContext.contextPath}/assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Material Dashboard javascript methods -->
<script src="${pageContext.servletContext.contextPath}/assets/js/material-dashboard.js?v=1.2.0"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="${pageContext.servletContext.contextPath}/assets/js/demo.js"></script>
<!-- Data Tables API JS-->
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="${pageContext.servletContext.contextPath}/js/dataTables.bootstrap.js"></script>
	<script src="${pageContext.servletContext.contextPath}/js/jquery.dataTables.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script type = "text/javascript">
    var $ = jQuery;
    $(document).ready(function() {
        $('#example').DataTable();
    });
    </script>

</html>

<%-- <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href = "webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" rel = "stylesheet">
<title>Student Petition</title>
<!-- Bootstrap core CSS -->
<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"
	rel="stylesheet">

<style>
.footer {
	position: absolute;
	bottom: 0;
	width: 100%;
	height: 60px;
	background-color: #f5f5f5;
}

.footer .container {
width: auto;
max-width: 680px;
padding: 0 15px;
}
</style>
</head>
<body>
<nav role="navigation" class="navbar navbar-default">
	<div class="">
		<a href="/" class="navbar-brand">UST OCPIS</a>
	</div>
	<div class="navbar-collapse">
		<ul class="nav navbar-nav">
		    <li class="active"><a href="#">Home</a></li>
            <li><a href="/ListPetition.com">Petition</a></li>
            <li><a href="/JoinedServlet.com">Joined Petition</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="/StudentLogout.com">Logout</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="#">${studentNumber}</a></li>
		</ul>
	</div>
</nav>
	<div class="container">
		<H1>Welcome STUDENT ${studentNumber}</H1>
		
		<table class = "table table-striped">
			<caption>Petition</caption>
			<tr>
				<thead>
					<th>Petition Name</th>
					<th>Faculty</th>
					<th>Action</th>
				</thead>
			</tr>
			<tbody>
				<c:forEach items="${petition}" var="PetitionBean">
					<tr>
						<td>${PetitionBean.course}</td>
						<td>${PetitionBean.faculty}</td>
						<td>&nbsp;&nbsp; <a class = "btn btn-info" href="/StudentJoinPetition.com?PetitionIDBean=${PetitionBean.petitionID}">
							Join</a>
							&nbsp;&nbsp; <a class = "btn btn-danger" href="/PetitionDelete.com?PetitionIDBean=${PetitionBean.petitionID}">
							Delete</a></td>
							<!-- Remove Delete Button for Student kasi dapat wala siya neto -->
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<ol>
			<c:forEach items="${petition}" var="PetitionBean">
				<li>${PetitionBean.name}&nbsp;${PetitionBean.course}&nbsp;
				<a	href="/PetitionDelete.com?PetitionBean=${PetitionBean.name}%CourseBean=${PetitionBean.course}">Delete</a></li>
			</c:forEach>
		</ol>

		<a href="/AddPetition.com">Add new petition</a>
		
		<table class = "table table-striped">
			<caption>Notification</caption>
			<tr>
				<thead>
					<th>Timelog</th>
					<th>Message</th>
				</thead>
			</tr>
			<tbody>
				<c:forEach items="${notif}" var="NotificationBean">
					<tr>
						<td>${NotificationBean.timelog}</td>
						<td>${NotificationBean.message}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>


<!-- 	<footer class="footer">
		<div class="container">
			<p>footer content</p>
		</div>
	</footer> -->

	<script src="webjars/jquery/3.3.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>

</body>

</html> --%>