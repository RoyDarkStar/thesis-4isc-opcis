<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import ="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<form action="InputSchedule.com" method="POST">
	
		<label>Subject: </label>
		<select name="selectCourse">
			<%
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;" +  
                    	"databaseName=OPCIS;user=sa;password=April241997;";
                Connection con=DriverManager.getConnection(connectionUrl); 
                PreparedStatement ps= con.prepareStatement("SELECT * FROM [Enrollment]E JOIN [Course]C ON E.courseID=C.courseID JOIN [Section]S ON E.sectionID=S.sectionID WHERE S.sectionName='4ISA'");
                ResultSet rs=ps.executeQuery();
			%>
			<%
				while(rs.next()){
					int enrollmentID = rs.getInt("enrollmentID");
					String courseName = rs.getString("courseName");
			%>
			<option value = "<%= enrollmentID %>"><%= courseName %></option>
			<%
				}
			%>
		</select><br>
		
		<label>Day: </label>
		<select name="day">
		
			<option value="Mon">Monday</option>
			<option value="Tue">Tuesday</option>
			<option value="Wed">Wednesday</option>
			<option value="Thur">Thursday</option>
			<option value="Fri">Friday</option>
			<option value="Sat">Saturday</option>
		</select><br>
		
		<label>Time Start: </label>
		<input type="time" name="timeStart" min="7:00" max="21:00"/><br>
		
		<label>Time End: </label>
		<input type="time" name="timeEnd" min="7:00" max="21:00"/><br>
		
		<label>Room: </label>
		<input type="text" name="room"/>
		
		<input type="submit" value="Submit"/>
	</form>
</body>
</html>