    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
    <html>
    <head><title>Search Results = UST OCPIS</title></head>
    <body>
            <table class = "table table-striped">
                <caption>Petition Searched</caption>
                <tr>
                    <thead>
                        <th>CourseCode</th>
                        <th>CourseName</th>
                    </thead>
                </tr>
                <tbody>
                    <c:forEach items="${search}" var="SearchBean">
                        <tr>
                            <td>${SearchBean.courseCode}</td>
                            <td>${SearchBean.courseName}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
    </body>
    </html>