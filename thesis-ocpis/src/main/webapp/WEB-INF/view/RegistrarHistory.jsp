<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import ="java.sql.*" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="images/icons/OfficialSealUST.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Dean Approve Petition - UST OPCIS</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
        <!--  Data Tables CSS -->
	<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel = "stylesheet">
	<style>
	 table 
		{
		    table-layout:fixed;
		    width:100%;
		}
	</style>
</head>
<%-- <nav role="navigation" class="navbar navbar-default">
	<div class="">
		<a href="/" class="navbar-brand">UST OCPIS</a>
	</div>
	<div class="navbar-collapse">
		<ul class="nav navbar-nav">
		    <li><a href="#">Home</a></li>
            <li class="active"><a href="/ListApprove.com">Petition Approval</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="/RegistrarLogout.com">Logout</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="#">${employeeNumber}</a></li>
		</ul>
	</div>
</nav> --%>
<body>
    <div class="wrapper">
        <div class="sidebar" data-color="orange" data-image="${pageContext.servletContext.contextPath}/assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo">
                <a href="#" class="simple-text">
                    UST OPCIS (${employeeNumber})
                </a>
            </div>
            <!-- Start of SideBar wrapper-->
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li>
                        <a href="/ListApprove.com">
                            <i class="material-icons">content_paste</i>
                            <p>Petitions for Approval</p>
                        </a>
                    </li>
                    <li class = "active">
                        <a href="/ListHistory.com">
                            <i class="material-icons">content_paste</i>
                            <p>Approval History</p>
                        </a>
                    </li>
                    <!-- <li class="active">
                        <a href="./table.html">
                            <i class="material-icons">content_paste</i>
                            <p>Table List</p>
                        </a>
                    </li>
                    <li>
                        <a href="./typography.html">
                            <i class="material-icons">library_books</i>
                            <p>Typography</p>
                        </a>
                    </li>
                    <li>
                        <a href="./icons.html">
                            <i class="material-icons">bubble_chart</i>
                            <p>Icons</p>
                        </a>
                    </li>
                    <li>
                        <a href="./maps.html">
                            <i class="material-icons">location_on</i>
                            <p>Maps</p>
                        </a>
                    </li> -->
                    <!-- <li>
                        <a href="./notifications.html">
                            <i class="material-icons text-gray">notifications</i>
                            <p>Notifications</p>
                        </a>
                    </li>
                    <li>
                    <a href="/AddPetition.com">
                    <i class="material-icons text-gray">add</i>
                    <p>Add new Petition</p></a>
                    </li> -->
                </ul>
            </div>
            <!-- End of SideBar wrapper-->
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Petitions for Approval</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li>
                                <a href="/DeanLogout.com">
                                    <i class="material-icons">power_settings_new</i>
                                    <p class="hidden-lg hidden-md">Logout</p>
                                </a>
                            </li>
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group  is-empty">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form>
                    </div>
                </div>
            </nav>
            
            
            
	<div class = "content">
	<div class="container-fluid">
	<div class="table-responsive">
	<div class = "card">
		<div class="card-header" data-background-color="orange">
			Approving Body: ${role}
			<% 
	String studentNumber = (String)request.getSession().getAttribute("studentNumber");
	String studentYear = (String)request.getSession().getAttribute("studentYear");
	//int year = Integer.parseInt(studentYear);
	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
	String connectionUrl = "jdbc:sqlserver://localhost:1433;" +  "databaseName=OPCIS;user=sa;password=April241997;";
	Connection con=DriverManager.getConnection(connectionUrl); 
	PreparedStatement ps= con.prepareStatement("SELECT * FROM SETTING Where settingID = 1");
	ResultSet rs=ps.executeQuery();  
%>
<%
	while(rs.next()) {
	String settingID = rs.getString("settingID");
	String dateString = rs.getString("date");
	String semesterString=rs.getString("semester");
%>
	<h6><strong>Semester:</strong> <%=semesterString%> Semester <strong>Enrollment Dates:</strong> <%=dateString%></h6>                                         
<%
	}
%> 
		</div>
		<table class = "table table-striped table-hover">
			<thead class = "thead-inverse">
				<tr>
					<!-- <th>Status ID</th>
					<th>Petition ID (to be name)</th> -->
					<th>Petition Course</th>
					<!-- <th>Petition Schedule (schedule ID</th> -->
					<!-- <th>Day</th> -->
					<!-- <th>Time</th> -->
					<!-- <th>TimeEnd(FromPetitionSchedule based on ID)</th> -->
					<!-- <th>Petition Course</th> -->
					<th>Faculty ID</th>
					<th>Created By</th>
					<th>Number of enlisted students</th>
					<!-- <th>Department Chair Remarks</th> -->
					<!-- <th>OVRAA Approval</th>
					<th>OVRAA Remarks</th>
					<th>Accounting Approval</th>
					<th>Accounting Remarks</th> -->
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${approve}" var="ApproveBean">
					<tr>
						<%-- <td>${ApproveBean.statusID}</td>
						<td>${ApproveBean.name}</td> --%>
						<td>${ApproveBean.course}</td>
						<td>${ApproveBean.faculty}</td>
						<td>${ApproveBean.creator}</td>
						<td>${ApproveBean.slot}</td>
						<%-- <td>${ApproveBean.deptChairRemarks}</td> --%>

						<%-- <td>${ApproveBean.ovraa}</td>
						<td>${ApproveBean.ovraaRemarks}</td>
						<td>${ApproveBean.accounting}</td>
						<td>${ApproveBean.accountingRemarks}</td> --%>
						<td>
						<br/>
						<%-- <div class = "btn-group-vertical">
							<form method = "POST" action = "/UpdateApprove.com">
							<input type = "hidden" value = "${ApproveBean.statusID}"  name = "ApproveBean" class = "form-control"/>
							<input type = "submit" value = "Approve" class="btn btn-success"/>
							</form>
							&nbsp;
							<br/>
							<a class = "btn btn-danger" data-toggle = "modal" data-target="#remarksModal">Reject</a>
							<div class = "modal fade" id = "remarksModal" role = "dialog">
								<div class = "modal-dialog">
									<div class = "modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class = "modal-title">Remarks</h4>
										</div>
										<div class = "modal-body">
											<div class = "form-group">
											<form method = "POST" action = "/RejectApprove.com">
												<label for = "comment">Reason on why we reject this petition:</label>
												 <input type = "text" name = "addingRemarks" class = "form-control"/>
												 <input type = "hidden" value = "${ApproveBean.statusID}"  name = "ApproveBean" class = "form-control"/>
												 <br>
												 <input type = "submit" value = "Update" class="btn btn-success"/>
												 <a type = "submit" href = "/RejectApprove.com?ApproveBean=${ApproveBean.statusID}" class="btn btn-success">Update</a>
												 <a type= "button" class="btn btn-default" data-dismiss="modal">Close</a>
											</form>
											</div>
										</div>
									</div>
									</div>
								</div>
							</div> --%>
							<br/>
						</td>		
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	</div>
	</div>
	</div>

<footer class="footer">
                <div class="container-fluid">
                     <div class="copyright pull-right">
                        �
                        <script>
                            document.write(new Date().getFullYear())
                        </script>, made with love by
                        <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.   
                    </div>
                             
                </div>
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="${pageContext.servletContext.contextPath}/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/assets/js/material.min.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="${pageContext.servletContext.contextPath}/assets/js/chartist.min.js"></script>
<!--  Dynamic Elements plugin -->
<script src="${pageContext.servletContext.contextPath}/assets/js/arrive.min.js"></script>
<!--  PerfectScrollbar Library -->
<script src="${pageContext.servletContext.contextPath}/assets/js/perfect-scrollbar.jquery.min.js"></script>
<!--  Notifications Plugin    -->
<script src="${pageContext.servletContext.contextPath}/assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Material Dashboard javascript methods -->
<script src="${pageContext.servletContext.contextPath}/assets/js/material-dashboard.js?v=1.2.0"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="${pageContext.servletContext.contextPath}/assets/js/demo.js"></script>
<!-- Data Tables API JS-->
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="${pageContext.servletContext.contextPath}/js/dataTables.bootstrap.js"></script>
	<script src="${pageContext.servletContext.contextPath}/js/jquery.dataTables.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script type = "text/javascript">
    var $ = jQuery;
    $(document).ready(function() {
        $('.table').DataTable();
    });
    </script>

