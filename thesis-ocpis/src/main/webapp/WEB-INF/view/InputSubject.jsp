<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import ="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<form action="InputSubject.com" method="POST">
	
		<label>Course: </label>
		<select name="selectCourse">
			<%
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;" +  
                    	"databaseName=OPCIS;user=sa;password=April241997;";
                Connection con=DriverManager.getConnection(connectionUrl); 
                PreparedStatement ps= con.prepareStatement("SELECT * FROM [Course] WHERE programID=1 AND year=4 AND term=2");
                ResultSet rs=ps.executeQuery();
			%>
			<%
				while(rs.next()){
					int courseID = rs.getInt("courseID");
					String courseName = rs.getString("courseName");
			%>
			<option value = "<%= courseID %>"><%= courseName %></option>
			<%
				}
			%>
		</select><br>
		
		<label>Section: </label>
		<select name="selectSection">
			<%
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance(); 
                PreparedStatement ab= con.prepareStatement("SELECT * FROM [Section]");
                ResultSet cd=ab.executeQuery();
			%>
			<%
				while(cd.next()){
					int sectionID = cd.getInt("sectionID");
					String sectionName = cd.getString("sectionName");
			%>
			<option value = "<%= sectionID %>"><%= sectionName %></option>
			<%
				}
			%>
		</select><br>
		
		<input type="submit" value="Submit">
	</form>
</body>
</html>