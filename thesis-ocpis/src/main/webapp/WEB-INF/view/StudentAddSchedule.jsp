<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page import ="java.sql.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="images/icons/OfficialSealUST.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Student Add Petition - UST OPCIS</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
       <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>
<body>
    <div class="wrapper">
        <div class="sidebar" data-color="orange" data-image="${pageContext.servletContext.contextPath}/assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo">
                <a href="#" class="simple-text">
                    UST OPCIS (${studentNumber})
                </a>
            </div>
            <!-- Start of SideBar wrapper-->
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li>
                        <a href="/ListPetition.com">
                            <i class="material-icons">dashboard</i>
                            <p>Available Petition</p>
                        </a>
                    </li>
                    <li>
                        <a href="/JoinedServlet.com">
                            <i class="material-icons">person</i>
                            <p>Joined Petition</p>
                        </a>
                    </li>
                    <li>
                        <a href="/CreatedServlet.com">
                            <i class="material-icons">assignment</i>
                            <p>myCreated Petition</p>
                        </a>
                    </li>
                    <li class ="active">
                    <a href="/AddPetitionSchedule.com">
                    <i class="material-icons text-gray">add</i>
                    <p>Add Schedule</p></a>
                    </li>
                    
                </ul>
            </div>
            <!-- End of SideBar wrapper-->
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Add Schedule</a>
                    </div>
                     <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                           
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification"></span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                </a>
                                <!-- Dito ko ilalagay yung for each -->
                                <ul class="dropdown-menu">
                                    <c:forEach items="${notif}" var="NotificationBean">
                                        <li>
                                            <a href = "#">Alert: ${NotificationBean.message} &nbsp;Time: ${NotificationBean.timelog}</a>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </li>
                            
                            <li>
                                <a href="/StudentLogout.com">
                                    <i class="material-icons">power_settings_new</i>
                                    <p class="hidden-lg hidden-md">Logout</p>
                                </a>
                            </li>
                        </ul>
                        
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div>
                        	<p><font color="red">${errorMessage}</font></p>
                            <div class="card">
                                <div class="card-header" data-background-color="orange">
                                <% 
	String studentNumber = (String)request.getSession().getAttribute("studentNumber");
	String studentYear = (String)request.getSession().getAttribute("studentYear");
	//int year = Integer.parseInt(studentYear);
	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
	String connectionUrl = "jdbc:sqlserver://localhost:1433;" +  "databaseName=OPCIS;user=sa;password=April241997;";
	Connection con=DriverManager.getConnection(connectionUrl); 
	PreparedStatement ps= con.prepareStatement("SELECT * FROM SETTING Where settingID = 1");
	ResultSet rs=ps.executeQuery();  
%>
<%
	while(rs.next()) {
	String settingID = rs.getString("settingID");
	String dateString = rs.getString("date");
	String semesterString=rs.getString("semester");
%>
	<h6><strong>Semester:</strong> <%=semesterString%> Semester <strong>Enrollment Dates:</strong> <%=dateString%></h6>                                         
<%
	}
%> 
                                    <h4 class="title">Add Schedule for ${createdPetition}</h4>   
                                    <p class="category">Complete the details</p>
                                </div>
                                <div class="card-content">
                                    <form method = "POST" action ="/AddPetitionSchedule.com">
                                        <div class="row">
                                            <div>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Petitioned by: ${studentNumber}</label>
                                                    <input type="text" class="form-control" disabled>
                                                </div>
                                            </div>
                                            <div>
                                            </div>
                                            <div>
                                                <div class="form-group label-floating">
                                                        <fieldset class = "form-group">
                                                                <label>Add Proposed Petition Time Day</label>
                                                                    <select name = "addingDay" class = "form-control" required>
                                                                    <option>MON</option>
                                                                    <option>TUE</option>
                                                                    <option>WED</option>
                                                                    <option>THU</option>
                                                                    <option>FRI</option>
                                                                    <option>SAT</option>
                                                                    </select>
                                                                </fieldset>     
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div>
                                                <div class="form-group label-floating">
                                                        <fieldset class = "form-group">
                                                                <label>Add Proposed Petition Time Start</label>
                                                                    <input type="time" name="addingTimeStart" min = "7:00" max = "21:00" class = "form-control" required>
                                                                </fieldset>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="form-group label-floating">
                                                    <fieldset class = "form-group">
                                                            <label>Add Proposed Petition Time End</label>
                                                                <input type="time" name="addingTimeEnd" class = "form-control" required>
                                                            </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div>
                                                <div class="form-group label-floating">
                                                        <fieldset class = "form-group">
                                                                <label>Add Proposed Petition Classroom</label>
                                                                <input name="addingRoom" type="text" class = "form-control" required><br/>
                                                            </fieldset> 
                                                </div>
                                            </div>
                                        </div>
                                         <a  class="btn btn-primary pull-right" href="/CreatedServlet.com">Done</a>
                                        <button type="submit" class="btn btn-primary pull-right">Add Schedule</button>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
             <footer class="footer">
                <div class="container-fluid">
                   <div class="copyright pull-right">
                        �
                        <script>
                            document.write(new Date().getFullYear())
                        </script>, made with love by
                        <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.   
                    </div>
                </div>
            </footer>
        </div>
</body>
<!--   Core JS Files   -->
<script src="${pageContext.servletContext.contextPath}/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/assets/js/material.min.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="${pageContext.servletContext.contextPath}/assets/js/chartist.min.js"></script>
<!--  Dynamic Elements plugin -->
<script src="${pageContext.servletContext.contextPath}/assets/js/arrive.min.js"></script>
<!--  PerfectScrollbar Library -->
<script src="${pageContext.servletContext.contextPath}/assets/js/perfect-scrollbar.jquery.min.js"></script>
<!--  Notifications Plugin    -->
<script src="${pageContext.servletContext.contextPath}/assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Material Dashboard javascript methods -->
<script src="${pageContext.servletContext.contextPath}/assets/js/material-dashboard.js?v=1.2.0"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="${pageContext.servletContext.contextPath}/assets/js/demo.js"></script>

</html>

<%-- <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page import ="java.sql.*" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/moment.js"></script> 
<script type="text/javascript" src="<%=request.getContextPath()%>/js/combodate.js"></script> 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href = "webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" rel = "stylesheet">
<title>Student Petition</title>
<!-- Bootstrap core CSS -->
<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"
	rel="stylesheet">

<style>
.footer {
	position: absolute;
	bottom: 0;
	width: 100%;
	height: 60px;
	background-color: #f5f5f5;
}

.footer .container {
width: auto;
max-width: 680px;
padding: 0 15px;
}
</style>
</head>
<body>
<nav role="navigation" class="navbar navbar-default">
	<div class="">
		<a href="/" class="navbar-brand">UST OCPIS</a>
	</div>
	<div class="navbar-collapse">
		<ul class="nav navbar-nav">
		    <li class="active"><a href="#">Home</a></li>
            <li><a href="/ListPetition.com">Petition</a></li>
            <li><a href="/JoinedServlet.com">Joined Petition</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="/StudentLogout.com">Logout</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="#">${studentNumber}</a></li>
		</ul>
	</div>
</nav>

	<div class="container">
		<form method="POST" action="/AddPetition.com">			
			<fieldset class = "form-group"> Pulling from DB CourseTable displaying Petition Course Name
				<label>Add Petition Course</label>
					<select name = "addingCourse" class = "form-control">
						<% 
							String studentNumber = (String)request.getSession().getAttribute("studentNumber");
							Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
							//Roy
							/* String connectionUrl = "jdbc:sqlserver://localhost:1433;" +  
									   "databaseName=OPCIS;user=sa;password=qwertyasi;";  */
							
							//Lugs
							String connectionUrl = "jdbc:sqlserver://localhost:1433;" +  
									   "databaseName=OPCIS;user=sa;password=April241997;";
									   
							Connection con=DriverManager.getConnection(connectionUrl); 
							PreparedStatement ps= con.prepareStatement("SELECT c.courseName FROM [FailedSubjects]f JOIN [Course]c ON f.courseID = c.courseID WHERE f.studentID=?");
							ps.setString(1, studentNumber);
							ResultSet rs=ps.executeQuery();  
						%>
						<%
							while(rs.next()) {
							String petitionCourse=rs.getString("courseName");
						%>
							<option value="<%=petitionCourse %>"><%=petitionCourse %></option>
						 <%}
						%>
				</select>	
			</fieldset>
			<fieldset class = "form-group">
			<label>Add Proposed Petition Time Start</label>
				<select name = "addingDay" class = "form-control">
				<option>MON</option>
				<option>TUE</option>
				<option>WED</option>
				<option>THU</option>
				<option>FRI</option>
				<option>SAT</option>
				</select>
			</fieldset> 
			<fieldset class = "form-group">
			<label>Add Proposed Petition Time Start</label>
				<select name = "addingTimeStart" class = "form-control">
				<option>7:00am</option>
				<option>8:00am</option>
				<option>9:00am</option>
				<option>10:00am</option>
				<option>11:00am</option>
				<option>12:00pm</option>
				<option>1:00pm</option>
				<option>2:00pm</option>
				<option>3:00pm</option>
				<option>4:00pm</option>
				<option>5:00pm</option>
				<option>6:00pm</option>
				<option>7:00pm</option>
				<option>8:00pm</option>
				<option>9:00pm</option>
				</select>
			</fieldset>
			<fieldset class = "form-group">
			<label>Add Proposed Petition Time End</label>
				<select name = "addingTimeEnd" class = "form-control">
				<option>7:00am</option>
				<option>8:00am</option>
				<option>9:00am</option>
				<option>10:00am</option>
				<option>11:00am</option>
				<option>12:00pm</option>
				<option>1:00pm</option>
				<option>2:00pm</option>
				<option>3:00pm</option>
				<option>4:00pm</option>
				<option>5:00pm</option>
				<option>6:00pm</option>
				<option>7:00pm</option>
				<option>8:00pm</option>
				<option>9:00pm</option>
				</select>
			</fieldset>
			<fieldset class = "form-group">
				<label>Add Proposed Faculty Professor</label> <!-- //FROM FACULTY PROFESSOR -->
					<select name = "addingProfessor" class = "form-control">
						<% 
							Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();  
							PreparedStatement ab= con.prepareStatement("SELECT * FROM FACULTY");
							ResultSet cd=ab.executeQuery();  
						%>
						<%
							while(cd.next()) {
							String professorLastName=cd.getString("lastName");
							String professorFirstName=cd.getString("firstName");
							String professorMiddleName=cd.getString("middleName");
							String professorName=professorFirstName+" "+professorMiddleName+" "+professorLastName;
							String professorID=cd.getString("facultyID");
						%>
							<option value = "<%=professorName%>"><%=professorName%></option>
				</select>
				<input type = "hidden" name = "facultyID" value="<%=professorID%>"/>
						 <%}
						%>	
			</fieldset> 
			<fieldset class = "form-group">
				<label>Add Proposed Petition Classroom</label>
			    <input name="addingRoom" type="text" class = "form-control"/><br/>
			</fieldset> 
			
			
			
			<input name="add" type="submit" class ="btn btn-sucess" value = "Submit Petition" />
		</form>
	</div>

	<footer class="footer">
		<div class="container">
			<p>footer content</p>
		</div>
	</footer>

	<script src="webjars/jquery/3.3.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>

</body>

</html> --%>