<!DOCTYPE html>
<html lang="en">
<head>
	<title>Student Login - UST OPCIS</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/studentportal.css" rel="stylesheet" media="screen"/>
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/OfficialSealUST.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
<script type="text/javascript">
        //if(self != top) { top.location = self.location; }
    </script>
    <script type="text/javascript">
        function validate() {
        
                var x = document.getElementById("txtUsername");
                var y = x.value;
                //y = y.replace(/^\d{10}$/,"");
                //if ((y == "") || (y == null)) {
                var re5digit=/^\d{10}$/
                if (document.form1.txtUsername.value.search(re5digit)==-1) {
                        alert("Username should consist of 10 digits only");
                        x.focus();
                        return false;
                }
                
                var x = document.getElementById("txtPassword");
                var y = x.value;
                y = y.replace(/^\s*|\s*$/g,"");
                if ((y == "") || (y == null)) {
                        alert("Password is required");
                        x.focus();
                        return false;
                }
        }
    </script>
    <style type="text/css">
        ol.letter {
                list-style-image: none;
                list-style-type: upper-alpha;
        }
        
        ol.letter li {
                padding-bottom: 10px;
        }
        
        ol.payment li {
                padding-bottom: 10px;
        }
        
        ol.payment ul li {
                padding-bottom: 0px;
        }
        
        ol.letter ol {
                list-style-image: none;
                list-style-type: decimal;
                padding-left: 20px;
        }
        
        .sub_color {
                font-size: 12px;
        }
        
        ol.procedure li {
           padding-bottom: 8px;
        }
        
     #backgroundPopup{  
         display:none;  
         position:fixed;  
         position:absolute; /* hack for internet explorer 6*/  
         height:100%;  
         width:100%;  
         top:0;  
         left:0;  
         background:#000000;  
         border:1px solid #cecece;  
         z-index:1;  
     }
     
     #popupBox {
     _position:absolute;
     position:fixed; 
     width: 600px;
     background-color: #FFF;
     padding: 20px;
     display: none;
     z-index:2;
     text-align: left;
     }
     tbody td {
       font-size: 12px;
     }
     
        table.sched {
                border-collapse:collapse;
                border: 1px solid black;
        }
        table.sched td {
                border: #000 solid 1px;
                padding: 7px 2px 7px 2px;
        }
        
        table.notes td {
                padding: 2px;
        }
    </style>
    

<!--===============================================================================================-->
</head>
<body>
	<!-- <div class="container">
		<form action = "StudentLogin.com" method = "post">
		<p><font color = "red">${errorMessage}</font></p>
			Enter your student number: <input type="text" name= "studentNumber"/>
			Enter your password: <input type = "password" name = "studentPassword"/>
			<input type="submit"/>
	</form> -->
<div id="container">
    <header>
    <div id="header_container"></div>
</header>
<div id="body_main_container">
      <div id="body_container1">
        <div id="container_body_left">
          <form name="form1" id="form1" action="StudentLogin.com" method="post">
            <input name="param" type="hidden" id="param" value="login" />
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                 <tr>
                      <td width="66" class="header_title1">LOGIN</td>
                      <p><font color = "red">${errorMessage}</font></p>
                      <td width="234">&nbsp;</td>
                 </tr>
                 <tr>
                    <td colspan="2">
                        <div align="center" class="error_style">
                            
                        </div>
                    </td>
                  </tr>
                   <tr>
                   
                      <td><strong>Username:</strong></td>
                      <td><input type="text" name="studentNumber" maxlength="10" style="width: 94%"/></td>
                 </tr>
                 <tr>
                      <td>&nbsp;</td>
                      
                      <td height="10px"><span style="font-size:10px">Sample username: <strong>2011001234</strong></span></td>
                 </tr>
                 <tr>
                      <td><strong>Password:</strong></td>
                      <td><input type="password" name="studentPassword" maxlength="15" style="width: 94%" required/></td>
                 </tr>
                 <tr>
                      <td>&nbsp;</td>
                      <td><input type="submit" value="Login" name="btnSubmit" form = "form1"/></td>
                 </tr>
            </table>
            </form>
            <h2 class="header_title1">GUIDELINES</h2>
<ul>
<li><a id="link_style1" href="index.jsp?gid=17#guid">Account Activation Guide</a></li>
<li><a id="link_style1" href="index.jsp?gid=16#guid">Frequently Asked Questions </a></li>
<li><a id="link_style1" href="index.jsp?gid=15#guid">Payment Thru BPI</a></li>
<li><a id="link_style1" href="index.jsp?gid=13#guid">Payment Thru Metrobank</a></li>
</ul>

<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
<tr>
<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=myuste.ust.edu.ph&amp;size=L&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=en"></script><br />
<a href="http://www.symantec.com/ssl-certificates" target="_blank"  style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a></td>
</tr>
</table>
          </div>
        <div id="container_body_right">
            <h2 class="header_title1">ANNOUNCEMENT</h2>

            <h3>6&nbsp;March 2018</h3>
<h2>STEPS&nbsp;Advisory To Graduating Students</h2>
<h4>The apparent &ldquo;premature launch&rdquo; of the Online Application for Graduation and Thomasian Yearbook yesterday, March 5, , via the MyUste Student Portal was a beta-test version. &nbsp;We would like to clarify that the said online module was not yet intended for use.</h4>
<h4>In view of this, please be advised that all data inputs made using this module on March 5, 2018 will be deleted. The official communication will be released once the application module is fully polished and made available for all concerned students.</h4>
<h4>Thank you very much and please be guided accordingly.</h4>
<hr />
<p>To: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; All Incoming College Freshmen</p>
<p>Re: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tuition and Other Fees for AY 2018-2019</p>
<p>____________________________________________________________________________</p>
<p>In preparation for your enrollment, please be guided by your respective school fees.</p>
<p>Thank you.</p>
<a href="https://drive.google.com/open?id=1OmiEF5h5XeONuxuTxqEjrurwaW8EkScy">Tuition and Other Fees for AY 2018-2019</a>
<hr />
<a href="https://drive.google.com/open?id=1rHlKTyIPMXrANbeJt_2Nfrt3s1dpukOk">Table of Fees for Senior High School Incoming Grade 11</a>
<hr />
<p style="color: #000000; font-family: 'Times New Roman'; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">To: All Grade 12 Graduating Senior High School students</p>
<p style="color: #000000; font-family: 'Times New Roman'; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">Re: Correction of Personal Data</p>
<p style="color: #000000; font-family: 'Times New Roman'; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">From: Office of the Registrar</p>
<p style="color: #000000; font-family: 'Times New Roman'; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">___________________________________________________________________________________</p>
<p style="color: #000000; font-family: 'Times New Roman'; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">In preparation of the final list of graduating students, all Grade 12 graduating students are enjoined to check on the correctness and completeness of personal data, particularly the spelling of names.</p>
<p style="color: #000000; font-family: 'Times New Roman'; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">In case correction is needed, the concerned students must download the APPLICATION FORM FOR CORRECTION OF PERSONAL data, accomplish the form, attach the required supporting evidence, and submit the form and attachments to the class adviser.</p>
<p style="color: #000000; font-family: 'Times New Roman'; font-size: medium; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;"><strong>Deadline for submission of application for correction of personal data is set on Tuesday, February 13, 2018.</strong></p>
<br /><a href="https://drive.google.com/file/d/1BQ_jIZETz0WwV6392ctUhnF3wJ88QM54/view?usp=sharing">Correction of PDS Form</a>
<hr />
<a href="https://drive.google.com/open?id=1i37kku5E9VVLzSB8HGXCSn4ivGtb3IzX" target="_blank">Collegiate Calendar for AY 2018-2019</a><br /><br /><a href="https://drive.google.com/file/d/0B0djNJ3Cl__gM1lwaVlGMXI4NzQ/view">Collegiate Calendar for AY 2017-2018</a>
<hr />
<p>27 October 2017</p>
<p>To: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>ANNOUNCEMENT TO ALL CONCERNED STUDENTS</strong></p>
<p>From: &nbsp;&nbsp; Office of the Registrar</p>
<p>Re: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Correction of Personal Information</p>
<p><strong>&nbsp;</strong></p>
<p>Prior to enrollment, you were required to accomplish a personal data sheet <em>online</em>. Inasmuch as these personal data are critical in the issuance of school records, you are enjoined to log on to your portal and carefully verify your respective personal data entries.</p>
<p>These data may include your name, date of birth, birth place, name of parents, name of guardian/person to be contacted in case of emergency, gender, religion, contact details.</p>
<p>To facilitate the correction of these data, bring copies of original and photocopy of supporting documents and proceed to the Office of the Registrar, as follows:</p>
<table>
<tbody>
<tr>
<td>
<p><strong><em>Window</em></strong></p>
</td>
<td>
<p><strong><em>Purpose</em></strong></p>
</td>
</tr>
<tr>
<td>
<p>Information Window</p>
</td>
<td>
<p>For correction of address, name of guardian, person to be contacted in case of emergency, name of previous school</p>
</td>
</tr>
<tr>
<td>
<p>College Window</p>
</td>
<td>
<p>For correction of name, birthdate, birthplace, name of parents, and other information (i.e. gender, religion etc.)</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Thank you.</p>
<hr />
<p>26 October 2017</p>
<p>To: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>All Graduating Students</strong></p>
<p>From: &nbsp;&nbsp; Office of the Registrar</p>
<p>Re: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Correction of Mailing Address</p>
<p><strong>&nbsp;</strong></p>
<p>Please double check the&nbsp;completeness of your home address&nbsp;appearing in your MyUste account. Your graduation records (certificate of graduation,&nbsp;diploma and official&nbsp;transcript of records) shall be sent through the address indicated. Make sure that it is your permanent home address and not a temporary residence.&nbsp;</p>
<p>The success of the delivery of these records heavily depend on the completeness and correctness of your respective addresses.&nbsp;</p>
<p>&nbsp;In case of a need to update the home address,&nbsp;please report&nbsp;immediately to the Academic Clerk-in-charge assigned at your respective Faculty/College window&nbsp;at the Office of the Registrar.&nbsp;</p>
<p>&nbsp;</p>
<p>Thank you.</p>
<hr />

            <a name="guid" id="guid"></a>
           <!-- <h2 class="header_title1">ANNOUNCEMENT</h2>
<p>TO ALL STUDENTS:</p>
<p style="font-size: 14px"><strong>Grades for 2nd Semester S.Y. 2010-2011 will be available for viewing on April 2, 2011</strong></p>
<hr />

             -->
            <!-- 
           <h2 class="header_title1">Account Activation Guide</h2>
           
         
              <span class="sub_color">Overview/Instructions<br /></span>
               The myUSTe portal is a system accessible via the Internet and is
              intended to include all the functionalities of K.I.O.S.K., the
              intranet facility for accessing student information. To use the
              myUSTe portal, a student must be officially enrolled, and he/she must
              activate his/her account on-line at 
              <a href="http://myuste.ust.edu.ph/student">
                http://myuste.ust.edu.ph/student.
              </a><br/>
               In the account activation, he/she will be required to supply his/her
              Identification Number and Date of Birth. One must ensure that he/she has
              already chosen a new Password to replace the default password
              generated by the system. 
            </p><p>
            
              <span class="sub_color">Using one's IDENTIFICATION NUMBER<br /></span>
              The student ID number is automatically generated by the system and is permanently assigned to the student upon confirmation of enrolment in the University. This is printed on the student's ID card and on the his/her copy of Registration Form using the format YYYY-999999 (e.g. 1991-050299). By simply dropping the hyphen (1991050299), the student number serves as his/her username in accessing the myUSTe portal. One's login account therefore, cannot be customized or changed. 
            </p><p>
            
              <span class="sub_color">Using one's Date of Birth to Activate the Account<br /></span>
              The date of birth a student entered/declared in the Admission system when he/she initially applied to the University is used as his/her initial password in the myUSTe portal. A student must supply this exact date of birth in the activation page in the format MMDDYYYY (e.g. 04261975) [April 26, 1975]. 
            </p><p>
            
              <span class="sub_color">Entering and Verifying the New Password<br /></span>
              The new password a student will supply should be easy to remember but difficult for others to guess since it will be used to access his/her information. The new password should be 8 to 12 characters long and preferably composed of numbers and letters. The system supports case sensitivity so it is also recommended that one enters his/her password in mixed cases (e.g. Cr8tivM3).
              <br/>If a student forgot his/her password, simply click the "Forgot your password?" link and supply the information required. The new password will be emailed to him/her. A student may change his/her password again once he/she has logged on to the system. 
            </p><p>
              You may check the 
              <strong>Frequently Asked Questions</strong>
               if you have troubles activating your myUSTe account.
            </p>

 -->
            <h2 class="header_title1">Account Activation Guide</h2>
<p><span class="sub_color">Overview/Instructions<br /></span> The myUSTe portal is a system accessible via the Internet and is               intended to include all the functionalities of K.I.O.S.K., the               intranet facility for accessing student information. To use the               myUSTe portal, a student must be officially enrolled, and he/she must               activate his/her account on-line at                <a href="http://myuste.ust.edu.ph/student"> http://myuste.ust.edu.ph/student. </a><br /> In the account activation, he/she will be required to supply his/her               Identification Number and Date of Birth. One must ensure that he/she has               already chosen a new Password to replace the default password               generated by the system.</p>
<p><span class="sub_color">Using one's IDENTIFICATION NUMBER<br /></span> The student ID number is automatically generated by the system and is permanently assigned to the student upon confirmation of enrolment in the University. This is printed on the student's ID card and on the his/her copy of Registration Form using the format YYYY-999999 (e.g. 1991-050299). By simply dropping the hyphen (1991050299), the student number serves as his/her username in accessing the myUSTe portal. One's login account therefore, cannot be customized or changed.</p>
<p><span class="sub_color">Using one's Date of Birth to Activate the Account<br /></span> The date of birth a student entered/declared in the Admission system when he/she initially applied to the University is used as his/her initial password in the myUSTe portal. A student must supply this exact date of birth in the activation page in the format MMDDYYYY (e.g. 04261975) [April 26, 1975].</p>
<p><span class="sub_color">Entering and Verifying the New Password<br /></span> The new password a student will supply should be easy to remember but difficult for others to guess since it will be used to access his/her information. The new password should be 8 to 12 characters long and preferably composed of numbers and letters. The system supports case sensitivity so it is also recommended that one enters his/her password in mixed cases (e.g. Cr8tivM3).               <br />If a student forgot his/her password, simply click the "Forgot your password?" link and supply the information required. The new password will be emailed to him/her. A student may change his/her password again once he/she has logged on to the system.</p>
<p>You may check the                <strong>Frequently Asked Questions</strong> if you have troubles activating your myUSTe account.</p>

            
          </div>
       </div>
     </div>
     <div style="clear:both;"></div>
<div id="footer">Copyright &copy; 2018  University of Santo Tomas. www.ust.edu.ph. All rights reserved. 
  
    <a href="http://steps.ust.edu.ph/" target="_blank" style="color:#fff;">Powered by Santo Tomas e-Service Providers</a></div>

  </div>
  <!--
  <div id="backgroundPopup"></div> 
  <div id="popupBox">
            <h2 class="header_title1" style="text-align: center">IMPORTANT ANNOUNCEMENT</h2>
            <p>TO ALL STUDENTS:</p>
            <p><strong>Grades for Summer S.Y. 2009-2010 will be available for viewing on May 28, 2010</strong></p>
            <p><a href="#" id="closeId" onclick="disablePopup()">CLOSE</a></p>
  </div>
  -->
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>