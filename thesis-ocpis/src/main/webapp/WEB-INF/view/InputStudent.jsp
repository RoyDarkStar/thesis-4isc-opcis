<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import ="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<form action="InputStudent.com" method="POST">
	
		<label>Student Number: </label>
		<input type="text" name="studentNumber"/><br>
		
		<label>College: </label>
		<select name="selectCollege">
			<%
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;" +  
                    	"databaseName=OPCIS;user=sa;password=April241997;";
                Connection con=DriverManager.getConnection(connectionUrl); 
                PreparedStatement ps= con.prepareStatement("SELECT * FROM [College]");
                ResultSet rs=ps.executeQuery();
			%>
			<%
				while(rs.next()){
					String collegeID = rs.getString("collegeID");
					String collegeName = rs.getString("collegeName");
			%>
			<option value = "<%= collegeID %>"><%= collegeName %></option>
			<%
				}
			%>
		</select><br>
		
		<label>Program: </label>
		<select name="selectProgram">
			<%	
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
                PreparedStatement ab= con.prepareStatement("SELECT * FROM [Program]");
                ResultSet cd=ab.executeQuery();
			%>
			<%
				while(cd.next()){
					String programID = cd.getString("programID");
					String programName = cd.getString("programName");
			%>
			<option value = "<%= programID %>"><%= programName %></option>
			<%
				}
			%>
		</select><br>
		
		<label>Curriculum: </label>
		<input type="text" name="curriculumCode"/><br>
		
		<label>Retention Status: </label>
		<select name="retentionStatus">
			<option value=1>Regular</option>
			<option value=2>Irregular</option>
			<option value=3>Conditional</option>
		</select><br>
		
		<label>Status: </label>
		<select name="status">
			<option value=1>On-going</option>
			<option value=2>Completed</option>
			<option value=3>Graduated</option>
			<option value=4>Discontinued</option>
			<option value=5>Temporary</option>
			<option value=6>Honorable Dismissal</option>
			<option value=7>Cancelled</option>
		</select><br>
		
		<label>First Name: </label>
		<input type="text" name="firstName"/><br>
		
		<label>Middle Name: </label>
		<input type="text" name="middleName"/><br>
		
		<label>Last Name: </label>
		<input type="text" name="lastName"/><br>
		
		<label>Password: </label>
		<input type="password" name="password"/><br>
		
		<label>Email: </label>
		<input type="text" name="email"/><br>
		
		<label>Birthdate(DD-MMM-YYYY): </label>
		<input type="text" name="birthdate"/><br>
		
		<label>Year: </label>
		<select name="year">
			<option value=1>1</option>
			<option value=2>2</option>
			<option value=3>3</option>
			<option value=4>4</option>
		</select><br>
		
		<label>Term: </label>
		<select name="term">
			<option value=1>First</option>
			<option value=2>Second</option>
			<option value=3>Summer</option>
		</select><br>
		
		<input type="submit" value="Submit"/>
	</form>
	<button><a href = "HomePage.com">Go Back</a></button>

</body>
</html>