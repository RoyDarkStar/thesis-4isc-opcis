<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import ="java.sql.*" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="images/icons/OfficialSealUST.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Joined Petition - UST OPCIS</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
        <!--  Data Tables CSS -->
	<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css" rel = "stylesheet">
 -->	<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel = "stylesheet">
<style>
 table 
{
    table-layout:fixed;
    width:100%;
     border-spacing: 0;
    border-collapse: collapse;
}
.blocks,
.btn {
  padding: 12px 12px;
  margin: 5px 10px 5px 10px;
  border-radius: 0;  
}
.blocks {width:100%;}

.btn {max-width:150px;width:100%;}
.container {
  background-color: pink;
  display: flex;
  justify-content: space-between;
  flex-flow: row wrap;
}

td
{
    max-width: 150px;
    word-wrap: break-word;
    padding: 0px;
    
}

</style>
</head>


<body>
    <div class="wrapper">
        <div class="sidebar" data-color="orange" data-image="${pageContext.servletContext.contextPath}/assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo">
                <a href="#" class="simple-text">
                    UST OPCIS (${studentNumber})
                </a>
            </div>
            <!-- Start of SideBar wrapper-->
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li>
                        <a href="/ListPetition.com">
                            <i class="material-icons">dashboard</i>
                            <p>Available Petition</p>
                        </a>
                    </li>
                    <li class ="active">
                        <a href="/JoinedServlet.com">
                            <i class="material-icons">person</i>
                            <p>Joined Petition</p>
                        </a>
                    </li>
                    <li>
                        <a href="/CreatedServlet.com">
                            <i class="material-icons">assignment</i>
                            <p>myCreated Petition</p>
                        </a>
                    </li>
                    <!-- <li class="active">
                        <a href="./table.html">
                            <i class="material-icons">content_paste</i>
                            <p>Table List</p>
                        </a>
                    </li>
                    <li>
                        <a href="./typography.html">
                            <i class="material-icons">library_books</i>
                            <p>Typography</p>
                        </a>
                    </li>
                    <li>
                        <a href="./icons.html">
                            <i class="material-icons">bubble_chart</i>
                            <p>Icons</p>
                        </a>
                    </li>
                    <li>
                        <a href="./maps.html">
                            <i class="material-icons">location_on</i>
                            <p>Maps</p>
                        </a>
                    </li> -->
                   <!--  <li>
                        <a href="./notifications.html">
                            <i class="material-icons text-gray">notifications</i>
                            <p>Notifications</p>
                        </a>
                    </li> -->
                    <li>
                    <a href="/AddPetition.com">
                    <i class="material-icons text-gray">add</i>
                    <p>Add new Petition</p></a>
                    </li>
                </ul>
            </div>
            <!-- End of SideBar wrapper-->
        </div>
       	<div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Available Petition</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                           
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification"></span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                </a>
                                <!-- Dito ko ilalagay yung for each -->
                                <ul class="dropdown-menu">
                                    <c:forEach items="${notif}" var="NotificationBean">
                                        <li>
                                            <a href = "#">Alert: ${NotificationBean.message} &nbsp;Time: ${NotificationBean.timelog}</a>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </li>
                            
                            <li>
                                <a href="/StudentLogout.com">
                                    <i class="material-icons">power_settings_new</i>
                                    <p class="hidden-lg hidden-md">Logout</p>
                                </a>
                            </li>
                        </ul>
                        
                    </div>
                </div>
            </nav>
            <div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header card-header-primary card-header-icon"
									data-background-color="orange">
									<h4 class="card-title">Pending</h4>
									<% 
										String studentNumber = (String)request.getSession().getAttribute("studentNumber");
										String studentYear = (String)request.getSession().getAttribute("studentYear");
										//int year = Integer.parseInt(studentYear);
										Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
										String connectionUrl = "jdbc:sqlserver://localhost:1433;" +  "databaseName=OPCIS;user=sa;password=April241997;";
										Connection con=DriverManager.getConnection(connectionUrl); 
										PreparedStatement ps= con.prepareStatement("SELECT * FROM SETTING Where settingID = 1");
										ResultSet rs=ps.executeQuery();  
									%>
									<%
										while(rs.next()) {
										String settingID = rs.getString("settingID");
										String dateString = rs.getString("date");
										String semesterString=rs.getString("semester");
									%>
										<h6><strong>Semester:</strong> <%=semesterString%> Semester <strong>Enrollment Dates:</strong> <%=dateString%></h6>                                         
									<%
										}
									%> 
									
									<p class="category"></p>
								</div>
								<div class="card-body">
									<div class="toolbar">
										<!--        Here you can write extra buttons/actions for the toolbar              -->
									</div>
									<div class="material-datatables" style="clear: both">

										<table id="example"
											class="table table-striped table-no-bordered table-hover"
											cellspacing="0" width="100%" style="width: 100%">
											<thead class="text-primary">
												<tr>
													<th>Course:</th>
													<th>Professor:</th>
													<th>Number of Joined students</th>
													<th>Action</th>
												</tr>
											</thead>
											<c:forEach items="${pending}" var="PendingBean">
												<tr>
													<td>${PendingBean.course}</td>
													<td>${PendingBean.professorName}</td>
													<td>${PendingBean.slot}</td>
													<!-- Sa # Mo ilalagay yung service to delete lugs  -->
													<td class="text-center">
													<div class="btn-group blocks"></div>
													<a class = "btn btn-info" href="/ViewSchedule.com?PetitionIDBean=${PendingBean.petitionID}">View Schedule</a>
													<a class = "btn btn-danger" href="/RemoveJoined.com?PetitionIDBean=${PendingBean.petitionID}">Withdraw</a></td>
													</div>
												</tr>
											</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
								<!-- end content-->
							</div>
							<!--  end card  -->
						</div>
						<!-- end col-md-12 -->
					</div>
					<!-- end row -->

				</div>
			</div>
            <div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header card-header-primary card-header-icon"
									data-background-color="orange">
									<h4 class="card-title">For Approval</h4>
									<p class="category"></p>
								</div>
								<div class="card-body">
									<div class="toolbar">
										<!--        Here you can write extra buttons/actions for the toolbar              -->
									</div>
									<div class="material-datatables" style="clear: both">

										<table id="example"
											class="table table-striped table-no-bordered table-hover table-condensed"
											cellspacing="0" width="100%" style="width: 100%">
											<thead class="text-primary">
												<tr>
													<th>Course:</th>
													<th>Professor:</th>
													<th>Number of Enlisted students</th>
													<th>Dept Chair Remarks</th>
													<th>Approval Date:</th>
													<th>Dean Remarks</th>
													<th>Approval Date:</th>
													<th>Registrar Remarks</th>
													<th>Approval Date:</th>
													<th>OVRAA Remarks</th>
													<th>Approval Date:</th>
													<th>Accounting Remarks</th>
													<th>Approval Date:</th>
													<th>Action</th>
												</tr>
											</thead>
											<c:forEach items="${join}" var="JoinBean">
												<tr>
													<td>${JoinBean.course}</td>
													<td>${JoinBean.professorName}</td>
													<td>${JoinBean.slot}</td>
													<td>${JoinBean.deptChairRemarks}</td>
													<td>${JoinBean.deptChairTimelog}</td>
													<td>${JoinBean.deanRemarks}</td>
													<td>${JoinBean.deanTimelog}</td>
													<td>${JoinBean.registrarRemarks}</td>
													<td>${JoinBean.registrarTimelog}</td>
													<td>${JoinBean.OVRAATimelog}</td>
													<td>${JoinBean.ovraaRemarks}</td>
													<td>${JoinBean.accountingTimelog}</td>
													<td>${JoinBean.accountingRemarks}</td>
													<!-- Sa # Mo ilalagay yung service to delete lugs  -->									
													<td class="text-left">
													<a class = "btn btn-info" href="/ViewSchedule.com?PetitionIDBean=${JoinBean.petitionID}">View<br>
													Schedule</a></td>
													
												</tr>
											</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
								<!-- end content-->
							</div>
							<!--  end card  -->
						</div>
						<!-- end col-md-12 -->
					</div>
					<!-- end row -->

				</div>
			</div>         
</body>
<!--   Core JS Files   -->
<script src="${pageContext.servletContext.contextPath}/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/assets/js/material.min.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="${pageContext.servletContext.contextPath}/assets/js/chartist.min.js"></script>
<!--  Dynamic Elements plugin -->
<script src="${pageContext.servletContext.contextPath}/assets/js/arrive.min.js"></script>
<!--  PerfectScrollbar Library -->
<script src="${pageContext.servletContext.contextPath}/assets/js/perfect-scrollbar.jquery.min.js"></script>
<!--  Notifications Plugin    -->
<script src="${pageContext.servletContext.contextPath}/assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Material Dashboard javascript methods -->
<script src="${pageContext.servletContext.contextPath}/assets/js/material-dashboard.js?v=1.2.0"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="${pageContext.servletContext.contextPath}/assets/js/demo.js"></script>
<!-- Data Tables API JS-->
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="${pageContext.servletContext.contextPath}/js/dataTables.bootstrap.js"></script>
	<script src="${pageContext.servletContext.contextPath}/js/jquery.dataTables.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script type = "text/javascript">
    var $ = jQuery;
    $(document).ready(function() {
        $('#example').DataTable();
    });
    </script>

</html>

