<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="images/icons/OfficialSealUST.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Joined Petition Success - UST OPCIS</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="orange" data-image="${pageContext.servletContext.contextPath}/assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo">
                <a href="#" class="simple-text">
                    UST OPCIS (${studentNumber})
                </a>
            </div>
            <!-- Start of SideBar wrapper-->
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li>
                        <a href="/ListPetition.com">
                            <i class="material-icons">dashboard</i>
                            <p>Available Petition</p>
                        </a>
                    </li>
                    <li class = "active">
                        <a href="/JoinedServlet.com">
                            <i class="material-icons">person</i>
                            <p>Joined Petition</p>
                        </a>
                    </li>
                    <!-- <li class="active">
                        <a href="./table.html">
                            <i class="material-icons">content_paste</i>
                            <p>Table List</p>
                        </a>
                    </li>
                    <li>
                        <a href="./typography.html">
                            <i class="material-icons">library_books</i>
                            <p>Typography</p>
                        </a>
                    </li>
                    <li>
                        <a href="./icons.html">
                            <i class="material-icons">bubble_chart</i>
                            <p>Icons</p>
                        </a>
                    </li>
                    <li>
                        <a href="./maps.html">
                            <i class="material-icons">location_on</i>
                            <p>Maps</p>
                        </a>
                    </li> -->
                    <!-- <li>
                        <a href="./notifications.html">
                            <i class="material-icons text-gray">notifications</i>
                            <p>Notifications</p>
                        </a>
                    </li> -->
                    <li>
                    <a href="/AddPetition.com">
                    <i class="material-icons text-gray">add</i>
                    <p>Add new Petition</p></a>
                    </li>
                </ul>
            </div>
            <!-- End of SideBar wrapper-->
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Available Petition</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                           
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification"></span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                </a>
                                <!-- Dito ko ilalagay yung for each -->
                                <ul class="dropdown-menu">
                                    <c:forEach items="${notif}" var="NotificationBean">
                                        <li>
                                           <a href = "#">Alert: ${NotificationBean.message} &nbsp;Time: ${NotificationBean.timelog}</a>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </li>
                            
                            <li>
                                <a href="/StudentLogout.com">
                                    <i class="material-icons">power_settings_new</i>
                                    <p class="hidden-lg hidden-md">Logout</p>
                                </a>
                            </li>
                        </ul>
                        
                    </div>
                </div>
            </nav>
<%-- <div class = "container">
	<h1>Petition info</h1>
	<p><strong> Congratulations ${studentNumber}! You have joined the Petition Class ${requestScope.joinCourse}</strong></p>
	</br>
	<a href = "/ListPetition.com" class = "btn btn-warning">Go Back</a>
	<form action = "/StudentJoinController" method = "POST">
	<input type = "hidden" id = "userName" value = "${studentNumber}">
	</form>
</div> --%>
<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                            <div class="card-header" data-background-color="orange">
                                    <h4 class="title">Petition Information</h4>
                                </div>
                                <div class="card-content">
                                	 <div class="row">
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<p class="category"> ${studentNumber} You have joined the Petition Class ${requestScope.joinCourse}</p>
									<br/>
									  &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;<a href = "/ListPetition.com" class = "btn btn-warning">Go Back</a>
									<form action = "/StudentJoinController" method = "POST">
										<input type = "hidden" id = "userName" value = "${studentNumber}">
									</form>
									</div>
									</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<footer class="footer">
                <div class="container-fluid">
                  <div class="copyright pull-right">
                        �
                        <script>
                            document.write(new Date().getFullYear())
                        </script>, made with love by
                        <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.   
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="${pageContext.servletContext.contextPath}/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/assets/js/material.min.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="${pageContext.servletContext.contextPath}/assets/js/chartist.min.js"></script>
<!--  Dynamic Elements plugin -->
<script src="${pageContext.servletContext.contextPath}/assets/js/arrive.min.js"></script>
<!--  PerfectScrollbar Library -->
<script src="${pageContext.servletContext.contextPath}/assets/js/perfect-scrollbar.jquery.min.js"></script>
<!--  Notifications Plugin    -->
<script src="${pageContext.servletContext.contextPath}/assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Material Dashboard javascript methods -->
<script src="${pageContext.servletContext.contextPath}/assets/js/material-dashboard.js?v=1.2.0"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="${pageContext.servletContext.contextPath}/assets/js/demo.js"></script>

</html>
