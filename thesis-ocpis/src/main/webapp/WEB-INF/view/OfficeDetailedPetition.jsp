<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="images/icons/OfficialSealUST.png"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>College Office List Approve Petition - UST OPCIS</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="${pageContext.servletContext.contextPath}/assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <!--  Data Tables CSS -->
	<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel = "stylesheet">
	<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel = "stylesheet">
	<style>
	 table 
		{
		    table-layout:fixed;
		    width:100%;
		}
	</style>
</head>
<%-- <nav role="navigation" class="navbar navbar-default">
	<div class="">
		<a href="/" class="navbar-brand">UST OCPIS</a>
	</div>
	<div class="navbar-collapse">
		<ul class="nav navbar-nav">
		    <li><a href="#">Home</a></li>
            <li class="active"><a href="/ListApprove.com">Petition Approval</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="/RegistrarLogout.com">Logout</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="#">${employeeNumber}</a></li>
		</ul>
	</div>
</nav> --%>
<body>
    <div class="wrapper">
        <div class="sidebar" data-color="orange" data-image="${pageContext.servletContext.contextPath}/assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo">
                <a href="#" class="simple-text">
                    UST OPCIS (${employeeNumber})
                </a>
            </div>
            <!-- Start of SideBar wrapper-->
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class = "active">
                        <a href="/ListApprove.com">
                            <i class="material-icons">content_paste</i>
                            <p>Petitions for Approval</p>
                        </a>
                    </li>
                    <!-- <li class="active">
                        <a href="./table.html">
                            <i class="material-icons">content_paste</i>
                            <p>Table List</p>
                        </a>
                    </li>
                    <li>
                        <a href="./typography.html">
                            <i class="material-icons">library_books</i>
                            <p>Typography</p>
                        </a>
                    </li>
                    <li>
                        <a href="./icons.html">
                            <i class="material-icons">bubble_chart</i>
                            <p>Icons</p>
                        </a>
                    </li>
                    <li>
                        <a href="./maps.html">
                            <i class="material-icons">location_on</i>
                            <p>Maps</p>
                        </a>
                    </li> -->
                    <!-- <li>
                        <a href="./notifications.html">
                            <i class="material-icons text-gray">notifications</i>
                            <p>Notifications</p>
                        </a>
                    </li>
                    <li>
                    <a href="/AddPetition.com">
                    <i class="material-icons text-gray">add</i>
                    <p>Add new Petition</p></a>
                    </li> -->
                </ul>
            </div>
            <!-- End of SideBar wrapper-->
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Petitions for Approval</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li>
                                <a href="/OfficeLogout.com">
                                    <i class="material-icons">power_settings_new</i>
                                    <p class="hidden-lg hidden-md">Logout</p>
                                </a>
                            </li>
                        </ul>
                        
                    </div>
                </div>
            </nav>
            
            
            
	<div class = "content">
	<div class="container-fluid">
	<div class="table-responsive">
	<div class = "card">
		<div class="card-header" data-background-color="orange">
			Detailed Information
		</div>
		<br>
		<table id = "example" class = "table table-striped table-hover table-condensed">
			<thead class = "thead-inverse">
				<tr><!-- Table Label  -->
				 <td>College</td> 
				 <td>Program</td>  
				 <td>Petition Course</td>
				 <td>Student Name</td>
			 	 <td>Student ID</td>
			 	<!--  <td>deptChairID</td>
				 <td>deptChairRemarks</td>  
				 <td>deanID</td>  
				 <td>deanRemarks</td>  
				 <td>registrarID</td>  
				 <td>registrarRemarks</td>
			 	 <td>ovraaID</td>  
			 	 <td>ovraaRemarks</td>  
			 	 <td>accountingID</td>  
			 	 <td>accountingRemarks</td>   -->
			 	 <td>Price</td>
			 	 <td>Sched</td>

				</tr>
			</thead>
			<tbody>
				<c:forEach items="${office}" var="OfficeBean">
					<%-- <c:forEach items="${sched}" var="ApproveScheduleServiceBean"> --%>
					<tr>
					<td>${OfficeBean.collegeName}</td>
					<td>${OfficeBean.programName}</td>
					<td>${OfficeBean.petitionCourse}</td>
			 		<td>${OfficeBean.studentLastName}, ${OfficeBean.studentFirstName} ${OfficeBean.studentMiddleName}
			 		<td>${OfficeBean.studentID}</td>  
			 		<%-- <td>${OfficeBean.deptChairID}</td>
			 		<td>${OfficeBean.deptChairRemarks}</td>  
			 		<td>${OfficeBean.deanID}</td>  
			 		<td>${OfficeBean.deanRemarks}</td>  
			 		<td>${OfficeBean.registrarID}</td>  
			 		<td>${OfficeBean.registrarRemarks}</td>
					 <td>${OfficeBean.ovraaID}</td>  
					 <td>${OfficeBean.ovraaRemarks}</td>  
					 <td>${OfficeBean.accountingID}</td>  
					 <td>${OfficeBean.accountingRemarks}</td>   --%>
					 <td>${OfficeBean.price}</td>
					 <td><c:forEach items="${sched}" var="ApproveScheduleServiceBean">${ApproveScheduleServiceBean.day} ${ApproveScheduleServiceBean.timeStart} - ${ApproveScheduleServiceBean.timeEnd} Rm. ${ApproveScheduleServiceBean.room}<br></c:forEach></td>
					<%-- <td>${ApproveScheduleServiceBean.day} ${ApproveScheduleServiceBean.timeStart} - ${ApproveScheduleServiceBean.timeEnd} Rm. ${ApproveScheduleServiceBean.room}</td> --%>
					</tr>
					<%-- </c:forEach> --%>
				</c:forEach>
			</tbody>
		</table>
	</div>
	</div>
	</div>
	<a class = "btn btn-info" href="ListApprove.com">Go Back</a>
	</div>
	
<footer class="footer">
                <div class="container-fluid">
                   <div class="copyright pull-right">
                        �
                        <script>
                            document.write(new Date().getFullYear())
                        </script>, made with love by
                        <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.   
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="${pageContext.servletContext.contextPath}/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/assets/js/material.min.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="${pageContext.servletContext.contextPath}/assets/js/chartist.min.js"></script>
<!--  Dynamic Elements plugin -->
<script src="${pageContext.servletContext.contextPath}/assets/js/arrive.min.js"></script>
<!--  PerfectScrollbar Library -->
<script src="${pageContext.servletContext.contextPath}/assets/js/perfect-scrollbar.jquery.min.js"></script>
<!--  Notifications Plugin    -->
<script src="${pageContext.servletContext.contextPath}/assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Material Dashboard javascript methods -->
<script src="${pageContext.servletContext.contextPath}/assets/js/material-dashboard.js?v=1.2.0"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="${pageContext.servletContext.contextPath}/assets/js/demo.js"></script>
<!-- Data Tables API JS-->
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="${pageContext.servletContext.contextPath}/js/dataTables.bootstrap.js"></script>
	<script src="${pageContext.servletContext.contextPath}/js/jquery.dataTables.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
	
    <script type = "text/javascript">
    var $ = jQuery;
    /* $(document).ready(function() {
        $('.table').DataTable({
        });
    }); */
    
    $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'csv', 'excel', 'pdf', {
                    extend: 'print', title: 'College Office - Detailed view of Approved Petition'
                    
               }
                ]
        } );
    } );
    
    
    </script>

</html>