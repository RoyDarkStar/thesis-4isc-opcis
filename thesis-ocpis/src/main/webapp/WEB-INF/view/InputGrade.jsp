<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import ="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	
	<form action="InputGrade.com" method="POST">
		
		<label>Student Number: </label>
		<select name="selectStudent">
		
			<%
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String connectionUrl = "jdbc:sqlserver://localhost:1433;" +  
                    	"databaseName=OPCIS;user=sa;password=April241997;";
                Connection con=DriverManager.getConnection(connectionUrl); 
                PreparedStatement ps= con.prepareStatement("SELECT * FROM [Student] WHERE studentID=2014000008");
                ResultSet rs=ps.executeQuery();
			%>
			<%
				while(rs.next()){
					String studentNumber = rs.getString("studentID");
			%>
			<option value = "<%= studentNumber %>"><%= studentNumber %></option>
			<%
				}
			%>
		</select><br>
		
		<label>Course: </label>
		<select name="selectCourse">
		
			<%
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
                PreparedStatement ab= con.prepareStatement("SELECT * FROM [Course] WHERE curriculumID=1 AND year<=3 ORDER BY year");
                ResultSet cd=ab.executeQuery();
			%>
			<%
				while(cd.next()){
					String courseID = cd.getString("courseID");
					String courseName = cd.getString("courseName");
			%>
			<option value = "<%= courseID %>"><%= courseName %></option>
			<%
				}
			%>
		</select><br>
		
		<label>Grade: </label>
		<select name="grade">
			<option value="1.0">1.0</option>
			<option value="1.25">1.25</option>
			<option value="1.5">1.5</option>
			<option value="1.75">1.75</option>
			<option value="2.0">2.0</option>
			<option value="2.25">2.25</option>
			<option value="2.5">2.5</option>
			<option value="2.75">2.75</option>
			<option value="3.0">3.0</option>
			<option value="5.0">5.0</option>
		</select><br>
		
		<input type="submit" value="Submit"/>
	</form>
	
	<button><a href = "HomePage.com">Go Back</a></button>
</body>
</html>