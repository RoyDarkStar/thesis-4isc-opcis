<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>.
    <%@ page import ="java.sql.*" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href = "webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" rel = "stylesheet">
<title>Registrar Petition</title>
<!-- Bootstrap core CSS -->
<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"
	rel="stylesheet">
<!-- To be updated na gawing pang faculty  -->

    <!--  Data Tables CSS -->
	<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel = "stylesheet">
	<style>
	 table 
		{
		    table-layout:fixed;
		    width:100%;
		}
	</style>
<style>
.footer {
	position: absolute;
	bottom: 0;
	width: 100%;
	height: 60px;
	background-color: #f5f5f5;
}

.footer .container {
width: auto;
max-width: 680px;
padding: 0 15px;
}
</style>
</head>
<body>
<nav role="navigation" class="navbar navbar-default">

		<div class="">
			<a href="/" class="navbar-brand">UST OCPIS</a>
		</div>

		<div class="navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">Home</a></li>
				<li><a href="/ListPetition.com">Petition</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/StudentLogin.com">Login</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/StudentLogout.com">Logout</a></li>
			</ul>
		</div>

	</nav>
	<div class="container">
		<H1>Welcome ${studentNumber}</H1>
		
		<table class = "table table-striped">
			<caption>Petition</caption>
			<thead>
			<th>Petition Name</th>
			<th>Department</th>
			<th>Action</th>
			<tbody>
				<c:forEach items="${petition}" var="PetitionBean">
					<tr>
						<td>${PetitionBean.name}</td>
						<td>${PetitionBean.course}</td>
						<td>&nbsp;&nbsp; <a class = "btn btn-info" href="/PetitionDelete.com?PetitionBean=${PetitionBean.name}&CourseBean=${PetitionBean.course}">
							Approve Petition:</a></td>
						<td>&nbsp;&nbsp; <a class = "btn btn-warning" href="/PetitionDelete.com?PetitionBean=${PetitionBean.name}&CourseBean=${PetitionBean.course}">
							Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
<%-- 		<ol>
			<c:forEach items="${petition}" var="PetitionBean">
				<li>${PetitionBean.name}&nbsp;${PetitionBean.course}&nbsp;
				<a	href="/PetitionDelete.com?PetitionBean=${PetitionBean.name}%CourseBean=${PetitionBean.course}">Delete</a></li>
			</c:forEach>
		</ol> --%>

		<a href="/AddPetition.com">Add new petition</a>
	</div>

<!-- 	<footer class="footer">
		<div class="container">
			<p>footer content</p>
		</div>
	</footer> -->

	<script src="webjars/jquery/3.3.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
	<!-- Data Tables API JS-->
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="${pageContext.servletContext.contextPath}/js/dataTables.bootstrap.js"></script>
	<script src="${pageContext.servletContext.contextPath}/js/jquery.dataTables.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script type = "text/javascript">
    var $ = jQuery;
    $(document).ready(function() {
        $('.table').DataTable();
    });
    </script>

</body>

</html>