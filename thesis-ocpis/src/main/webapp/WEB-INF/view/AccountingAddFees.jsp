<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href = "webjars/bootstrap/3.3.7-1/css/bootstrap.min.css" rel = "stylesheet">
<title>Accounting Add Balance</title>
<!-- Bootstrap core CSS -->
<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"
	rel="stylesheet">

<style>
.footer {
	position: absolute;
	bottom: 0;
	width: 100%;
	height: 60px;
	background-color: #f5f5f5;
}

.footer .container {
width: auto;
max-width: 680px;
padding: 0 15px;
}
</style>
</head>
<body>
<nav role="navigation" class="navbar navbar-default">

		<div class="">
			<a href="/" class="navbar-brand">UST OCPIS</a>
		</div>

		<div class="navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">Home</a></li>
				<li><a href="/ListPetition.com">Petition</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/StudentLogin.com">Login</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/StudentLogout.com">Logout</a></li>
			</ul>
		</div>

	</nav>
	<div class="container">
		<form method="POST" action="/AddPetition.com">
			<fieldset class = "form-group">
				<label>Add Petition:</label>
			 	<input name="addingPetition" type="text" class = "form-control"/><br/>
			</fieldset>
			<fieldset class = "form-group">
				<label>Add Course:</label>
				<input name="addingCourse" type="text" class = "form-control"/><br/>
			</fieldset>
			<input name="add" type="submit" class ="btn btn-sucess" value = "Submit Petition" />
		</form>
	</div>

	<footer class="footer">
		<div class="container">
			<p>footer content</p>
		</div>
	</footer>

	<script src="webjars/jquery/3.3.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>

</body>

</html>