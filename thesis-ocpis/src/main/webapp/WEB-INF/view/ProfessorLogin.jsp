<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
<link rel="icon" type="image/png" href="images/icons/OfficialSealUST.png"/>
<title>Professor - UST OPCIS</title>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/gurospatio.css">

</head>
<body>
<header>
    <div class="container">
    	<img src="images/guruspatioheader.png" class="img-responsive img-div"/>
    </div>
</header> 
        <section class="index-section gateway">
            <div class="container">
                <div class="row">s
                    <div class="col-lg-3 col-md-3  col-sm-12 col-xs-12">
                        

<h5 class="header-label-panel">
    <i class="glyphicon glyphicon-user"></i>&nbsp;
    Login
</h5>

<div class="login-box">
    <form action="ProfessorLogin.com" method="post">
        <div class="form-group">
        	<p><font color = "red">${errorMessage}</font></p>
            <label>Employee Number</label>
            <input type="text" name="employeeNumber" maxlength="6" class="form-control input-sm" required="required" value=""/>
        </div>
        
        <div class="form-group">
            <label>Password</label>
            <input type="password" name="employeePassword" maxlength="20" class="form-control input-sm" required="required" value=""/>
            <p class="help-block">Initial password is your birthdate format&nbsp;:&nbsp;<span class="text-danger"><strong>MM/DD/YYYY</strong></span></p>
        </div>
        
        <input type="submit" name="btnSubmit" id="btnSubmit" value="Submit" class="btn btn-warning btn-block "/><br/>
    </form>
</div>
<br/>
                         
                        

<h5 class="header-label-panel-red">
    <strong>Announcement</strong>
</h5>

<div class="announce-box">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            
                <form action="announcementuploadview" method="POST" target="_blank" class="form-inline">
                    <input type="hidden" name="hidAnnouncementUploadId" value="237"/>
                    <p>
                        <button type="submit" class="btn-link text-left">
                            <strong>2018 Intake of CHED PhilFrance Programme</strong>
                            <br/><small class="text-muted"><i class="glyphicon glyphicon-calendar"></i>&nbsp;Posted :&nbsp; 2018-02-12 </small>
                        </button>
                        
                    </p>
                </form>
            
                <form action="announcementuploadview" method="POST" target="_blank" class="form-inline">
                    <input type="hidden" name="hidAnnouncementUploadId" value="236"/>
                    <p>
                        <button type="submit" class="btn-link text-left">
                            <strong>2018 CHED SCIENCE Scholarship Programme</strong>
                            <br/><small class="text-muted"><i class="glyphicon glyphicon-calendar"></i>&nbsp;Posted :&nbsp; 2018-02-12 </small>
                        </button>
                        
                    </p>
                </form>
            
                <form action="announcementuploadview" method="POST" target="_blank" class="form-inline">
                    <input type="hidden" name="hidAnnouncementUploadId" value="235"/>
                    <p>
                        <button type="submit" class="btn-link text-left">
                            <strong>2018 CHED MACQUARIE Scholarship Programme</strong>
                            <br/><small class="text-muted"><i class="glyphicon glyphicon-calendar"></i>&nbsp;Posted :&nbsp; 2018-02-12 </small>
                        </button>
                        
                    </p>
                </form>
            
        </div>
    </div>
</div>
                        <div id="divHttpsSeal" align="center">
                            <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
                                <tr>
                                    <td width="135" align="center" valign="top">
                                        <script type="text/javascript"
                                                src="https://seal.verisign.com/getseal?host_name=guruspatio.ust.edu.ph&amp;size=L&amp;use_flash=NO&amp;use_transparent=NO&amp;lang=en"></script>
                                        <br/>
                                         
                                        <a href="http://www.symantec.com/ssl-certificates" target="_blank"
                                           style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL
                                                                                                                                                                                           CERTIFICATES</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <h5 class="text-right header-label-box ">
    <span class="header-label-box-inner"><strong>INTRODUCTION</strong></span>
</h5>

<p class="lead">Welcome to Thomasian Administrator and Faculty Portal.</p>

<p class="text-justify">
    The faculty portal is gateway of the academic personnel of the University to access their personal 
    information that is related to their field of work. This gateway bridges communication between the 
    academic work force and the Office of the Vice Rector for Academic Affairs through its simple Policy Library and Forms Link.
</p>

<br/>
<h5 class="text-right header-label-box ">
    <span class="header-label-box-inner"><strong>INSTRUCTIONS</strong></span>
</h5>

<p class="text-warning">
    <strong>On Portal Log-in</strong>
</p>

<ol>
    <li>All faculty members given their faculty code by OVRAA will have access to the Faculty Portal.</li>
    <li>The faculty code provided by OVRAA upon issuance of appointment paper will be the username of the faculty member.</li>
    <li>The default password is the birthday of the faculty member using the format MM/DD/YYYY.</li>
    <li>The faculty member is advised to change his/her password the after first login to ensure security of personal records.</li>
</ol>

<p class="text-warning">
    <strong>On Changing of Password</strong>
</p>

<ol>
    <li>To change your password, access your profile then click account security icon at the Guru's PATIO gateway.</li>
    <li>Initially, the portal will ask the user to set up a secret question that will help him/her to reset their password in case of forgotten password.</li>
    <li>Once the secret question is set, the user may proceed in changing his/her password.</li>
    <li>Instructions for the secret questions and change password are available on the same pages.</li>
</ol>

<p class="text-warning">
    <strong>On Viewing and Updating of Personal Information</strong>
</p>

<ol>
    <li>OVRAA defines the information that can only be viewed  and those that can be updated by the faculty members.</li>
    <li>If there will be any discrepancies on the information that are allowed only to be viewed, the faculty member shall proceed personally to OVRAA to request for the change of information.</li>
    <li>Information that has ADD/UPDATE/DELETE button can be updated by the faculty member. The faculty member is responsible for any update of his/her own record.</li>
    <li>For the items that require supporting documents, such as Academic Preparations, Trainings and Awards, faculty members are advised to submit the documents to OVRAA after updating their records.</li>
</ol>

<p class="text-warning">
    <strong>On Policy Library</strong>
</p>

<ol>
    <li>The Policy Library is a collection of significant academic and faculty-related policies and procedures of OVRAA.</li>
    <li>The Policies are in graphical presentation to easily trace the flow of information.</li>
</ol>

<p class="text-warning">
    <strong>On Downloadable Forms</strong>
</p>

<ol>
    <li>The Downloadable Forms are the pertinent forms that faculty members usually fill out.</li>
    <li>The portal provides PDF files that can be save on the disks. Faculty members do not need to go to OVRAA to secure these forms.</li>
</ol>




                    </div>
                </div>
            </div>
        </section>
         
        <footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p class="text-center">Copyright &copy; 2016  University of Santo Tomas, Office of the Vice Rector for Academic Affairs (OVRAA). www.ust.edu.ph. All rights reserved.</p> 
                <p class="text-center">Powered by <a href="http://steps.ust.edu.ph/" class="text-gold" target="_blank">Santo Tomas e-Service Providers</a></p>
            </div>
        </div>
    </div>
</footer>

<script src="webjars/jquery/3.3.1/jquery.min.js"></script>
<script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
<script src="${pageContext.servletContext.contextPath}/js/jqueryui.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/jquery-ui.custom.min.js" type="text/javascript"></script>

<%--======================================================================================================== --%>
<script type="text/javascript">
  $(document).ready(function () {
      $('[data-toggle="offcanvas"]').click(function () {
          $('.row-offcanvas').toggleClass('active')
      })
  });
</script>

<script type="text/javascript">
  $(document).ready(function () {
      if ($(window).width() <= 568) {
          $('div.name-div').addClass('name-phone');
          $('a.link-header').removeClass('pull-right');
          $('a.link-header').before('<br/>');
          $('form.link-header').removeClass('pull-right');
          $('ol.breadcrumb').hide();
          $('h3').addClass('label-phone')
          $('body').css('font-size', '1.3em');
          $('h3').css('font-size', '1.3em');
          $('h3 small form').removeClass('pull-right');
          $('h3 small form').addClass('block');
          $('div#navbar').removeClass('remove-margin');
      }
      if ($(window).width() > 568) {
          $('h3 small form').addClass('pull-right');
          $('h3 small form').removeClass('block')
          $('div#navbar').addClass('remove-margin');
      }
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
      var bodyId = $('body').attr('id');
      if (bodyId == 'personalProfileId' || bodyId == 'pendingLeaveId' || bodyId == 'changePasswordId' || bodyId == 'academicPrepId' || bodyId == 'adminEvalId') {
          $('ul.main-menu li:nth-child(1)').addClass('active');
      }
      if (bodyId == 'absencesId' || bodyId == 'leaveArchiveId' || bodyId == 'secretQuestionId' || bodyId == 'teachAdminId') {
          $('ul.main-menu li:nth-child(2)').addClass('active');
      }
      if (bodyId == 'payslipId' || bodyId == 'researchCreativeId') {
          $('ul.main-menu li:nth-child(3)').addClass('active');
      }
      if (bodyId == 'facultyLoadId' || bodyId == 'communityExtId') {
          $('ul.main-menu li:nth-child(4)').addClass('active');
      }
      if (bodyId == 'downloadsId' || bodyId == 'printSupportId') {
          $('ul.main-menu li:nth-child(5)').addClass('active');
      }
  });
</script>
        <script type="text/javascript">
         var $buoop={c:2,text:"The browser you are using is already out-of date. It has known security flaws and some features of this system may not work properly. We strongly recommend that you <u>UPDATE</u>  your browser now to avoid any problems in the future. Thank you."};function $buo_f(){var e=document.createElement("script");e.src="//browser-update.org/update.min.js";document.body.appendChild(e)};try{document.addEventListener("DOMContentLoaded",$buo_f,!1)}
catch(e){window.attachEvent("onload",$buo_f)}
function disableLogin(){if($('#buorg').length)
$('#btnSubmit').attr('disabled','disabled')}
window.onload=disableLogin
        </script>

</body>
</html>